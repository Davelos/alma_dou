﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ItemSoket : MonoBehaviour, IDropHandler
{
    public UnityEvent OnSoketFilled => _onSoketFilled;

    [SerializeField] private bool _useAnim; 
    [SerializeField] private DragAndDropItem _desiredItem;

    public void OnDrop(PointerEventData eventData)
    {
        if (!eventData.pointerDrag.TryGetComponent<DragAndDropItem>(out var dropItem))
            return;

        TryAddToBasket(dropItem);
    }

    public bool IsDesiredItem(DragAndDropItem item)
    {
        return _desiredItem == item;
    }

    public bool TryAddToBasket(DragAndDropItem item)
    {
        if (transform.childCount == 0 && IsDesiredItem(item))
        {
            item.AddToBasket(transform as RectTransform, _useAnim);
            _onSoketFilled.Invoke();
            return true;
        }
        return false;
    }

    private UnityEvent _onSoketFilled = new();
}