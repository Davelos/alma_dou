using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemBasket : MonoBehaviour, IDropHandler
{
    public UnityEvent OnBasketFilled => _onBasketFilled;
    public UnityEvent<DragAndDropItem> OnItemAdded => _onItemAdded;

    [SerializeField] private Image _myImage;
    [SerializeField] private List<DragAndDropItem> _desiredItems;
    [SerializeField] private Transform[] _basketPlaces;
    [SerializeField] private bool _editAlphaHitTest = false;
    [SerializeField] private bool _disableImageAfterFilled = false;

    private UnityEvent _onBasketFilled = new();
    private UnityEvent<DragAndDropItem> _onItemAdded = new();

    private void Awake()
    {
        if (_editAlphaHitTest)
            GetComponent<Image>().alphaHitTestMinimumThreshold = 0.3f;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!eventData.pointerDrag.TryGetComponent<DragAndDropItem>(out var dropItem))
            return;

        TryAddToBasket(dropItem);
    }

    public bool IsDesiredItem(DragAndDropItem item)
    {
        return _desiredItems.Contains(item);
    }

    public bool TryAddToBasket(DragAndDropItem item)
    {
        Transform freePlace = GetFreePlace(out var remainigSpace);
        if (freePlace != null && IsDesiredItem(item))
        {
            item.AddToBasket(freePlace as RectTransform);
            _onItemAdded.Invoke(item);
            if (remainigSpace == 0)
            {
                _onBasketFilled.Invoke();

                if (_disableImageAfterFilled && _myImage != null)
                    _myImage.enabled = false;
            }
            return true;
        }
        return false;
    }

    public void Restart()
    {
        _myImage.enabled = true;
    }

    private Transform GetFreePlace(out int remainingSpace)
    {
        Transform freePlace = null;
        remainingSpace = 0;

        for (int i = 0; i < _basketPlaces.Length; i++)
        {
            Transform place = _basketPlaces[i];
            if (place.childCount == 0)
            {
                remainingSpace = _basketPlaces.Length - i - 1;
                freePlace = place;
                break;
            }
        }

        return freePlace;
    }
}
