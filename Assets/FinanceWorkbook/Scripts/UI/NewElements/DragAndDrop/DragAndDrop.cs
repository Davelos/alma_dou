﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class DragAndDrop : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public bool Interactable { get => _myGraphic.raycastTarget; set => _myGraphic.raycastTarget = value; }

    public RectTransform Parent { get => _parent; set => _parent = value; }

    protected RectTransform RectTransform => _rectTransform;
    protected MaskableGraphic MyGraphic => _myGraphic;
    protected Canvas MyCanvas => _canvas;

    [SerializeField] private RectTransform _parent;
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private MaskableGraphic _myGraphic;

    private Canvas _canvas;

    [Inject]
    public void Construct(Canvas canvas)
    {
        _canvas = canvas;
    }

    private void OnValidate()
    {
        if (_rectTransform == null)
            _rectTransform = GetComponent<RectTransform>();

        if (_parent == null)
            _parent = transform.parent as RectTransform;

        if (_myGraphic == null)
            _myGraphic = GetComponent<MaskableGraphic>();
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        Interactable = false;
        transform.SetAsLastSibling();
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        Interactable = true;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        Vector2 newAnchorPos = RectTransform.anchoredPosition + eventData.delta / _canvas.scaleFactor;
        float halfSizeY = RectTransform.sizeDelta.y * 0.5f;
        float halfSizeX = RectTransform.sizeDelta.x * 0.5f;

        Vector2 correctAnchorPos = new(
            Mathf.Clamp(newAnchorPos.x, halfSizeX, _parent.sizeDelta.x - halfSizeX),
            Mathf.Clamp(newAnchorPos.y, halfSizeY, _parent.sizeDelta.y - halfSizeY));

        RectTransform.anchoredPosition = correctAnchorPos;
    }
}