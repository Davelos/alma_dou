using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResponseBtn : MonoBehaviour
{
    public Button Bt => _button;
    public TextMeshProUGUI TMP => _textMeshPro;

    [SerializeField] private Button _button;
    [SerializeField] private TextMeshProUGUI _textMeshPro;

    private void OnValidate()
    {
        if (_button == null)
            _button = GetComponent<Button>();

        if (_textMeshPro == null)
            _textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
    }
}
