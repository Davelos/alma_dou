using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ResponcePanel : MonoBehaviour, IActivatable
{
    public enum AnimType
    {
        OnlyXScale,
        AllScale
    }

    [SerializeField] private float _openCloseAnimDur = 0.5f;
    [SerializeField] private ResponseBtn _responseBtnPrefab;
    [SerializeField] private AnimType _animType;

    private bool _isActive;
    private string[] _panelAnswers;
    private List<ResponseBtn> _numResponseBtns = new();
    private UnityAction<int> _collback;
    private Tween _openCloseTween;

    public void Show(string[] newPanelAnswers, UnityAction<int> collback)
    {
        _collback = collback;
        SetActive(true);

        if (!HasChangedAnswers(newPanelAnswers)) return;
        RebuildBtns(newPanelAnswers);
        EditBtnsAnswer(newPanelAnswers);
    }

    public void SetActive(bool active)
    {
        if (_openCloseTween != null)
            _openCloseTween.Kill();

        if (active)
        {
            gameObject.SetActive(true);
            _openCloseTween = OpenAnim(_animType);
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            _openCloseTween = CloseAnim(_animType)
                .OnComplete(() => gameObject.SetActive(false));
        }

        _isActive = active;
    }

    public bool IsActive() => _isActive;

    private bool HasChangedAnswers(string[] panelAnswers)
    {
        if (_panelAnswers == null || _panelAnswers.Length != panelAnswers.Length) return true;

        bool needRebuild = false;
        for (int i = 0; i < panelAnswers.Length && !needRebuild; i++)
            needRebuild |= panelAnswers[i] != _panelAnswers[i];

        return needRebuild;
    }

    private void RebuildBtns(string[] newPanelAnswers)
    {
        int prevAnswersLength = _panelAnswers != null ? _panelAnswers.Length : 0;
        int countDiff = prevAnswersLength - newPanelAnswers.Length;
        if (countDiff > 0)
        {
            int startDelID = _numResponseBtns.Count - countDiff;

            for (int i = startDelID; i <= _numResponseBtns.Count; ++i)
                Destroy(_numResponseBtns[i].gameObject);
            _numResponseBtns.RemoveRange(startDelID, countDiff);
        }
        else if (countDiff < 0)
        {
            for (; countDiff < 0; ++countDiff)
            {
                var btn = Instantiate(_responseBtnPrefab, transform);
                int id = _numResponseBtns.Count;
                btn.Bt.onClick.AddListener(() => ClickOnResponseBtn(id));
                _numResponseBtns.Add(btn);
            }
        }
    }

    private void EditBtnsAnswer(string[] newPanelAnswers)
    {
        for (int i = 0; i < newPanelAnswers.Length; i++)
        {
            ResponseBtn btn = _numResponseBtns[i];
            btn.TMP.text = newPanelAnswers[i];
        }
        _panelAnswers = newPanelAnswers;
    }

    private void ClickOnResponseBtn(int id)
    {
        _collback?.Invoke(id);
        SetActive(false);
    }

    private Tween OpenAnim(AnimType animType) => animType switch
    {
        AnimType.OnlyXScale => OpenOnlyXScaleAnim(),
        AnimType.AllScale => OpenAllScaleAnim(),
        _ => null,
    };

    private Tween CloseAnim(AnimType animType) => animType switch
    {
        AnimType.OnlyXScale => CloseOnlyXScaleAnim(),
        AnimType.AllScale => CloseAllScaleAnim(),
        _ => null,
    };

    private Tween OpenOnlyXScaleAnim()
    {
        transform.localScale = new Vector3(0f, 1f, 1f);
        return transform.DOScaleX(1f, _openCloseAnimDur);
    }

    private Tween CloseOnlyXScaleAnim()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        return transform.DOScaleX(0f, _openCloseAnimDur);
    }

    private Tween OpenAllScaleAnim()
    {
        transform.localScale = new Vector3(0f, 0f, 1f);
        return transform.DOScale(1f, _openCloseAnimDur);
    }

    private Tween CloseAllScaleAnim()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        return transform.DOScale(0f, _openCloseAnimDur);
    }
}
