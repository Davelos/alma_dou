using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

public class SelectBtn : MonoBehaviour
{
    public Button Btn => _btn;
    public UnityEvent OnAnswerCorrect { get; private set; } = new();
    public UnityEvent OnAnswerIncorrect { get; private set; } = new();

    [SerializeField] private ResponcePanel _responcePanel;
    [SerializeField] private Button _btn;
    [SerializeField] private TextMeshProUGUI _btnText;
    [SerializeField] private string[] _answers;
    [SerializeField] private int _rightAnswerID;


    [Inject]
    public void Construct(ResponcePanel responcePanel)
    {
        if (_responcePanel == null)
            _responcePanel = responcePanel;
    }

    private void Awake()
    {
        _btn.onClick.AddListener(OnBtnClick);
    }

    private void OnDestroy()
    {
        _btn.onClick?.RemoveListener(OnBtnClick);
    }

    private void OnValidate()
    {
        if (_btn == null) _btn = GetComponent<Button>();
        if (_btnText == null) _btnText = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void OnBtnClick()
    {
        _responcePanel.Show(_answers, CheckAnswer);
        _responcePanel.transform.position = transform.position;
    }

    private void CheckAnswer(int answerId)
    {
        _btnText.text = _answers[answerId];

        if (answerId == _rightAnswerID)
        {
            _btnText.color = Color.green;
            _btn.interactable = false;
            OnAnswerCorrect?.Invoke();
        }
        else
        {
            _btnText.color = Color.red;
            OnAnswerIncorrect?.Invoke();
        }
    }
}
