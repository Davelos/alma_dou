﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewVideoInfo", menuName = "ScriptableObjects/VideoInfo", order = 3)]
public class VideoInfo : ScriptableObject
{
    public string Title;
    public string VideoPath;
    public AudioClip AudioRU;
    public AudioClip AudioENG;
    public AudioClip AudioKZX;
}