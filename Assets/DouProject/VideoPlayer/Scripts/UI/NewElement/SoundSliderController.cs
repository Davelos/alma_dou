using ALMA.SoundService;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class SoundSliderController : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private bool _collapseOnStart;
    [SerializeField] private bool _collapseAfterTime;
    [SerializeField] private SoundGroups _groupControl;
    [SerializeField] private float _autoCloseTime = 2f;

    [Header("Controls")]
    [SerializeField] private Image _soundBtnImage;
    [SerializeField] private Sprite _soundEnableSprite;
    [SerializeField] private Sprite _soundDisableSprite;
    [SerializeField] private Slider _slider;
    [SerializeField] private Animator _soundSliderAnimator;

    private Coroutine _autoCloseCorutine;
    private SoundGroup _controllSoundGroup;
    private bool _isSoundSliderOpen = true;
    private bool _soundIsDisable;
    private int _openAnimHash;
    private int _closeAnimHash;
    private float _lastSoundVal = 1f;

    [Inject]
    private void Construct(SoundGroupsController soundGroupsController)
    {
        _controllSoundGroup = soundGroupsController.GetGroup(_groupControl);
    }

    private void Awake()
    {
        _openAnimHash = Animator.StringToHash("OpenSlider");
        _closeAnimHash = Animator.StringToHash("CloseSlider");
    }

    void Start()
    {
        SetupSlider();
        if (_isSoundSliderOpen && _collapseOnStart)
            SwichSoundSliderState();
    }

    private void OnDestroy()
    {
        _slider.onValueChanged.RemoveListener(SoundValChanched);
    }

    public void ToggleSound()
    {
        if (!_soundIsDisable)
            _lastSoundVal = _slider.value;

        _slider.value = _soundIsDisable ? _lastSoundVal : 0f;
    }

    public void SwichSoundSliderState()
    {
        if (_isSoundSliderOpen)
            _soundSliderAnimator.Play(_closeAnimHash);
        else
            _soundSliderAnimator.Play(_openAnimHash);
        _isSoundSliderOpen = !_isSoundSliderOpen;
    }

    private void SetupSlider()
    {
        float savedVal = _controllSoundGroup.Volume;
        ToggleSoundState(savedVal);
        _slider.value = savedVal;
        _slider.onValueChanged.AddListener(SoundValChanched);
    }

    private void ToggleSoundState(float vol)
    {
        _soundIsDisable = vol <= 0.0001f;
        if (_soundBtnImage != null)
            _soundBtnImage.sprite = _soundIsDisable ? _soundDisableSprite : _soundEnableSprite;
    }

    private void SoundValChanched(float vol)
    {
        CollapseAfterTime();

        ToggleSoundState(vol);
        _controllSoundGroup.Volume = vol;
        _controllSoundGroup.SaveGroupInPrefs();
    }

    private void CollapseAfterTime()
    {
        if (!_collapseAfterTime)
            return;

        if (_autoCloseCorutine != null)
        {
            StopCoroutine(_autoCloseCorutine);
            _autoCloseCorutine = null;
        }
        _autoCloseCorutine = StartCoroutine(AutoCloseRoutine());
    }

    private IEnumerator AutoCloseRoutine()
    {
        yield return new WaitForSeconds(_autoCloseTime);
        _soundSliderAnimator.Play(_closeAnimHash);
        _isSoundSliderOpen = false;
        _autoCloseCorutine = null;
    }
}
