using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class TimeSliderController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private double VideoTime
    {
        set
        {
            _lastVideoTime = _videoPlayer.time;
            _videoPlayer.time = value;
            _videoTime = value;
            _videoPlayer.GetTargetAudioSource(0).time = (float)value;
        }

        get
        {
            if (_lastVideoTime == _videoPlayer.time)
                return _videoTime;
            else if (_lastVideoTime > 0)
            {
                _lastVideoTime = -1f;
                _videoTime = -1f;
            }
            return _videoPlayer.time;
        }
    }

    private Slider _slider;
    private VideoPlayer _videoPlayer;
    private bool _lastIsPlaying;
    private bool _isPressed;
    private double _videoTime = -1f;
    private double _lastVideoTime = -1f;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }

    private void Start()
    {
        _slider.onValueChanged.AddListener(SetNewSliderVal);
    }

    private void Update()
    {
        if (_isPressed)
            return;

        _slider.SetValueWithoutNotify((float)(VideoTime / _videoPlayer.clip.length));
    }

    private void OnDestroy()
    {
        _slider.onValueChanged.RemoveAllListeners();
    }

    public void Init(VideoPlayer videoPlayer)
    {
        _videoPlayer = videoPlayer;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _lastIsPlaying = _videoPlayer.isPlaying;
        if (_lastIsPlaying)
            _videoPlayer.Pause();
        _isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_lastIsPlaying)
            _videoPlayer.Play();
        _isPressed = false;
    }

    private void SetNewSliderVal(float value)
    {
        VideoTime = value * _videoPlayer.clip.length;
    }
}
