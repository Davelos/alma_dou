using ALMA.SoundService;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;

public class VideoPlayerController : MonoBehaviour
{
    public VideoInfo CurrentVideoInfo => _currentVideoInfo;

    [Header("Play button settings")]
    [SerializeField] private Button _playPauseBtn;
    [SerializeField] private Button _playPauseBigBtn;
    [SerializeField] private Image _playPauseBigBtnImage;
    [SerializeField] private Button _rewindBtn;
    [SerializeField] private float _rewindTime = 10f;
    [SerializeField] private Button _fastForwardBtn;
    [SerializeField] private float _fastForwardTime = 10f;
    [SerializeField] private Sprite _playBtnPlaySprite;
    [SerializeField] private Sprite _playBtnPauseSprite;

    [Header("Self settings")]
    [SerializeField] private VideoPlayer _videoPlayer;
    [SerializeField] private TextMeshProUGUI _titleText;
    [SerializeField] private TimeSliderController _timeSlider;
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private Transform _selectedLangiagePanel;

    [Header("Animation settings")]
    [SerializeField] private Animator _videoAnimator;
    [SerializeField] private Animator _pauseBtnAnimator;
    [SerializeField] private Animator _CenterAnimator;
    [SerializeField] private AnimationClip _closeBtnClip;
    [Min(1.5f)]
    [SerializeField] private float _controlsTimeClose;

    private bool IsDoubleClick => Time.time - _lastClickTime <= _doubleClickTime;

    private Coroutine _closeControlsCoroutine;
    private Coroutine _closeBigBtnImageCoroutine;
    private bool _isShowingControls;
    private bool _mouseIsMoved;
    private bool _videoIsStoped;
    private int _showAnimHash;
    private int _hideAnimHash;
    private int _showPauseBtnAnimHash;
    private int _hidePauseBtnAnimHash;
    private int _FastForwardAnimHash;
    private int _RewindAnimHash;
    private const float _doubleClickTime = 0.3f;
    private float _lastClickTime;
    private float _lastMusicVol;
    private bool _IsMusicTurnedOff;

    private AudioSource _audioSourceVideo;
    private VideoInfo _currentVideoInfo;
    private SoundGroup _musicGroup;
    private SoundGroup _targetGroup;

    [Inject]
    private void Construct(SoundGroupsController soundGroupsController)
    {
        _musicGroup = soundGroupsController.GetGroup(SoundGroups.Music);
        _targetGroup = soundGroupsController.GetGroup(SoundGroups.Video);
    }

    private void Awake()
    {
        SetupAnimHash();
        _timeSlider.Init(_videoPlayer);
        _videoPlayer.loopPointReached += OnVideoEnd;
    }

    private void Start()
    {
        ClearRenderTexture();
        SetButtonsListeners();
    }

    private void OnDestroy()
    {
        RemoveButtonsListeners();
        _videoPlayer.loopPointReached += OnVideoEnd;
    }

    private void OnVideoEnd(VideoPlayer vp)
    {
        _audioSourceVideo.time = 0;
        _audioSourceVideo.Stop();

        ShowBigPlayPauseBtn();
    }

    private void Update()
    {
        ToggleControlsOnMouseMovement();
        SetPlayTimeText();
    }

    public void TogglePlayerPlayState()
    {
        if (_videoPlayer.isPlaying)
        {
            _videoPlayer.Pause();
            _audioSourceVideo.Pause();
            _videoAnimator.Play(_showAnimHash);
            _videoIsStoped = true;
        }

        else
        {
            _videoPlayer.Play();
            _videoIsStoped = false;
            _audioSourceVideo.Play();
            _videoAnimator.Play(_hideAnimHash);
            _selectedLangiagePanel.gameObject.SetActive(false);
        }

        SetButtonState(_videoPlayer.isPlaying);
    }

    public void Rewind()
    {
        if (IsDoubleClick)
        {
            _videoPlayer.time -= Mathf.Min(_rewindTime, (float)_videoPlayer.time);
            _CenterAnimator.Play(_RewindAnimHash, 0, 0f);
            _audioSourceVideo.time = (float)_videoPlayer.time;
        }
        else
            _lastClickTime = Time.time;
    }

    public void FastForward()
    {
        if (IsDoubleClick)
        {
            _videoPlayer.time += Mathf.Min(_fastForwardTime, (float)(_videoPlayer.clip.length - _videoPlayer.time));
            _CenterAnimator.Play(_FastForwardAnimHash, 0, 0f);
            _audioSourceVideo.time = (float)_videoPlayer.time;
        }
        else
            _lastClickTime = Time.time;
    }

    public void OpenVideoPlayer(VideoInfo videoInfo)
    {
        _currentVideoInfo = videoInfo;

        SetupVideoPlayer(Resources.Load<VideoClip>(videoInfo.VideoPath));
        _titleText.text = videoInfo.Title;
        gameObject.SetActive(true);
        SetButtonState(_videoPlayer.playOnAwake);

        if (_musicGroup.SoundOn)
        {
            _musicGroup.TurnOff();
            _IsMusicTurnedOff = true;
        }

        _videoPlayer.Play();
        StartCoroutine(CloseBigBtnImage());
        StartCoroutine(CloseControlsRoutine());
        SetPlayPauseBntImage(_playBtnPauseSprite);

    }

    public void CloseVideoPlayer()
    {
        _videoPlayer.Stop();
        ShowBigPlayPauseBtn();
        gameObject.SetActive(false);

        Resources.UnloadAsset(_videoPlayer.clip);
        _videoPlayer.clip = null;
        _titleText.text = string.Empty;
        ClearRenderTexture();

        if (_IsMusicTurnedOff)
        {
            _musicGroup.TurnOn();
            _IsMusicTurnedOff = false;
        }

        _audioSourceVideo.Stop();
        _audioSourceVideo.clip = null;
    }

    private void ToggleControlsOnMouseMovement()
    {
        Vector2 mousePosition = Input.mousePosition;
        _mouseIsMoved = mousePosition.y <= 200;

        if (_mouseIsMoved)
        {
            if (!_isShowingControls)
            {
                _videoAnimator.Play(_showAnimHash);
                _isShowingControls = true;
            }
        }
        else
        {
            if (_isShowingControls && _closeControlsCoroutine == null && !_videoIsStoped)
            {
                _closeControlsCoroutine = StartCoroutine(CloseControlsRoutine());
            }
        }

    }

    private void SetupVideoPlayer(VideoClip clip)
    {
        if (_videoPlayer == null)
            _videoPlayer = GetComponent<VideoPlayer>();

        _videoPlayer.clip = clip;

        for (ushort i = 0; i < clip.audioTrackCount; i++)
            _videoPlayer.SetTargetAudioSource(i, _targetGroup.Source);

        _audioSourceVideo = _videoPlayer.GetTargetAudioSource(0);
    }

    public void SetupAudioVideoPlayer(AudioClip clip)
    {
        if (clip == null) return;

        if (_videoPlayer == null)
            _videoPlayer = GetComponent<VideoPlayer>();

        _audioSourceVideo.clip = clip;
        _audioSourceVideo.time = (float)_videoPlayer.time;

        if (!_videoIsStoped)
            _audioSourceVideo.Play();

    }

    private void SetPlayTimeText()
    {
        _timeText.text = ConvertDoubleToTimeString(_videoPlayer.time);
    }

    private string ConvertDoubleToTimeString(double time)
    {
        int minutes = (int)(time / 60);
        int seconds = (int)(time % 60);

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void ClearRenderTexture()
    {
        Graphics.SetRenderTarget(_videoPlayer.targetTexture);
        GL.Clear(true, true, Color.clear);
        Graphics.SetRenderTarget(null);
    }

    private void SetButtonState(bool videoIsPlaying)
    {
        StopAndClearCoroutine(ref _closeBigBtnImageCoroutine);

        if (videoIsPlaying)
            HideBigPlayPauseBtn();
        else ShowBigPlayPauseBtn();
    }

    private void HideBigPlayPauseBtn()
    {
        SetPlayPauseBntImage(_playBtnPauseSprite);
        _closeBigBtnImageCoroutine = StartCoroutine(CloseBigBtnImage());
    }

    private void ShowBigPlayPauseBtn()
    {
        SetPlayPauseBntImage(_playBtnPlaySprite);
        _pauseBtnAnimator.Play(_showPauseBtnAnimHash);
        _playPauseBigBtnImage.gameObject.SetActive(true);
    }

    private void SetPlayPauseBntImage(Sprite BtnImg)
    {
        _playPauseBtn.image.sprite = BtnImg;
        _playPauseBigBtnImage.sprite = BtnImg;
    }

    private void StopAndClearCoroutine(ref Coroutine coroutine)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }

    private void SetButtonsListeners()
    {
        _playPauseBtn.onClick.AddListener(TogglePlayerPlayState);
        _playPauseBigBtn.onClick.AddListener(TogglePlayerPlayState);
        _rewindBtn.onClick.AddListener(Rewind);
        _fastForwardBtn.onClick.AddListener(FastForward);
    }

    private void RemoveButtonsListeners()
    {
        _playPauseBtn.onClick.RemoveAllListeners();
        _playPauseBigBtn.onClick.RemoveAllListeners();
        _rewindBtn.onClick.RemoveAllListeners();
        _fastForwardBtn.onClick.RemoveAllListeners();
    }

    private void SetupAnimHash()
    {
        _showAnimHash = Animator.StringToHash("ShowVideoControls");
        _hideAnimHash = Animator.StringToHash("HideVideoControls");
        _showPauseBtnAnimHash = Animator.StringToHash("ShowPauseVideoBtn");
        _hidePauseBtnAnimHash = Animator.StringToHash("HidePauseVideoBtn");
        _FastForwardAnimHash = Animator.StringToHash("FastForvardAnim");
        _RewindAnimHash = Animator.StringToHash("RewindAnim");
    }

    private IEnumerator CloseBigBtnImage()
    {
        yield return new WaitForSeconds(0.5f);

        _pauseBtnAnimator.Play(_hidePauseBtnAnimHash);
        yield return new WaitForSeconds(_closeBtnClip.length);

        _playPauseBigBtnImage.gameObject.SetActive(false);
        _closeBigBtnImageCoroutine = null;
    }

    private IEnumerator CloseControlsRoutine()
    {
        yield return new WaitForSeconds(_controlsTimeClose);

        _selectedLangiagePanel.gameObject.SetActive(false);
        _videoAnimator.Play(_hideAnimHash);
        _isShowingControls = false;
        _closeControlsCoroutine = null;
    }
}
