using Assets.SimpleLocalization.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ALMA.Language;

public class SelestLanhuageSound : MonoBehaviour
{
    [SerializeField] private MultiLanguage _multiLanguage;
    [SerializeField] private VideoPlayerController _videoPlayerController;
    [SerializeField] private Toggle _ru;
    [SerializeField] private Toggle _eng;
    [SerializeField] private Toggle _kzh;

    private void OnEnable()
    {
        _ru.onValueChanged.AddListener(SetRu);
        _eng.onValueChanged.AddListener(SetENG);
        _kzh.onValueChanged.AddListener(SetKZH);

        LoadDefaultLanguageSound();
    }

    private void LoadDefaultLanguageSound()
    {
        switch (_multiLanguage.CurrentLanguage)
        {
            case MultiLanguage.Languages.Russian:
                SetRu(true);
                break;
            case MultiLanguage.Languages.English:
                SetENG(true);
                break;
            case MultiLanguage.Languages.Kazakh:
                SetKZH(true);
                break;

        }
    }

    private void SetRu(bool active)
    {
        if (active)
            _videoPlayerController.SetupAudioVideoPlayer(_videoPlayerController.CurrentVideoInfo.AudioRU);
    }

    private void SetENG(bool active)
    {
        if (active)
            _videoPlayerController.SetupAudioVideoPlayer(_videoPlayerController.CurrentVideoInfo.AudioENG);
    }

    private void SetKZH(bool active)
    {
        if (active)
            _videoPlayerController.SetupAudioVideoPlayer(_videoPlayerController.CurrentVideoInfo.AudioKZX);
    }
}
