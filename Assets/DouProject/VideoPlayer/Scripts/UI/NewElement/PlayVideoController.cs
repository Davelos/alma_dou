using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG;
using DG.Tweening;

public class PlayVideoController : MonoBehaviour
{
    [SerializeField] private Button _playVideoBTN;
    [SerializeField] private VideoInfo _videoInfo;
    [SerializeField] private VideoPlayerController _videoPlayer;

    void Start()
    {
        _playVideoBTN.onClick.AddListener(PlayVideo);
    }

    private void OnDestroy()
    {
        _playVideoBTN.onClick.RemoveListener(PlayVideo);
    }

    private void PlayVideo()
    {
        _playVideoBTN.transform.DOScale(0.7f, 0.5f).SetAutoKill(false).OnComplete(() =>
        {
            _videoPlayer.OpenVideoPlayer(_videoInfo);

        }).From();
    }

}
