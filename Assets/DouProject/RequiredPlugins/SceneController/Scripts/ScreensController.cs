using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum TypeModeController
{
    Click,
    Swipe
}

[Serializable]
public class ScreenPanel
{
    public GameObject Screen;
    public Button Button;
    public UnityEvent ShowScreen;
    public UnityEvent HideScreen;
}

public class ScreensController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    [SerializeField] private TypeModeController _typeModeScreen;
    [SerializeField] [Range(1, 50)] private int _startNumberScreen;
    [SerializeField] private ScreenPanel[] _screens;
    [Space]
    [Header("Type Click Settings")]
    [SerializeField] private bool _isFade;
    [SerializeField] private Image _fadeImage;
    [SerializeField] [Range(0.2f, 2f)] private float _fadeOutDuration = 0.2f;
    [Space]
    [SerializeField] private Button _homeButtonForClick;
    [SerializeField] private Button _previousScreenButtonForClick;
    [SerializeField] private bool _isPrevScreenButton;
    [Space]
    [Header("Type Swipe Settings")]
    [SerializeField] private RectTransform _rectTtransformScreen;
    [SerializeField] private RectTransform _containerScreen;
    [SerializeField] [Range(0f, 150f)] private float _offsetScreen = 60f;
    [SerializeField] private bool _navigationButton;
    [SerializeField] private Button _previousScreenButtonForSwipe;
    [SerializeField] private Button _nextScreenButtonForSwipe;
    [Space]
    [SerializeField] [Range(150, 550)] private float _minSwipeDistance;
    [SerializeField] [Range(0.3f, 1f)] private float _scrollDuration;

    private ScreenPanel _currentScreen;

    //Type Click Propirties
    private List<ScreenPanel> _screensPanels = new List<ScreenPanel>();

    //Type Swipe Propirties
    private int _currentScreenIndex;
    private Vector3 _startScreenPosition;
    private Vector2 _dragStartPosition;

    private void Start()
    {
        if (_typeModeScreen == TypeModeController.Click)
            SetupScreensForClick();
        else if (_typeModeScreen == TypeModeController.Swipe)
            SetupScreensForSwipe();
    }

    private void OnDestroy()
    {
        if (_typeModeScreen == TypeModeController.Click)
            CleanupClickScreens();
        else if (_typeModeScreen == TypeModeController.Swipe && _navigationButton)
            CleanupSwipeScreens();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_typeModeScreen == TypeModeController.Swipe)
        {
            _dragStartPosition = eventData.position;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_typeModeScreen == TypeModeController.Swipe)
        {
            //if (eventData.delta.x < 0 && _currentScreenIndex < _screens.Length - 1 || eventData.delta.x > 0 && _currentScreenIndex > 0)
            _rectTtransformScreen.anchoredPosition = new Vector2(_rectTtransformScreen.anchoredPosition.x + eventData.delta.x, _rectTtransformScreen.anchoredPosition.y);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_typeModeScreen == TypeModeController.Swipe)
        {
            float dragDistance = _dragStartPosition.x - eventData.position.x;

            if (Mathf.Abs(dragDistance) >= _minSwipeDistance)
                NavigationScroll(Math.Sign(dragDistance));
            ScrollTo(_rectTtransformScreen, _startScreenPosition.x);
        }
    }

    #region Click methods
    private void SetupScreensForClick()
    {
        _homeButtonForClick?.onClick.AddListener(ReturnInMainScreen);

        if (_isPrevScreenButton)
            _previousScreenButtonForClick?.onClick.AddListener(PrevScreen);

        for (int x = 0; x < _screens.Length; x++)
        {
            int index = x;
            _screens[x].Button?.onClick.AddListener(() => ShowScreen(_screens[index], true));
            _screens[x].Screen?.SetActive(false);
        }

        _startNumberScreen = _startNumberScreen > _screens.Length - 1 ? _screens.Length - 1 : _startNumberScreen - 1;
        _currentScreen = _screens[_startNumberScreen];
        _currentScreen.Screen.SetActive(true);

        if (_isFade)
            StartCoroutine(FadeAnim());
    }

    private void CleanupClickScreens()
    {
        _homeButtonForClick?.onClick.RemoveListener(ReturnInMainScreen);

        if (_isPrevScreenButton)
            _previousScreenButtonForClick?.onClick.RemoveListener(PrevScreen);

        for (int x = 0; x < _screens.Length; x++)
            _screens[x].Button?.onClick.RemoveListener(() => ShowScreen(_screens[x], true));
    }

    private void ShowScreen(ScreenPanel screenPanel, bool newScreen)
    {
        if (_isFade)
            StartCoroutine(FadeAnim());

        if (newScreen)
            _screensPanels.Add(_currentScreen);

        _currentScreen.HideScreen?.Invoke();
        _currentScreen.Screen.SetActive(false);

        _currentScreen = screenPanel;
        _currentScreen.Screen.SetActive(true);
        _currentScreen.ShowScreen?.Invoke();

        CheckStateButton();
    }

    private void CheckStateButton()
    {
        if (_isPrevScreenButton)
        {
            if (_screensPanels.Count > 0 && !_previousScreenButtonForClick.gameObject.activeSelf && _isPrevScreenButton)
                _previousScreenButtonForClick.gameObject.SetActive(true);
            else if (_screensPanels.Count <= 0 && _previousScreenButtonForClick.gameObject.activeSelf && _isPrevScreenButton)
                _previousScreenButtonForClick.gameObject.SetActive(false);
        }

        if (_currentScreen != _screens[_startNumberScreen] && !_homeButtonForClick.gameObject.activeSelf)
            _homeButtonForClick.gameObject.SetActive(true);
        else if (_currentScreen == _screens[_startNumberScreen] && _homeButtonForClick.gameObject.activeSelf)
            _homeButtonForClick.gameObject.SetActive(false);
    }

    private void PrevScreen()
    {
        ScreenPanel prevScreen = _screensPanels[_screensPanels.Count - 1];
        _screensPanels.Remove(prevScreen);
        ShowScreen(prevScreen, false);
    }

    private void ReturnInMainScreen()
    {
        if (_isFade)
            StartCoroutine(FadeAnim());

        _screensPanels.Clear();
        ShowScreen(_screens[_startNumberScreen], false);
    }

    private IEnumerator FadeAnim()
    {
        StopCoroutine(nameof(FadeAnim));

        float alphaFade = 1.0f;
        float stack = 0;

        Color newColor = _fadeImage.color;
        newColor.a = alphaFade;
        _fadeImage.color = newColor;

        while (_fadeImage.color.a > 0)
        {
            stack += Time.deltaTime / _fadeOutDuration;
            newColor.a = Mathf.Lerp(1, 0, stack);
            _fadeImage.color = newColor;
            yield return null;
        }
    }
    # endregion Click methods

    #region Swipe methods
    private void SetupScreensForSwipe()
    {
        _startScreenPosition = _rectTtransformScreen.anchoredPosition;
        _currentScreenIndex = _startNumberScreen > _screens.Length - 1 ? _screens.Length - 1 : _startNumberScreen - 1;

        SetNavigBtnsListeners();
        SetupScreensPositions();

        _currentScreen = _screens[_currentScreenIndex];
        ScrollTo(_containerScreen, _currentScreenIndex);

        _currentScreen.ShowScreen?.Invoke();
    }

    private void SetNavigBtnsListeners()
    {
        if (_navigationButton)
        {
            _previousScreenButtonForSwipe?.onClick.AddListener(() => NavigationScroll(-1));
            _nextScreenButtonForSwipe?.onClick.AddListener(() => NavigationScroll(+1));
            _previousScreenButtonForSwipe?.gameObject.SetActive(true);
            _nextScreenButtonForSwipe?.gameObject.SetActive(true);
        }
    }

    private void SetupScreensPositions()
    {
        RectTransform currenScreen;
        RectTransform nextScreen;
        Vector2 positionScreen;
        float offsetX = Screen.currentResolution.width + _offsetScreen;

        for (int i = 0; i < _screens.Length; i++)
        {
            currenScreen = _screens[i].Screen.GetComponent<RectTransform>();
            currenScreen.gameObject.SetActive(true);

            if (i < _screens.Length - 1)
            {
                positionScreen = new Vector2(currenScreen.anchoredPosition.x + offsetX, 0);
                nextScreen = _screens[i + 1].Screen.GetComponent<RectTransform>();
                nextScreen.anchoredPosition = positionScreen;
            }
        }
    }

    private void CleanupSwipeScreens()
    {
        if (_navigationButton)
        {
            _previousScreenButtonForSwipe?.onClick.RemoveAllListeners();
            _nextScreenButtonForSwipe?.onClick.RemoveAllListeners();
        }
    }

    private void ScrollTo(RectTransform rectTransform, float targetIndex)
    {
        float targetPosition = -targetIndex * Screen.currentResolution.width;
        rectTransform.DOKill();
        rectTransform.DOAnchorPosX(targetPosition, _scrollDuration).SetEase(Ease.OutQuad);
    }

    private void NavigationScroll(int indexOffset)
    {
        int nextIndex = _currentScreenIndex + indexOffset;
        if (nextIndex > _screens.Length - 1 || nextIndex < 0) return;

        _currentScreen.HideScreen?.Invoke();
        _currentScreen = _screens[nextIndex];
        _currentScreen.ShowScreen?.Invoke();

        _currentScreenIndex = nextIndex;
        ScrollTo(_containerScreen, nextIndex);



}
    # endregion Swipe methods
}
