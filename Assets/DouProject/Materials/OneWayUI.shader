// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/One Way UI"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
        Pass {

    		Cull Back
    		Lighting Off
    		ZWrite Off
            ZTest [unity_GUIZTestMode]
    		Fog { Mode Off }
    		Blend SrcAlpha OneMinusSrcAlpha

    		CGPROGRAM
    //        #pragma target 2.0              //Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)
            #pragma vertex vert_img_cust    //The vertex shader is named 'vert'
            #pragma fragment frag           //The fragment shader is named 'frag'
            #include "UnityCG.cginc"        //Include Unity's predefined inputs and macros

    		sampler2D _MainTex;
    		fixed4 _Color;
    		
            struct v_in {
                float4 vertex : POSITION;
                half2 texcoord : TEXCOORD0;
                float4 color : COLOR;
            };
            
    		struct Input
    		{
    			float2 uv_MainTex;
    			fixed4 color;
    		};
            
            struct v2f_img_vert_color {
                float4 pos : SV_POSITION;
                half2 uv : TEXCOORD0;
                float4 diff : COLOR0;
            };
            
            v2f_img_vert_color vert_img_cust( v_in v )
            {
                v2f_img_vert_color o;
                o.pos = UnityObjectToClipPos (v.vertex);
                o.uv = v.texcoord;
                o.diff =  v.color;
                return o;
            }

    		fixed4 frag(v2f_img_vert_color IN) : COLOR {
    			fixed4 c = tex2D(_MainTex, IN.uv);
    			c.a *= IN.diff.a;
                return c;
    		}
            ENDCG
        }
    }
}
