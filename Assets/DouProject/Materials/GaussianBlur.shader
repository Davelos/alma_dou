// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader"Custom/UI Blur Gaussian" {
    Properties{
        [Toggle(IS_BLUR_ALPHA_MASKED)] _IsAlphaMasked("Image Alpha Masks Blur", Float) = 1
        [Toggle] _IsAddColor("Add Color", Float) = 1
        _Color("Tint", Color) = (1,1,1,1)
        _MainTex("Texture (RGB)", 2D) = "white" {}

        _Size("Blur Size", Range(0,0.5)) = 0
		[KeywordEnum(Low, Medium, High)] _Samples ("Sample amount", Float) = 0
		[Toggle(GAUSS)] _Gauss ("Gaussian Blur", float) = 0
		[PowerSlider(3)]_StandardDeviation("Standard Deviation (Gauss only)", Range(0.00, 0.3)) = 0.02

        [HideInInspector]_StencilComp("Stencil Comparison", Float) = 8
        [HideInInspector]_Stencil("Stencil ID", Float) = 0
        [HideInInspector]_StencilOp("Stencil Operation", Float) = 0
        [HideInInspector]_StencilWriteMask("Stencil Write Mask", Float) = 255
        [HideInInspector]_StencilReadMask("Stencil Read Mask", Float) = 255
        [HideInInspector]_ColorMask("Color Mask", Float) = 15
        [HideInInspector]_UseUIAlphaClip("Use Alpha Clip", Float) = 0
    }

        Category{

            // We must be transparent, so other objects are drawn before this one.
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Stencil
            {
                Ref[_Stencil]
                Comp[_StencilComp]
                Pass[_StencilOp]
                ReadMask[_StencilReadMask]
                WriteMask[_StencilWriteMask]
            }
            
            Cull Off
            Lighting Off
            ZWrite Off
            ZTest[unity_GUIZTestMode]
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask[_ColorMask]

            SubShader {

                // Horizontal blur
                GrabPass {
                    Tags
                    {
                        "LightMode" = "Always"
                    }
                }
                Pass {
                    Tags { "LightMode" = "Always" }

                    CGPROGRAM
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma fragmentoption ARB_precision_hint_fastest
                    #pragma multi_compile __ UNITY_UI_ALPHACLIP
                    #pragma shader_feature GAUSS
                    #pragma multi_compile _SAMPLES_LOW _SAMPLES_MEDIUM _SAMPLES_HIGH
                    #include "UnityCG.cginc"

                    struct appdata_t {
                        float4 vertex : POSITION;
                        float2 texcoord: TEXCOORD0;
                    };

                    struct v2f {
                        float4 vertex : POSITION;
                        float4 uvgrab : TEXCOORD0;
                        float2 uvmain : TEXCOORD2;
                    };

                    float4 _MainTex_ST;

                    v2f vert(appdata_t v) {
                        v2f o;
                        o.vertex = UnityObjectToClipPos(v.vertex);
                        #if UNITY_UV_STARTS_AT_TOP
                        float scale = -1.0;
                        #else
                        float scale = 1.0;
                        #endif
                        o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
                        o.uvgrab.zw = o.vertex.zw;
                        o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
                        return o;
                    }

                    sampler2D _MainTex;
                    sampler2D _GrabTexture;
                    float4 _GrabTexture_TexelSize;
                    float _Size;
			        float _StandardDeviation;

			        #define PI 3.14159265359
			        #define E 2.71828182846

		            #if _SAMPLES_LOW
			            #define SAMPLES 10
		            #elif _SAMPLES_MEDIUM
			            #define SAMPLES 30
		            #else
			            #define SAMPLES 100
		            #endif

                    half4 frag(v2f i) : COLOR {
                        #if GAUSS
				            //failsafe so we can use turn off the blur by setting the deviation to 0
				            if(_StandardDeviation == 0)
				            return tex2D(_GrabTexture, i.uvgrab);
			            #endif
				            //calculate aspect ratio
				            float invAspect = _ScreenParams.y / _ScreenParams.x;
				            //init color variable
				            float4 col = 0;
			            #if GAUSS
				            float sum = 0;
			            #else
				            float sum = SAMPLES;
			            #endif
				            //iterate over blur samples
				            for(float index = 0; index < SAMPLES; index++){
					            //get the offset of the sample
					            float offset = (index/(SAMPLES-1) - 0.5) * _Size * invAspect;
					            //get uv coordinate of sample
					            float2 uv = i.uvgrab + float2(offset, 0);
				            #if !GAUSS
					            //simply add the color if we don't have a gaussian blur (box)
					            col += tex2D(_GrabTexture, uv);
				            #else
					            //calculate the result of the gaussian function
					            float stDevSquared = _StandardDeviation*_StandardDeviation;
					            float gauss = (1 / sqrt(2*PI*stDevSquared)) * pow(E, -((offset*offset)/(2*stDevSquared)));
					            //add result to sum
					            sum += gauss;
					            //multiply color with influence from gaussian function and add it to sum color
					            col += tex2D(_GrabTexture, uv) * gauss;
				            #endif
				            }
				            //divide the sum of values by the amount of samples
				            col = col / sum;
				            return col;
                    }
                    ENDCG
                }

            // Vertical blur
            GrabPass {
                Tags
                {
                    "LightMode" = "Always"
                }
            }

            Pass {
                Tags { "LightMode" = "Always" }

                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma fragmentoption ARB_precision_hint_fastest
                #pragma multi_compile __ UNITY_UI_ALPHACLIP
                #pragma shader_feature GAUSS
                #pragma multi_compile _SAMPLES_LOW _SAMPLES_MEDIUM _SAMPLES_HIGH
                #include "UnityCG.cginc"

                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 texcoord: TEXCOORD0;
                };

                struct v2f {
                    float4 vertex : POSITION;
                    float4 uvgrab : TEXCOORD0;
                    float2 uvmain : TEXCOORD2;
                };

                float4 _MainTex_ST;

                v2f vert(appdata_t v) {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    #if UNITY_UV_STARTS_AT_TOP
                    float scale = -1.0;
                    #else
                    float scale = 1.0;
                    #endif
                    o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
                    o.uvgrab.zw = o.vertex.zw;
                    o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }

                sampler2D _MainTex;
                sampler2D _GrabTexture;
                float4 _GrabTexture_TexelSize;
                float _Size;
			    float _StandardDeviation;

			    #define PI 3.14159265359
			    #define E 2.71828182846

		        #if _SAMPLES_LOW
			        #define SAMPLES 10
		        #elif _SAMPLES_MEDIUM
			        #define SAMPLES 30
		        #else
			        #define SAMPLES 100
		        #endif

                half4 frag(v2f i) : COLOR {
                    #if GAUSS
				        //failsafe so we can use turn off the blur by setting the deviation to 0
				        if(_StandardDeviation == 0)
				        return tex2D(_GrabTexture, i.uvgrab);
			        #endif
				        //init color variable
				        float4 col = 0;
			        #if GAUSS
				        float sum = 0;
			        #else
				        float sum = SAMPLES;
			        #endif
				        //iterate over blur samples
				        for(float index = 0; index < SAMPLES; index++){
					        //get the offset of the sample
					        float offset = (index/(SAMPLES-1) - 0.5) * _Size;
					        //get uv coordinate of sample
					        float2 uv = i.uvgrab + float2(0, offset);
				        #if !GAUSS
					        //simply add the color if we don't have a gaussian blur (box)
					        col += tex2D(_GrabTexture, uv);
				        #else
					        //calculate the result of the gaussian function
					        float stDevSquared = _StandardDeviation*_StandardDeviation;
					        float gauss = (1 / sqrt(2*PI*stDevSquared)) * pow(E, -((offset*offset)/(2*stDevSquared)));
					        //add result to sum
					        sum += gauss;
					        //multiply color with influence from gaussian function and add it to sum color
					        col += tex2D(_GrabTexture, uv) * gauss;
				        #endif
				        }
				        //divide the sum of values by the amount of samples
				        col = col / sum;
				        return col;
                }
                ENDCG
                }

                // Distortion
                GrabPass {
                    Tags
                    {
                        "LightMode" = "Always"
                    }
                }
                Pass {
                    Tags { "LightMode" = "Always" }

                    CGPROGRAM
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma fragmentoption ARB_precision_hint_fastest
                    #pragma multi_compile __ IS_BLUR_ALPHA_MASKED
                    #pragma multi_compile __ IS_SPRITE_VISIBLE
                    #pragma multi_compile __ UNITY_UI_ALPHACLIP
                    #include "UnityCG.cginc"
                    #include "UnityUI.cginc"

                    struct appdata_t {
                        float4 color    : COLOR;
                        float4 vertex : POSITION;
                        float2 texcoord: TEXCOORD0;
                    };

                    struct v2f {
                        float4 color    : COLOR;
                        float4 vertex : POSITION;
                        float4 uvgrab : TEXCOORD0;
                        float2 uvbump : TEXCOORD1;
                        float2 uvmain : TEXCOORD2;
                    };

                    float4 _Color;
                    float _BumpAmt;
                    float4 _BumpMap_ST;
                    float4 _MainTex_ST;

                    v2f vert(appdata_t v) {
                        v2f o;
                        o.vertex = UnityObjectToClipPos(v.vertex);
                        #if UNITY_UV_STARTS_AT_TOP
                        float scale = -1.0;
                        #else
                        float scale = 1.0;
                        #endif
                        o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
                        o.uvgrab.zw = o.vertex.zw;
                        o.uvbump = TRANSFORM_TEX(v.texcoord, _BumpMap);
                        o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
                        o.color = v.color * _Color;
                        return o;
                    }

                    #define BLEND_OVERLAY(a, b) b <= 0.5 ? (2*b)*a : (1 - (1-2*(b-0.5)) * (1-a)) 
                    half3 overlayBlend(half3 back, half3 front)
                    {
                        return
                            half3(
                                BLEND_OVERLAY(back.r, front.r),
                                BLEND_OVERLAY(back.g, front.g),
                                BLEND_OVERLAY(back.b, front.b)
                            );
                    }

                    sampler2D _GrabTexture;
                    float4 _GrabTexture_TexelSize;
                    sampler2D _BumpMap;
                    sampler2D _MainTex;
                    float _IsAddColor;
                    float4 _ClipRect;

                    half4 frag(v2f i) : COLOR {
                        float4 ver = UnityObjectToClipPos(i.vertex);
    
                        // calculate perturbed coordinates
                        half2 bump = UnpackNormal(tex2D(_BumpMap, i.uvbump)).rg; // we could optimize this by just reading the x  y without reconstructing the Z
                        float2 offset = bump * _BumpAmt * _GrabTexture_TexelSize.xy;
                        i.uvgrab.xy = offset * i.uvgrab.z + i.uvgrab.xy;

                    #ifdef UNITY_COLORSPACE_GAMMA
                        float4 color =i.color;
                    #else
                        float4 color = float4(LinearToGammaSpace(i.color.rgb), i.color.a);
                    #endif
        
                    float4 pixel = tex2D(_MainTex, i.uvmain);
    
                    #if IS_BLUR_ALPHA_MASKED
                        float visibility = color.a*pixel.a;
                    #else
                        float visibility = 1;
                    #endif
    
                        visibility *= UnityGet2DClipping(ver.xy, _ClipRect);
    
                        half4 col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.uvgrab));

                        half4 tint = pixel * color;
   // return col;
                        return half4(overlayBlend(col.rgb, tint.rgb), col.a * visibility);
                    }
                    ENDCG
                }
            }
        }
}