using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public static class Utils
{
    public static bool IsScreenPosInScreenBounds(Vector3 screenPosition, Camera camera = null)
    {
        if (camera == null) camera = Camera.main;

        Vector3 viewportPosition = camera.ScreenToViewportPoint(screenPosition);
        return IsViewPortPosInScreenBounds(viewportPosition);
    }

    public static bool IsWorldPosInScreenBounds(Vector3 worldPosition, Camera camera = null)
    {
        if (camera == null) camera = Camera.main;

        Vector3 viewportPosition = camera.WorldToViewportPoint(worldPosition);
        return IsViewPortPosInScreenBounds(viewportPosition);
    }

    public static bool IsViewPortPosInScreenBounds(Vector3 viewportPosition)
    {
        return viewportPosition.x >= 0 && viewportPosition.x <= 1 &&
               viewportPosition.y >= 0 && viewportPosition.y <= 1;
    }

    public static void RefreshLayout(this RectTransform transform, bool hard = false)
    {
        if (hard) LayoutRebuilder.ForceRebuildLayoutImmediate(transform); // Принудительно перестраиваем макет
        else LayoutRebuilder.MarkLayoutForRebuild(transform); // Помечаем макет для перестроения
    }

    public static IEnumerator InvokeOnEndFrame(UnityAction action)
    {
        yield return new WaitForEndOfFrame();

        action.Invoke();
    }

    public static void SetColorAlpha(MaskableGraphic targetGraphic, float newAlpha)
    {
        var backColor = targetGraphic.color;
        backColor.a = newAlpha;
        targetGraphic.color = backColor;
    }

    public static bool IsPointerOverGameObject()
    {
        if (Input.touches.Length > 0)
        {
            return EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId);
        }
        return EventSystem.current.IsPointerOverGameObject();
    }

    public static Texture2D LoadImage(string imagePath)
    {
        var data = File.ReadAllBytes(imagePath);
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(data);

        return texture;
    }

    public static void GetMinMaxScreenPoint(RectTransform rectTransform, out Vector3 minScreen, out Vector3 maxScreen, Camera camera = null)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);

        if (camera == null) camera = Camera.main;
        minScreen = camera.WorldToScreenPoint(corners[0]);
        maxScreen = camera.WorldToScreenPoint(corners[2]);
    }

    public static void ShuffleArray<T>(this T[] array)
    {
        for (int i = array.Length - 1; i > 0; i--)
        {
            int j = Random.Range(0, i + 1);
            (array[i], array[j]) = (array[j], array[i]);
        }
    }

    internal static float UnlerpV3(Vector3 a, Vector3 b, Vector3 lerp)
    {
        Vector3 aToB = b - a;
        float dotProduct = Vector3.Dot(lerp - a, aToB);
        float bToAMagnitudeSquared = aToB.sqrMagnitude;

        if (bToAMagnitudeSquared == 0f)
            return 0f;

        return Mathf.Clamp01(dotProduct / bToAMagnitudeSquared);
    }
}
