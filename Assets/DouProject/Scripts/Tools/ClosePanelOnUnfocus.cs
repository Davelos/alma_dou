using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePanelOnUnfocus : MonoBehaviour
{
    [SerializeField] private Camera _cameraMain;

    private IActivatable _activatable;

    private void Awake()
    {
        if (_activatable == null)
            _activatable = GetComponent<IActivatable>();
    }

    private void OnValidate()
    {
        if (_cameraMain == null)
            _cameraMain = Camera.main;
    }

    private void Update()
    {
        if (_activatable.IsActive() && Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Input.mousePosition;
            if (!RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform, mousePos, _cameraMain))
                _activatable.SetActive(false);
        }
    }
}
