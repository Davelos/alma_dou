using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLessonConfig", menuName = "Settings/Lessons/Lesson Config", order = 0)]
public class LessonConfig : ScriptableObject
{
    public int StartPartID = 0;
}
