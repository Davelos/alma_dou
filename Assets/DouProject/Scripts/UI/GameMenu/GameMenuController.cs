using Assets.SimpleLocalization.Scripts;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;
using ALMA.Language;
using ALMA.SoundService;

public class GameMenuController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    [field: SerializeField] public float OpenCloseAnimDur { get; private set; } = 0.5f;
    [field: SerializeField] public RectTransform Window { get; private set; }
    [field: SerializeField] public RectTransform UpPanel { get; private set; }
    [field: SerializeField] public RectTransform DownPanel { get; private set; }
    [field: SerializeField] public MaskableGraphic MenuBackground { get; private set; }
    [field: SerializeField] public CanvasGroup BtnsGroup { get; private set; }

    public RectTransform OpenBtn => _openMenuBtn.transform as RectTransform;
    public RectTransform LanguageBtn => _languageGroup.transform as RectTransform;
    public RectTransform DrawingBtn => _drawingBtn.transform as RectTransform;
    public RectTransform ScreenshotBtn => _screenshotBtn.transform as RectTransform;
    public RectTransform ToggleSoundBtn => _toggleSoundBtn.transform as RectTransform;
    public RectTransform CompanyInfoBtn => _companyInfoBtn.transform as RectTransform;
    public RectTransform ExitBtn => _exitBtn.transform as RectTransform;


    [Header("LocalizeSettings")]
    [SerializeField] private MultiLanguage _multiLanguage;
    [SerializeField] private string _soundBtnOnKey;
    [SerializeField] private string _soundBtnOffKey;
    [Space, Header("Buttons settings")]
    [SerializeField] private Button _openMenuBtn;
    [SerializeField] private Button _closeMenuBtn;
    [SerializeField] private LanguageGroup _languageGroup;
    [SerializeField] private Button _drawingBtn;
    [SerializeField] private Button _screenshotBtn;
    [SerializeField] private Button _toggleSoundBtn;
    [SerializeField] private TextMeshProUGUI _toggleSoundBtnText;
    [SerializeField] private Button _companyInfoBtn;
    [SerializeField] private Button _exitBtn;
    [Space]
    [SerializeField] private DrawnPanelController _drawPanel;
    [SerializeField] private AboutAsPanel _aboutAsPanel;

    private GameMenuAnimatorBase _animator;
    private SoundGroup _musicGroup;

    [Inject]
    public void Construct(Screenshoter screenshoter, SoundGroupsController soundGroupsController)
    {
        _openMenuBtn.onClick.AddListener(OpenGameMenu);
        _closeMenuBtn.onClick.AddListener(() => CloseGameMenu());
        _drawingBtn.onClick.AddListener(DrawPanelOpen);
        _screenshotBtn.onClick.AddListener(screenshoter.SaveScreenshot);
        _toggleSoundBtn.onClick.AddListener(ToggleSound);
        _languageGroup.OnLanguageBtnChanged.AddListener(_multiLanguage.SetLanguage);
        _companyInfoBtn.onClick.AddListener(OpenCompanyInfoPanel);
        _exitBtn.onClick.AddListener(ExitApplicationMessageBox);

        _animator = new GameMenuAnimatorDO(this);
        _musicGroup = soundGroupsController.GetGroup(SoundGroups.Music);
    }

    private void Start()
    {
        LocalizeSoundBtn();
        LocalizationManager.OnLocalizationChanged += LocalizeSoundBtn;
    }

    private void OnDestroy()
    {
        _openMenuBtn.onClick.RemoveAllListeners();
        _closeMenuBtn.onClick.RemoveAllListeners();
        _drawingBtn.onClick.RemoveAllListeners();
        _screenshotBtn.onClick.RemoveAllListeners();
        _toggleSoundBtn.onClick.RemoveAllListeners();
        _exitBtn.onClick.RemoveAllListeners();

        LocalizationManager.OnLocalizationChanged -= LocalizeSoundBtn;
    }

    public void OnBeginDrag(PointerEventData eventData) { }

    public void OnEndDrag(PointerEventData eventData) { }

    public void OnDrag(PointerEventData eventData) { }

    public void DrawPanelOpen()
    {
        _drawPanel.Open();
        CloseGameMenu(false);
    }

    public void DrawPanelClose()
    {
        _drawPanel.Close();
        OpenGameMenu();
    }

    private void OpenGameMenu()
    {
        gameObject.SetActive(true);
        _openMenuBtn.interactable = false;
        _animator.OpenAnim().
            OnComplete(() => _closeMenuBtn.interactable = true);
    }

    public void CloseGameMenu(bool showOpenBtn = true)
    {
        _closeMenuBtn.interactable = false;
        _animator.CloseAnim(showOpenBtn).
            OnComplete(() =>
        {
            _openMenuBtn.interactable = true;
            gameObject.SetActive(false);
        });
    }

    public void SwapActionForExitBtn(Action newBtnAction = null)
    {
        newBtnAction ??= ExitApplicationMessageBox;
        _exitBtn.onClick.RemoveAllListeners();
        _exitBtn.onClick.AddListener(newBtnAction.Invoke);
    }

    private void ToggleSound()
    {
        if (_musicGroup.SoundOn)
            _musicGroup.TurnOff();
        else _musicGroup.TurnOn();

        LocalizeSoundBtn();
    }

    private void LocalizeSoundBtn()
    {
        string localizeKey = _musicGroup.SoundOn ? _soundBtnOnKey : _soundBtnOffKey;
        _toggleSoundBtnText.text = LocalizationManager.Localize(localizeKey);
    }

    private void OpenCompanyInfoPanel()
    {
        _aboutAsPanel.SetActive(true);
        CloseGameMenu();
    }

    public void ExitApplicationMessageBox()
    {
        VolumeBox.Utils.MessageBox.Show(new VolumeBox.Utils.MessageBoxData
        {
            OkAction = Application.Quit,
            CancelAction = null,


        });
    }
}
