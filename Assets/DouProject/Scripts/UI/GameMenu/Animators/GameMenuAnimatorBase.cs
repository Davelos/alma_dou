﻿using DG.Tweening;
using System;
using Zenject;

public abstract class GameMenuAnimatorBase
{
    protected GameMenuController _gameMenu;

    private Sequence _openAnim;
    private Sequence _closeAnim;

    public GameMenuAnimatorBase(GameMenuController gameMenu)
    {
        _gameMenu = gameMenu;
    }

    public Sequence OpenAnim()
    {
        CreateOpenAnim();
        OpenAnimPrepare();
        _openAnim.OnComplete(() => { }).Play();
        return _openAnim;
    }

    public Sequence CloseAnim(bool showOpenBtn = true)
    {
        CreateCloseAnim(showOpenBtn);
        CloseAnimPrepare();
        _closeAnim.OnComplete(() => { }).Play();
        return _closeAnim;
    }

    protected abstract void SetupOpenAnim(Sequence openAnim);

    protected abstract void SetupCloseAnim(Sequence closeAnim, bool showOpenBtn);

    protected abstract void OpenAnimPrepare();

    protected abstract void CloseAnimPrepare();

    private void CreateOpenAnim()
    {
        _openAnim = DOTween.Sequence(_gameMenu).Pause();
        SetupOpenAnim(_openAnim);
    }

    private void CreateCloseAnim(bool showOpenBtn)
    {
        _closeAnim = DOTween.Sequence(_gameMenu).Pause();
        SetupCloseAnim(_closeAnim, showOpenBtn);
    }
}
