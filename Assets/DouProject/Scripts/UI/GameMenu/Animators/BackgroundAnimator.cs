﻿using DG.Tweening;
using UnityEngine;

public class BackgroundAnimator : MonoBehaviour
{
    [SerializeField] private float _openBtnAnimDur = 2f;
    [SerializeField] private float _openBtnAnimInterval = 7f;
    [SerializeField] private float _openBtnAnimMaxScale = 1.2f;
    [SerializeField] private float _openBtnAnimMinScale = 0.8f;
    [SerializeField] private RectTransform _openMenuBtn;

    private Sequence _openBtnAnim;
    private float _lastAnimTime;

    public void Awake()
    {
        CreateOpenBtnAnim();
        _lastAnimTime = Time.time;
    }

    public void FixedUpdate()
    {
        if (Time.time - _lastAnimTime < _openBtnAnimInterval) return;

        if (!_openBtnAnim.IsPlaying())
            _openBtnAnim.Restart();
    }

    private void CreateOpenBtnAnim()
    {
        _openBtnAnim = DOTween.Sequence(_openMenuBtn).SetAutoKill(false).Pause();

        float stageAnimDur = _openBtnAnimDur * 0.25f;
        _openBtnAnim.Append(_openMenuBtn.DOScale(_openBtnAnimMinScale, stageAnimDur * 0.5f));
        _openBtnAnim.Append(_openMenuBtn.DOScale(_openBtnAnimMaxScale, stageAnimDur));
        _openBtnAnim.Append(_openMenuBtn.DOScale(_openBtnAnimMinScale, stageAnimDur));
        _openBtnAnim.Append(_openMenuBtn.DOScale(_openBtnAnimMaxScale, stageAnimDur));
        _openBtnAnim.Append(_openMenuBtn.DOScale(1f, stageAnimDur * 0.5f));

        _openBtnAnim.OnComplete(() => _lastAnimTime = Time.time);
    }
}
