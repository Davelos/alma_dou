﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuAnimatorDO : GameMenuAnimatorBase
{
    private float _startBackgroundAlpha;
    private Vector2 _startOpenBtnPos;
    private Vector2 _startMenuBtnsPos;

    public GameMenuAnimatorDO(GameMenuController gameMenu) : base(gameMenu)
    {
        _startBackgroundAlpha = _gameMenu.MenuBackground.color.a;
        _startOpenBtnPos = _gameMenu.OpenBtn.anchoredPosition;
        _startMenuBtnsPos = _gameMenu.LanguageBtn.anchoredPosition;
    }

    protected override void SetupOpenAnim(Sequence openAnim)
    {
        float stageAnimDur = _gameMenu.OpenCloseAnimDur * 0.5f;

        openAnim.Append(_gameMenu.OpenBtn.DOAnchorPosY(-_gameMenu.OpenBtn.sizeDelta.y, stageAnimDur));

        openAnim.Insert(0, _gameMenu.Window.DOAnchorPosY(0f, stageAnimDur));
        openAnim.Insert(0, _gameMenu.UpPanel.DOAnchorPosY(0f, stageAnimDur));
        openAnim.Insert(0, _gameMenu.DownPanel.DOAnchorPosY(0f, stageAnimDur));
        openAnim.Insert(0, _gameMenu.MenuBackground.DOFade(_startBackgroundAlpha, stageAnimDur));

        RectTransform[] rectsBns = new RectTransform[]
        { _gameMenu.LanguageBtn, _gameMenu.DrawingBtn, _gameMenu.ScreenshotBtn,
            _gameMenu.ToggleSoundBtn, _gameMenu.CompanyInfoBtn, _gameMenu.ExitBtn
        };

        stageAnimDur /= rectsBns.Length;
        float halfStageAnimDur = stageAnimDur * 0.5f;
        float extendedStageAnimDur = stageAnimDur * 1.5f;
        float atPos = openAnim.Duration() - halfStageAnimDur;

        foreach (var rt in rectsBns)
        {
            openAnim.Insert(atPos, rt.DOAnchorPosX(_startMenuBtnsPos.x, extendedStageAnimDur));
            atPos += halfStageAnimDur;
        }
    }

    protected override void SetupCloseAnim(Sequence closeAnim, bool showOpenBtn)
    {
        float stageAnimDur = _gameMenu.OpenCloseAnimDur * 0.5f;

        closeAnim.Append(_gameMenu.BtnsGroup.DOFade(0, stageAnimDur));

        float secondStageTime = closeAnim.Duration();
        closeAnim.Insert(secondStageTime, _gameMenu.Window.DOAnchorPosY(-_gameMenu.Window.sizeDelta.y, stageAnimDur));
        closeAnim.Insert(secondStageTime + 0.1f, _gameMenu.UpPanel.DOAnchorPosY(_gameMenu.UpPanel.sizeDelta.y, stageAnimDur));
        closeAnim.Insert(secondStageTime + 0.1f, _gameMenu.DownPanel.DOAnchorPosY(-_gameMenu.DownPanel.sizeDelta.y, stageAnimDur));
        closeAnim.Insert(secondStageTime, _gameMenu.MenuBackground.DOFade(0, stageAnimDur));

        if (showOpenBtn)
            closeAnim.Append(_gameMenu.OpenBtn.DOAnchorPosY(_startOpenBtnPos.y, stageAnimDur));////////////////
    }

    protected override void OpenAnimPrepare()
    {
        _gameMenu.BtnsGroup.alpha = 1f;
        _gameMenu.OpenBtn.anchoredPosition = _startOpenBtnPos;

        _gameMenu.LanguageBtn.anchoredPosition = new Vector2(-_gameMenu.LanguageBtn.sizeDelta.x, _gameMenu.LanguageBtn.anchoredPosition.y);
        _gameMenu.DrawingBtn.anchoredPosition = new Vector2(-_gameMenu.DrawingBtn.sizeDelta.x, _gameMenu.DrawingBtn.anchoredPosition.y);
        _gameMenu.ScreenshotBtn.anchoredPosition = new Vector2(-_gameMenu.ScreenshotBtn.sizeDelta.x, _gameMenu.ScreenshotBtn.anchoredPosition.y);
        _gameMenu.ToggleSoundBtn.anchoredPosition = new Vector2(-_gameMenu.ToggleSoundBtn.sizeDelta.x, _gameMenu.ToggleSoundBtn.anchoredPosition.y);
        _gameMenu.CompanyInfoBtn.anchoredPosition = new Vector2(-_gameMenu.CompanyInfoBtn.sizeDelta.x, _gameMenu.CompanyInfoBtn.anchoredPosition.y);
        _gameMenu.ExitBtn.anchoredPosition = new Vector2(-_gameMenu.ExitBtn.sizeDelta.x, _gameMenu.ExitBtn.anchoredPosition.y);

        _gameMenu.Window.anchoredPosition = new Vector2(0f, -_gameMenu.Window.sizeDelta.y);
        _gameMenu.UpPanel.anchoredPosition = new Vector2(0f, _gameMenu.UpPanel.sizeDelta.y);
        _gameMenu.DownPanel.anchoredPosition = new Vector2(0f, -_gameMenu.DownPanel.sizeDelta.y);
        Utils.SetColorAlpha(_gameMenu.MenuBackground, 0f);
    }

    protected override void CloseAnimPrepare()
    {
        _gameMenu.BtnsGroup.alpha = 1f;
        _gameMenu.Window.anchoredPosition = Vector2.zero;
        _gameMenu.UpPanel.anchoredPosition = Vector2.zero;
        _gameMenu.DownPanel.anchoredPosition = Vector2.zero;
        _gameMenu.OpenBtn.anchoredPosition = new Vector2(_startOpenBtnPos.x, -_gameMenu.OpenBtn.sizeDelta.y);
        Utils.SetColorAlpha(_gameMenu.MenuBackground, _startBackgroundAlpha);
    }
}