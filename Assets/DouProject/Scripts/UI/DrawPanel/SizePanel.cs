using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SizePanel : MonoBehaviour, IActivatable
{
    public class SizePanelEvent : UnityEvent<float> { }
    public SizePanelEvent OnSizeChanged => _onSizeChanged;
    public float ActiveSize
    {
        get
        {
            if (_activeSize < 0)
            {
                int id = _sizeToggles.FindIndex(toggle => toggle.isOn);
                _activeSize = _sizes[id];
            }

            return _activeSize;
        }
    }

    [SerializeField] private float _openCloseAnimDur = 0.5f;
    [SerializeField] private List<Toggle> _sizeToggles;
    [SerializeField] private List<float> _sizes;
    [SerializeField] private ToggleGroup _toggleGroup;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Color _selectedColor = Color.blue;
    [SerializeField] private RectTransform _rectTransform;

    private SizePanelEvent _onSizeChanged = new();
    private bool _isActive;

    private Sequence _closeAnim;
    private Sequence _openAnim;
    private float _activeSize = -1;

    private void OnDestroy()
    {
        foreach (var toggle in _sizeToggles)
            toggle.onValueChanged.RemoveAllListeners();
    }

    private void OnValidate()
    {
        if (_toggleGroup == null)
            _toggleGroup = GetComponent<ToggleGroup>();
        if (_canvasGroup == null)
            _canvasGroup = GetComponent<CanvasGroup>();
        if (_rectTransform == null)
            _rectTransform = GetComponent<RectTransform>();
    }

    public void Init(UnityAction<float> call = null)
    {
        if (call != null) _onSizeChanged.AddListener(call);

        for (int i = 0; i < _sizeToggles.Count; i++)
        {
            int id = i;
            Toggle toggle = _sizeToggles[id];
            toggle.onValueChanged.AddListener((isActive) => OnValueChanged(isActive, _sizes[id], toggle.image));

            if (toggle.isOn)
                toggle.image.color = _selectedColor;
        }

        CreateCloseAnim();
        CreateOpenAnim();
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            OpenAnim();
        }
        else
        {
            CloseAnim().
                OnComplete(() => gameObject.SetActive(false));
        }
        _isActive = active;
    }

    public bool IsActive() => _isActive;

    private void OnValueChanged(bool isActive, float newSize, MaskableGraphic targetGraphic)
    {
        if (isActive)
        {
            _activeSize = newSize;
            _onSizeChanged?.Invoke(newSize);
            targetGraphic.color = _selectedColor;
            SetActive(false);
        }
        else
        {
            targetGraphic.color = Color.white;
        }
    }

    private void CreateCloseAnim()
    {
        _closeAnim = DOTween.Sequence(transform).SetAutoKill(false).Pause();

        _closeAnim.Append(_rectTransform.DOScale(0.7f, _openCloseAnimDur));
        _closeAnim.Insert(0, _canvasGroup.DOFade(0, _openCloseAnimDur));
    }

    private void CreateOpenAnim()
    {
        _openAnim = DOTween.Sequence(transform).SetAutoKill(false).Pause();

        _openAnim.Append(_rectTransform.DOScale(1f, _openCloseAnimDur));
        _openAnim.Insert(0, _canvasGroup.DOFade(1, _openCloseAnimDur));
    }

    private Sequence OpenAnim()
    {
        if (_closeAnim.IsPlaying())
            _closeAnim.Pause();

        _rectTransform.localScale = Vector3.one * 0.7f;
        _canvasGroup.alpha = 0f;

        _openAnim.OnComplete(null).Restart();
        return _openAnim;
    }

    private Sequence CloseAnim()
    {
        if (_openAnim.IsPlaying())
            _openAnim.Pause();

        _rectTransform.localScale = Vector3.one;
        _canvasGroup.alpha = 1f;

        _closeAnim.OnComplete(null).Restart();
        return _closeAnim;
    }
}
