using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class ColorPanel : MonoBehaviour, IActivatable
{
    public class ColorPanelEvent : UnityEvent<Color> { }
    public ColorPanelEvent OnColorChanged => _onColorChanged;
    public Color ActiveColor
    {
        get
        {
            if (_activeColor == Color.clear)
            {
                var toggle = _colorToggles.Find(toggle => toggle.isOn);
                if (toggle == null)
                {
                    Debug.LogWarning("Add ToggleGroup to toggles parent!");
                    _activeColor = Color.clear;
                }
                else
                    _activeColor = toggle.image.color;
            }

            return _activeColor;
        }
    }

    [SerializeField] private bool HideAfterColorChange = true;
    [SerializeField] private float _openCloseAnimDur = 0.5f;
    [SerializeField] private List<Toggle> _colorToggles;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private RectTransform _rectTransform;

    private ColorPanelEvent _onColorChanged = new();
    private bool _isActive;

    private Sequence _closeAnim;
    private Sequence _openAnim;
    private Color _activeColor = Color.clear;

    private void OnDestroy()
    {
        foreach (var toggle in _colorToggles)
            toggle.onValueChanged.RemoveAllListeners();
    }

    private void OnValidate()
    {
        if (_canvasGroup == null)
            _canvasGroup = GetComponent<CanvasGroup>();
        if (_rectTransform == null)
            _rectTransform = GetComponent<RectTransform>();
    }

    public void Init(UnityAction<Color> call = null)
    {
        if (call != null) _onColorChanged.AddListener(call);

        foreach (var toggle in _colorToggles)
        {
            toggle.onValueChanged.AddListener((isActive) => OnValueChanged(isActive, toggle.image.color));
        }

        CreateCloseAnim();
        CreateOpenAnim();
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            OpenAnim();
        }
        else
        {
            CloseAnim().
                OnComplete(() => gameObject.SetActive(false));
        }
        _isActive = active;
    }

    public bool IsActive() => _isActive;

    private void CreateCloseAnim()
    {
        _closeAnim = DOTween.Sequence(transform).SetAutoKill(false).Pause();

        _closeAnim.Append(_rectTransform.DOScale(0.7f, _openCloseAnimDur));
        _closeAnim.Insert(0, _canvasGroup.DOFade(0, _openCloseAnimDur));
    }

    private void CreateOpenAnim()
    {
        _openAnim = DOTween.Sequence(transform).SetAutoKill(false).Pause();

        _openAnim.Append(_rectTransform.DOScale(1f, _openCloseAnimDur));
        _openAnim.Insert(0, _canvasGroup.DOFade(1, _openCloseAnimDur));
    }

    private Sequence OpenAnim()
    {
        if (_closeAnim.IsPlaying())
            _closeAnim.Pause();

        _rectTransform.localScale = Vector3.one * 0.7f;
        _canvasGroup.alpha = 0f;

        _openAnim.OnComplete(null).Restart();
        return _openAnim;
    }

    private Sequence CloseAnim()
    {
        if (_openAnim.IsPlaying())
            _openAnim.Pause();

        _rectTransform.localScale = Vector3.one;
        _canvasGroup.alpha = 1f;

        _closeAnim.OnComplete(null).Restart();
        return _closeAnim;
    }

    private void OnValueChanged(bool isActive, Color newColor)
    {
        if (isActive)
        {
            _activeColor = newColor;
            _onColorChanged?.Invoke(newColor);

            if (HideAfterColorChange)
                SetActive(false);
        }
    }
}
