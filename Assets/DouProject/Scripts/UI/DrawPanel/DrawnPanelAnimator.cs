﻿using DG.Tweening;
using UnityEngine;

public class DrawnPanelAnimator
{
    private readonly DrawnPanelController _pc;
    private Sequence _openAnim;
    private Sequence _closeAnim;

    private readonly float _menuStartPosY;

    public DrawnPanelAnimator(DrawnPanelController panelController)
    {
        _pc = panelController;
        _menuStartPosY = _pc.Menu.anchoredPosition.y;

        CreateOpenAnim();
        CreateCloseAnim();
    }

    public Sequence OpenAnim()
    {
        OpenAnimPrepare();
        _openAnim.OnComplete(null).Restart();
        return _openAnim;
    }

    public Sequence CloseAnim()
    {
        CloseAnimPrepare();
        _closeAnim.OnComplete(null).Restart();
        return _closeAnim;
    }

    private void CloseAnimPrepare()
    {
        _pc.Menu.anchoredPosition = new Vector2(0f, _menuStartPosY);
    }

    private void OpenAnimPrepare()
    {
        _pc.Menu.anchoredPosition = new Vector2(0f, _pc.Menu.sizeDelta.y);
        _pc.DrawnObjs.alpha = 1f;
        Utils.SetColorAlpha(_pc.DrawnGraphic, 1f);
    }

    private void CreateOpenAnim()
    {
        _openAnim = DOTween.Sequence(_pc).Pause().SetAutoKill(false);
        SetupOpenAnim(_openAnim);
    }

    private void CreateCloseAnim()
    {
        _closeAnim = DOTween.Sequence(_pc).Pause().SetAutoKill(false);
        SetupCloseAnim(_closeAnim);
    }

    private void SetupCloseAnim(Sequence closeAnim)
    {
        closeAnim.Append(_pc.Menu.DOAnchorPosY(_pc.Menu.sizeDelta.y, _pc.OpenCloseAnimDur));
        closeAnim.Insert(0, _pc.DrawnGraphic.DOFade(0f, _pc.OpenCloseAnimDur));
        closeAnim.Insert(0, _pc.DrawnObjs.DOFade(0f, _pc.OpenCloseAnimDur));
    }

    private void SetupOpenAnim(Sequence openAnim)
    {
        openAnim.Append(_pc.Menu.DOAnchorPosY(_menuStartPosY, _pc.OpenCloseAnimDur));
    }
}