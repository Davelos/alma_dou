using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class DrawTextController : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [SerializeField] RectTransform _rectTransform;
    [SerializeField] RectTransform _background;
    [SerializeField] Button _removeBtn;
    [SerializeField] TMP_InputField _textField;

    private Canvas _canvas;
    private Camera _cameraMain;
    private KeyboardController _keyboardController;

    [Inject]
    public void Consruct(Canvas canvas, KeyboardController keyboardController)
    {
        _canvas = canvas;
        _keyboardController = keyboardController;
    }

    private void Awake()
    {
        _removeBtn.onClick.AddListener(RemoveThis);
        _textField.onSelect.AddListener(_ => { if (!_keyboardController.HasPhysicalKeyboard()) _keyboardController.ShowKeyboard(); });

        _cameraMain = Camera.main;
    }

    private void OnValidate()
    {
        if (_rectTransform == null)
            _rectTransform = GetComponent<RectTransform>();

        if (_removeBtn == null)
            _removeBtn = GetComponentInChildren<Button>();

        if (_textField == null)
            _textField = GetComponentInChildren<TMP_InputField>();
    }

    private void Update()
    {
        if (_removeBtn.gameObject.activeSelf && Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Input.mousePosition;
            if (!RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform, mousePos, _cameraMain))
                _removeBtn.gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        _removeBtn.onClick.RemoveAllListeners();
        _textField.onSelect.RemoveAllListeners();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _removeBtn.gameObject.SetActive(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 screenPos = _rectTransform.anchoredPosition * _canvas.scaleFactor + eventData.delta;
        float halfSizeY = _rectTransform.sizeDelta.y * _canvas.scaleFactor * 0.5f;
        float sizeX = _rectTransform.sizeDelta.x * _canvas.scaleFactor;

        Vector2 correctScreenPos = new(
            Mathf.Clamp(screenPos.x, 0, Screen.width - sizeX),
            Mathf.Clamp(screenPos.y, halfSizeY, Screen.height - halfSizeY));

        _rectTransform.anchoredPosition = correctScreenPos / _canvas.scaleFactor;
    }

    private void RemoveThis()
    {
        Destroy(gameObject);
    }
}
