using DG.Tweening;
using LS.DrawTexture.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class DrawnPanelController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    [field: SerializeField] public float OpenCloseAnimDur { get; private set; }

    public RectTransform Menu => _menu;
    public MaskableGraphic DrawnGraphic => _drawnGraphic;
    public CanvasGroup DrawnObjs => _drawnObjs;

    [Header("Buttons")]
    [SerializeField] private Button _brushColorBtn;
    [SerializeField] private Button _eraserModeBtn;
    [SerializeField] private Button _penModeBtn;
    [SerializeField] private Button _addTextBtn;
    [SerializeField] private Button _screenshotBtn;
    [SerializeField] private Button _closeBtn;
    [Space]
    [Header("Other settings")]
    [SerializeField] private CanvasGroup _buttons;
    [SerializeField] private CanvasGroup _drawnObjs;
    [SerializeField] private DrawTextureUI _drawTextureUI;
    [SerializeField] private MaskableGraphic _drawnGraphic;
    [SerializeField] private RectTransform _menu;
    [SerializeField] private ColorPanel _colorPanel;
    [SerializeField] private SizePanel _eraserSizePanel;
    [SerializeField] private SizePanel _penSizePanel;
    [SerializeField] private GameMenuController _gameMenu;

    private readonly List<GameObject> _drawnTexts = new();
    private DrawnPanelAnimator _animator;
    private DrawTextFactory _textFactory;
    private Screenshoter _screenshoter;

    [Inject]
    public void Construct(DrawTextFactory textFactory, Screenshoter screenshoter)
    {
        _textFactory = textFactory;
        _screenshoter = screenshoter;
    }

    private void Awake()
    {
        InitButtons();

        _drawTextureUI.Color = _colorPanel.ActiveColor;
        _drawTextureUI.Size = _penSizePanel.ActiveSize;
        _animator = new DrawnPanelAnimator(this);
    }

    private void OnDestroy()
    {
        _closeBtn.onClick.RemoveAllListeners();
    }

    public void OnBeginDrag(PointerEventData eventData) { }

    public void OnDrag(PointerEventData eventData) { }

    public void OnEndDrag(PointerEventData eventData) { }

    private void InitButtons()
    {
        _closeBtn.onClick.AddListener(_gameMenu.DrawPanelClose);

        _brushColorBtn.onClick.AddListener(() => _colorPanel.SetActive(!_colorPanel.IsActive()));
        _colorPanel.Init(SetColorToBrush);

        _eraserModeBtn.onClick.AddListener(SetEraserMode);
        _eraserSizePanel.Init(SetSizeToBrush);

        _penModeBtn.onClick.AddListener(SetPencilMode);
        _penSizePanel.Init(SetSizeToBrush);

        _addTextBtn.onClick.AddListener(AddText);
        _screenshotBtn.onClick.AddListener(_screenshoter.SaveScreenshot);
    }

    public void Open()
    {
        gameObject.SetActive(true);
        _animator.OpenAnim().
            OnComplete(() => SetElemsInteractable(true));
    }

    public void Close()
    {
        SetElemsInteractable(false);
        _animator.CloseAnim().
            OnComplete(() =>
            { 
                _drawTextureUI.Clear();
                RemoveAllDrawnText();
                gameObject.SetActive(false); 
            });
    }

    private void SetElemsInteractable(bool interact)
    {
        _drawTextureUI.enabled = interact;
        _buttons.interactable = interact;
        _drawnObjs.interactable = interact;
    }

    private void ActivateDrawTextureUI()
    {
        if (!_drawTextureUI.enabled)
            _drawTextureUI.enabled = true;
    }

    private void SetColorToBrush(Color color)
    {
        ActivateDrawTextureUI();
        _drawTextureUI.Color = color;
        _drawTextureUI.Size = _penSizePanel.ActiveSize;
    }

    private void SetSizeToBrush(float newSize)
    {
        _drawTextureUI.Size = newSize;
    }

    private void SetEraserMode()
    {
        ActivateDrawTextureUI();
        _drawTextureUI.Color = Color.clear;
        _drawTextureUI.Size = _eraserSizePanel.ActiveSize;

        if (!_eraserSizePanel.IsActive())
            _eraserSizePanel.SetActive(true);
    }

    private void SetPencilMode()
    {
        ActivateDrawTextureUI();
        _drawTextureUI.Color = _colorPanel.ActiveColor;
        _drawTextureUI.Size = _penSizePanel.ActiveSize;

        if (!_penSizePanel.IsActive())
            _penSizePanel.SetActive(true);
    }

    private void AddText()
    {
        if (_drawTextureUI.enabled)
            _drawTextureUI.enabled = false;

        _drawnTexts.Add(_textFactory.Build().gameObject);
    }

    private void RemoveAllDrawnText()
    {
        foreach(var text in _drawnTexts)
            Destroy(text);
        _drawnTexts.Clear();
    }
}
