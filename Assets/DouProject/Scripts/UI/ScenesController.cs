using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;

public class ScenesController : MonoBehaviour
{
    [Serializable]
    public struct SceneToButton
    {
        public string SceneName;
        public Button LoadSceneBtn;
    }

    [SerializeField] private GameObject _screenController;
    [SerializeField] private GameMenuController _gameMenuController;
    [SerializeField] private SceneToButton[] _sceneButtons;

    private ISceneLoader _sceneLoader;

    [Inject]
    public void Construct(ISceneLoader sceneLoader)
    {
        _sceneLoader = sceneLoader;
        _sceneLoader.OnLoadedScene += OnLoadedScene;
        _sceneLoader.OnUnloadSceneAndGoToMenu += OnUnloadSceneAndGoToMenu;
    }

    private void Awake()
    {
        foreach (var sceneButton in _sceneButtons)
        {
            sceneButton.LoadSceneBtn.onClick.AddListener(() => AnimateBtnAndLoadScene(sceneButton.LoadSceneBtn, sceneButton.SceneName));
        }
    }

    private void OnDestroy()
    {
        _sceneLoader.OnLoadedScene -= OnLoadedScene;
        _sceneLoader.OnUnloadSceneAndGoToMenu -= OnUnloadSceneAndGoToMenu;

        foreach (var sceneButton in _sceneButtons)
        {
            sceneButton.LoadSceneBtn.onClick.RemoveAllListeners();
        }
    }

    private void OnUnloadSceneAndGoToMenu()
    {
        if (_gameMenuController.gameObject.activeSelf)
            _gameMenuController.CloseGameMenu(true);

        _gameMenuController.SwapActionForExitBtn();
        _screenController.SetActive(true);
    }

    private void OnLoadedScene()
    {
        _gameMenuController.SwapActionForExitBtn(_sceneLoader.UnloadSceneAndGoToMenu);
        _screenController.SetActive(false);
    }

    private void AnimateBtnAndLoadScene(Button button, string sceneName)
    {
        button.transform.DOScale(0.7f, 0.2f).SetAutoKill(false).OnComplete(() =>
        {
            button.transform.DOScale(Vector3.one, 0.5f).OnComplete(() =>
            {
                _sceneLoader.LoadScene(sceneName);
            });
        });
    }
}

public class LessonsLoader : ISceneLoader
{
    public Action OnLoadedScene { get => _onLoadedScene; set => _onLoadedScene = value; }
    public Action OnUnloadSceneAndGoToMenu { get => _onUnloadSceneAndGoToMenu; set => _onUnloadSceneAndGoToMenu = value; }
    public Func<Tween> OnUnloadCurSceneAnim { get; set; }

    private event Action _onLoadedScene;
    private event Action _onUnloadSceneAndGoToMenu;
    private ZenjectSceneLoader _sceneLoader;
    private string _curSceneName = string.Empty;

    public LessonsLoader(ZenjectSceneLoader sceneLoader)
    {
        _sceneLoader = sceneLoader;
    }

    public void LoadScene(string sceneName)
    {
        if (_curSceneName != string.Empty)
            UnloadSceneWithAnim(_curSceneName);

        _onLoadedScene?.Invoke();

        _sceneLoader.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        _curSceneName = sceneName;
    }

    public void UnloadSceneAndGoToMenu()
    {
        if (_curSceneName == string.Empty)
            return;

        _onUnloadSceneAndGoToMenu?.Invoke();

        UnloadSceneWithAnim(_curSceneName);
    }

    private void UnloadSceneWithAnim(string unloadSceneName)
    {
        if (OnUnloadCurSceneAnim == null)
        {
            //Debug.LogWarning("OnUnloadCurSceneAnim is empty!");
            UnloadScene(unloadSceneName);
        }
        else OnUnloadCurSceneAnim()
            .OnComplete(() => UnloadScene(unloadSceneName));
        _curSceneName = string.Empty;
        OnUnloadCurSceneAnim = null;
    }

    private void UnloadScene(string unloadSceneName)
    {
        var unloadOperation = SceneManager.UnloadSceneAsync(unloadSceneName);
        unloadOperation.completed += operation => Resources.UnloadUnusedAssets();
    }
}

public interface ISceneLoader
{
    Action OnLoadedScene { get; set; }
    Action OnUnloadSceneAndGoToMenu { get; set; }
    public Func<Tween> OnUnloadCurSceneAnim { get; set; }

    void LoadScene(string sceneName);

    void UnloadSceneAndGoToMenu();
}