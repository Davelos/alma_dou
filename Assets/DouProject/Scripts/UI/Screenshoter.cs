using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;

public class Screenshoter : MonoBehaviour, IInitializable
{
    [SerializeField] private string _screenshotDirectoryName = "���� ���������� �����������";
    [SerializeField] private bool _lookAtCenter;
    [SerializeField] private GameObject _shuttersParent;
    [SerializeField] List<RectTransform> _shutterElems;
    [SerializeField] private GameMenuController _gameMenuController;
    [Space]
    [Header("AnimSettings")]
    [SerializeField] private float _animDur = 0.5f;
    [SerializeField] private float _endScaleY = 22;

    private Sequence _shuttersOpenAnim;
    private Sequence _shuttersCloseAnim;
    private string _screenshotPath;

    public void Initialize()
    {
        _screenshotPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)}\\{_screenshotDirectoryName}";
        if (!Directory.Exists(_screenshotPath))
            Directory.CreateDirectory(_screenshotPath);

        InitializeOpenAnim();
        InitializeCloseAnim();
    }

    private void Awake()
    {
        LookAtCenterUpwards();
    }

    public void SaveScreenshot()
    {
        gameObject.SetActive(true);
        bool startActive = _gameMenuController.gameObject.activeSelf;

        ShuttersCloseAnim()
            .OnComplete(() =>
            {
                _gameMenuController.gameObject.SetActive(false);
                _shuttersParent.SetActive(false);
                StartCoroutine(SaveScreenshotRoutine(startActive));
            });
    }

    private IEnumerator SaveScreenshotRoutine(bool startActive)
    {
        yield return new WaitForEndOfFrame();
        var curTime = DateTime.Now;

        string fileName = $"{_screenshotPath}\\{curTime.ToLongDateString()} {curTime.Hour}.{curTime.Minute}.{curTime.Second}.png";
        ScreenCapture.CaptureScreenshot(fileName);

        _gameMenuController.gameObject.SetActive(startActive);
        _shuttersParent.SetActive(true);

        yield return new WaitForEndOfFrame();
        ShuttersOpenAnim()
            .OnComplete(() => gameObject.SetActive(false));
    }

    private void LookAtCenterUpwards()
    {
        if (!_lookAtCenter) return;

        foreach (var shutterElem in _shutterElems)
        {
            Vector2 centerDir = -shutterElem.localPosition.normalized;

            float angle = Mathf.Atan2(centerDir.y, centerDir.x) * Mathf.Rad2Deg;

            shutterElem.rotation = Quaternion.Euler(0f, 0f, angle - 90);
        }
    }

    private void InitializeOpenAnim()
    {
        _shuttersOpenAnim = DOTween.Sequence().SetAutoKill(false).Pause();

        foreach (var shutterElem in _shutterElems)
            _shuttersOpenAnim.Insert(0, shutterElem.DOScaleY(0, _animDur));
    }

    private void InitializeCloseAnim()
    {
        _shuttersCloseAnim = DOTween.Sequence(gameObject).SetAutoKill(false).Pause();

        foreach (var shutterElem in _shutterElems)
            _shuttersCloseAnim.Insert(0, shutterElem.DOScaleY(_endScaleY, _animDur));
    }

    private Sequence ShuttersOpenAnim()
    {
        _shuttersOpenAnim.OnComplete(null).Restart();
        return _shuttersOpenAnim;
    }

    private Sequence ShuttersCloseAnim()
    {
        foreach (var shutterElem in _shutterElems)
            shutterElem.localScale = new Vector3(1f, 0f, 1f);

        _shuttersCloseAnim.OnComplete(null).Restart();
        return _shuttersCloseAnim;
    }
}
