using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBox : MonoBehaviour
{
    [SerializeField] private MaskableGraphic _background;

    public void SetBackgroundColorState(Color newColr)
    {
        _background.color = newColr;
    }
}
