﻿using System;
using UnityEngine.Events;
using VolumeBox.Utils;

public class GameMessageBox
{
    private GameMesBoxTypes _curMesBoxType;

    public void ShowCorrectInfoBox(string mainCaption = "", string headerCaption = "",
        Action okAction = null, UnityAction onShowEvent = null)
    {
        MessageBoxData correctAnswerData = new()
        {
            OkCaption = "ОК",
            HeaderCaption = headerCaption != "" ? headerCaption : "Всё верно!",
            MainCaption = mainCaption,
            OkAction = okAction,
            OkButtonState = OkButtonState.Positive,
        };

        ShowMessageBox(correctAnswerData, GameMesBoxTypes.InfoBox, onShowEvent);
    }

    public void ShowIncorrectInfoBox(string mainCaption = "", string headerCaption = "",
        Action okAction = null, UnityAction onShowEvent = null)
    {
        MessageBoxData incorrectAnswerData = new()
        {
            OkCaption = "ОК",
            HeaderCaption = headerCaption != "" ? headerCaption : "Надо подумать!",
            MainCaption = mainCaption,
            OkAction = okAction,
            OkButtonState = OkButtonState.Negative
        };

        ShowMessageBox(incorrectAnswerData, GameMesBoxTypes.InfoBox, onShowEvent);
    }

    public void ShowCorrectMessageBox(string headerCaption = "", Action okAction = null, UnityAction onShowEvent = null)
    {
        MessageBoxData incorrectAnswerData = new()
        {
            OkCaption = "ОК",
            HeaderCaption = headerCaption != "" ? headerCaption : "Правильно!",
            OkAction = okAction,
            OkButtonState = OkButtonState.Positive
        };

        ShowMessageBox(incorrectAnswerData, GameMesBoxTypes.CorrectBox, onShowEvent);
    }

    public void ShowWinMessageBox(string nextThemeName, Action okAction = null, Action noAction = null)
    {
        MessageBoxData winData = new()
        {
            OkCaption = "Да",
            NoCaption = "Нет",
            HeaderCaption = "Поздравляем!",
            MainCaption = string.Format("Хочешь перейти к теме \"{0:N1}\"?", nextThemeName),
            OkAction = okAction,
            NoAction = noAction
        };

        ShowMessageBox(winData, GameMesBoxTypes.WinBox, null);
    }

    private void ShowMessageBox(MessageBoxData boxData, GameMesBoxTypes mesBoxType, UnityAction onShowEvent)
    {
        MessageBox.Instance.OnShowEvent.RemoveAllListeners();
        if (onShowEvent != null)
            MessageBox.Instance.OnShowEvent.AddListener(onShowEvent);

        if (_curMesBoxType != mesBoxType)
        {
            MessageBox.SetSkin(mesBoxType.ToString());
            _curMesBoxType = mesBoxType;
        }
        MessageBox.Show(boxData);
    }
}

public enum GameMesBoxTypes
{
    InfoBox,
    CorrectBox,
    WinBox
}