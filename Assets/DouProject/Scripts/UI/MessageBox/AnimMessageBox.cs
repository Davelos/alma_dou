using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimMessageBox : MonoBehaviour
{
    [SerializeField] private float _openCloseAnimDur = 0.5f;
    [SerializeField] private RectTransform _window;
    [SerializeField] private Image _backgroundImage;

    private Sequence _openAnim;
    private Sequence _closeAnim;
    private Vector3 _startPos;
    private float _startAlpha;

    public void Construct()
    {
        _startPos = _window.anchoredPosition;
        _startAlpha = _backgroundImage.color.a;

        CreateOpenAnim();
        CreateCloseAnim();
    }

    private void OnEnable()
    {
        SetActive(true);
    }

    private void OnValidate()
    {
        if (_window == null)
            _window = transform.GetChild(0) as RectTransform;
        if (_backgroundImage == null)
            _backgroundImage = transform.GetComponent<Image>();
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            OpenAnim();
        }
        else
        {
            CloseAnim()
                .OnComplete(() => gameObject.SetActive(false));
        }
    }

    private void CreateOpenAnim()
    {
        _openAnim = DOTween.Sequence().SetAutoKill(false).Pause();

        _openAnim.Append(_window.DOAnchorPosY(_startPos.y, _openCloseAnimDur).SetEase(Ease.OutBack));
        _openAnim.Insert(0f, _backgroundImage.DOFade(_startAlpha, _openCloseAnimDur));
    }

    private void CreateCloseAnim()
    {
        _closeAnim = DOTween.Sequence().SetAutoKill(false).Pause();

        _closeAnim.Append(_window.DOAnchorPosY(Screen.currentResolution.height, _openCloseAnimDur).SetEase(Ease.InBack));
        _closeAnim.Insert(0f, _backgroundImage.DOFade(0f, _openCloseAnimDur));
    }

    private Sequence OpenAnim()
    {
        _window.anchoredPosition = new Vector2(_startPos.x, -Screen.currentResolution.height);
        Utils.SetColorAlpha(_backgroundImage, 0);

        _openAnim.OnComplete(() => { })?.Restart();
        return _openAnim;
    }

    private Sequence CloseAnim()
    {
        _window.anchoredPosition = _startPos;
        Utils.SetColorAlpha(_backgroundImage, _startAlpha);

        _closeAnim.OnComplete(() => { })?.Restart();
        return _closeAnim;
    }
}
