using Assets.SimpleLocalization.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Image))]
public class LocalizedImage : MonoBehaviour
{
    public string LocalizationKey;

    [SerializeField] private Image _targetImage;

    private void OnValidate()
    {
        if (_targetImage == null)
            _targetImage = GetComponent<Image>();
    }

    public void Start()
    {
        Localize();
        LocalizationManager.OnLocalizationChanged += Localize;
    }

    public void OnDestroy()
    {
        LocalizationManager.OnLocalizationChanged -= Localize;
    }

    private void Localize()
    {
        var path = LocalizationManager.Localize(LocalizationKey);
        var sprite = Resources.Load<Sprite>($"LocalizeImages/{path}");

        if (sprite != null)
            _targetImage.sprite = sprite;
        Resources.UnloadUnusedAssets();
    }


    //private void LocalizeAcync()
    //{
    //    var path = LocalizationManager.Localize(LocalizationKey);
    //    var sprite = Resources.LoadAsync<Sprite>($"LocalizeImages/{path}");
    //    _targetImage.sprite = (Sprite)sprite.asset;
    //}

}
