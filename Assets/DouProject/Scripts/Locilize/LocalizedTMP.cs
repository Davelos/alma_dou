﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.SimpleLocalization.Scripts
{
	/// <summary>
	/// Localize text component.
	/// </summary>
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedTMP : MonoBehaviour
    {
        public string LocalizationKey;

        [SerializeField] private TextMeshProUGUI _targetTMP;

        private void OnValidate()
        {
            if (_targetTMP == null)
                _targetTMP= GetComponent<TextMeshProUGUI>();
        }

        public void Start()
        {
            Localize();
            LocalizationManager.OnLocalizationChanged += Localize;
        }

        public void OnDestroy()
        {
            LocalizationManager.OnLocalizationChanged -= Localize;
        }

        private void Localize()
        {
            _targetTMP.text = LocalizationManager.Localize(LocalizationKey);
        }
    }
}