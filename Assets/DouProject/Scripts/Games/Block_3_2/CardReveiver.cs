using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace DOU.PartThree.CashMachine
{
    public class CardReveiver : MonoBehaviour, IDropHandler
    {
        [SerializeField] private Bankomate _bankomate;
        [SerializeField] private GameObject _effectHint;
        private BankomateCard _bankomateCard;

        private void Start()
        {
            BankomateCard.HintEffect += EffectHint;
            BankomateCard.InsertCard += InsercCard;
        }

        private void OnDestroy()
        {
            BankomateCard.InsertCard -= InsercCard;
            BankomateCard.HintEffect -= EffectHint;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<BankomateCard>())
            {
                _bankomateCard = eventData.pointerDrag.GetComponent<BankomateCard>();
                _bankomateCard.PlayAnimInsertCard();
            }
        }

        private void InsercCard()
        {
            _bankomate.ManageCardData(_bankomateCard.Card, true);
        }

        private void EffectHint(bool effect)
        {
            _effectHint.SetActive(effect);
        }

    }
}
