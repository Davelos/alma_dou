using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization.Scripts;

namespace DOU.PartThree.CashMachine
{
    public class BankomateGameManager : MonoBehaviour
    {
        [SerializeField] private MessageManager _messageManager;
        [SerializeField] private BackGround _backGround;
        [SerializeField] private Bankomate _bankomate;
        [SerializeField] private BankomateCard _bankomateCard;
        [SerializeField] private MoneyReceiver _moneyReceiver;
        [SerializeField] SoundQueueHandler _soundQueue;
        [SerializeField] private DropCell[] _dropCells;
        [SerializeField] private DragMoney[] _dragMoney;
        [Space]
        [SerializeField] private AudioClip _hintClip;
        [SerializeField] private AudioClip[] _pinCodeClips;
        [SerializeField] private AudioClip[] _setCardClips;
        [SerializeField] private AudioClip[] _inserCardClips;
        [SerializeField] private AudioClip[] _chooseDisplayClips;
        [SerializeField] private AudioClip[] _setMoneyClips;
        [SerializeField] private AudioClip[] _getMoneyClips;

        // Audio Clips
        private int _currentIndexClip;

        // PinCode
        private const int _countSetCreatedPinCode = 2;
        private int _currentSetPincode;

        private void Start()
        {
            _bankomate.PinCodeCreated += InputAgainPinCode;
            _bankomate.SpeakPinCode += PlaySpeakPinCode;
            _bankomate.HintPinCode += PlayHintClip;
            _bankomate.InsertCard += InsertCard;
            _bankomate.ChooseDisplay += ChooseDisplay;
            _bankomate.SetMoneyAction += SetMoney;
            _bankomate.GetMoneyAction += GetMoney;

            _moneyReceiver.CompletedSetMoney += SetMoneyCompleted;
            _moneyReceiver.ComletedGetMoney += CompletedGame;

            _backGround.IsNight += StartGetMoney;

            _messageManager.EndPreview += StartCreatePinCode;
            _messageManager.ReplayGame += ReplayGame;
        }

        private void OnDestroy()
        {
            _bankomate.PinCodeCreated -= InputAgainPinCode;
            _bankomate.SpeakPinCode -= PlaySpeakPinCode;
            _bankomate.HintPinCode -= PlayHintClip;
            _bankomate.InsertCard -= InsertCard;
            _bankomate.ChooseDisplay -= ChooseDisplay;
            _bankomate.SetMoneyAction -= SetMoney;
            _bankomate.GetMoneyAction -= GetMoney;

            _moneyReceiver.CompletedSetMoney -= SetMoneyCompleted;
            _moneyReceiver.ComletedGetMoney -= CompletedGame;

            _backGround.IsNight -= StartGetMoney;

            _messageManager.EndPreview -= StartCreatePinCode;
            _messageManager.ReplayGame -= ReplayGame;
        }

        private void ResetDragMoney()
        {
            foreach (var dragMoney in _dragMoney)
                dragMoney.IsDrop = false;
        }

        private void ResetDropCells()
        {
            foreach (var cell in _dropCells)
                cell.ResetCell();
        }

        private void ResetGame()
        {
            ResetDragMoney();
            ResetDropCells();
            _moneyReceiver.ResetMoneyReceiver();
            _bankomate.ResetBankomate();
            _bankomateCard.ResetBankomateCard();
            _bankomate.SetStateDisplay(StateDiplayBankomate.Default);
        }

        private void CompletedGame()
        {
            _currentIndexClip++;
            _soundQueue.AddSoundsToQueue(_getMoneyClips[_currentIndexClip]);
            _moneyReceiver.CloseReceiver();
            ResetGame();

            _messageManager.StartShowBox(false);
        }

        private void ReplayGame()
        {
            _messageManager.StartShowBox(true);
        }

        #region Create PinCode
        private void StartCreatePinCode()
        {
            _currentIndexClip = 0;
            _bankomate.IsCreatingPinCode = true;

            _bankomate.SetStateDisplay(StateDiplayBankomate.SetPinCode);
            _bankomate.CheckSetPinCodeDisplay();

            _soundQueue.AddSoundsToQueue(_pinCodeClips[_currentIndexClip]);
        }

        private void InputAgainPinCode()
        {
            _currentSetPincode++;

            if (_currentSetPincode >= _countSetCreatedPinCode)
            {
                _bankomate.SetStateDisplay(StateDiplayBankomate.Default);
                _bankomate.ClearInputPinCode();
                StartSetCard();
            }
            else
            {
                _currentIndexClip++;

                _soundQueue.AddSoundsToQueue(_pinCodeClips[_currentIndexClip]);
                _soundQueue.SubscribeToSoundQueueEmpty(_bankomate.ClearInputPinCode);
            }
        }

        private void PlaySpeakPinCode()
        {
            _currentIndexClip++;

            List<AudioClip> audioClips = new List<AudioClip>();
            audioClips.Add(_pinCodeClips[_currentIndexClip]);

            foreach (var clip in _bankomate.PinCodeClips)
                audioClips.Add(clip);

            _currentIndexClip++;
            audioClips.Add(_pinCodeClips[_currentIndexClip]);

            _soundQueue.AddSoundsToQueue(audioClips.ToArray());

            _soundQueue.SubscribeToSoundQueueEmpty(_bankomate.ClearInputPinCode);
            _soundQueue.SubscribeToSoundQueueEmpty(_bankomate.CheckSetPinCodeDisplay);
        }

        public void PlayHintClip()
        {
            _soundQueue.AddSoundsToQueue(_hintClip);
        }
        #endregion

        #region SetCard
        private void StartSetCard()
        {
            _currentIndexClip = 0;
            _bankomate.IsCreatingPinCode = false;
            _bankomateCard.SetPinCodeCard(_bankomate.CreatedPinCode);

            _soundQueue.AddSoundsToQueue(_setCardClips);
            _soundQueue.SubscribeToSoundQueueEmpty(_bankomateCard.PlayAnimTakeCard);
            _soundQueue.SubscribeToSoundQueueEmpty(() => _bankomate.SetStateDisplay(StateDiplayBankomate.SetCard));

            _bankomateCard.ActivatedCard();
            _bankomateCard.PlayAnimShowCard();
        }

        private void InsertCard()
        {
            if (!_bankomate.IsSetMoney && !_bankomate.IsGetMoney)
            {
                _bankomate.IsSetMoney = true;
                StartSetMoney();
            }
            else if (_bankomate.IsGetMoney)
            {
                _currentIndexClip++;
                _soundQueue.AddSoundsToQueue(_getMoneyClips[_currentIndexClip]);
            }
        }

        private void ChooseDisplay()
        {
            if (_bankomate.IsSetMoney)
            {
                _soundQueue.AddSoundsToQueue(_chooseDisplayClips);
            }
            else if (_bankomate.IsGetMoney)
            {
                _currentIndexClip++;
                _soundQueue.AddSoundsToQueue(_getMoneyClips[_currentIndexClip]);
            }
        }


        #endregion

        #region SetMoney
        private void StartSetMoney()
        {
            foreach (var cell in _dropCells)
                cell.CanDrop = false;

            _bankomate.IsSetMoney = true;
            _soundQueue.AddSoundsToQueue(_inserCardClips);
            _bankomate.CheckSetPinCodeDisplay();
        }

        private void SetMoney()
        {
            _moneyReceiver.OpenReceiver();
            _soundQueue.AddSoundsToQueue(_setMoneyClips[_currentIndexClip]);
            _soundQueue.SubscribeToSoundQueueEmpty(_moneyReceiver.ResetDragMoney);
            _soundQueue.SubscribeToSoundQueueEmpty(_moneyReceiver.RandomImage);

        }

        private void SetMoneyCompleted()
        {
            _currentIndexClip++;
            _soundQueue.AddSoundsToQueue(_setMoneyClips[_currentIndexClip]);
            _soundQueue.SubscribeToSoundQueueEmpty(() => _backGround.DayNight(false));

            _moneyReceiver.CloseReceiver();
            _bankomate.SetStateDisplay(StateDiplayBankomate.Default);
            _bankomateCard.PlayAnimShowCard();
        }
        #endregion

        #region GetMoney

        private void StartGetMoney()
        {
            _currentIndexClip = 0;
            _soundQueue.AddSoundsToQueue(_getMoneyClips[_currentIndexClip]);

            _bankomate.IsSetMoney = false;
            _bankomate.IsGetMoney = true;

            _soundQueue.SubscribeToSoundQueueEmpty(_bankomate.CheckSetPinCodeDisplay);
            _soundQueue.SubscribeToSoundQueueEmpty(_bankomateCard.PlayAnimTakeCard);
            _soundQueue.SubscribeToSoundQueueEmpty(() => _bankomate.SetStateDisplay(StateDiplayBankomate.SetCard));
        }

        private void GetMoney()
        {
            foreach (var cell in _dropCells)
                cell.CanDrop = true;

            _currentIndexClip++;
            _soundQueue.AddSoundsToQueue(_getMoneyClips[_currentIndexClip]);
            _moneyReceiver.OpenReceiver();
        }

        #endregion
    }
}