using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;
using System;
using UnityEngine.UI;


namespace DOU.PartThree.CashMachine
{

    public class MoneyReceiver : MonoBehaviour, IDropHandler
    {
        [Serializable]
        private class MoneyType
        {
            public Sprite MoneySprite;
            public int MoneyValue;
            public AudioClip MoneyClip;
        }

        [SerializeField] private Bankomate _bankomate;
        [SerializeField] private Animator _moneyReceiverAnimator;
        [SerializeField] private Animator _walletAnimator;
        [SerializeField] private GameObject _effectHint;
        [Space]
        [SerializeField] private Image _imageBoxMoney;
        [SerializeField] private MoneyType[] _moneyTypes;
        [SerializeField] private int _countWallet = 35000;
        [SerializeField] private TextMeshProUGUI _textSumWallet;
        [SerializeField] private TextMeshProUGUI _textSetSumBankomate;
        [SerializeField] private TextMeshProUGUI _textGetSumBankomate;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _complimentClips;
        [SerializeField] private AudioClip[] _mistakeClips;

        private int _currentMoney;
        private bool _isGetMoney; public bool IsGetMOney => _isGetMoney;

        private const int MAX_SUM_WALLET = 35000;

        private const string NAME_ANIM_OPEN_RECEIVER = "OpenMoneyReceiver";
        private const string NAME_ANIM_CLOSE_RECEIVER = "CloseMoneyReceiver";

        private const string NAME_ANIM_OPEN_WALLET = "OpenWallet";
        private const string NAME_ANIM_CLOSE_WALLET = "CloseWallet";

        public Action CompletedSetMoney;
        public Action ComletedGetMoney;


        private List<DragMoney> _money = new List<DragMoney>();

        private void Start()
        {
            DragMoney.HintDragMoney += EffectHint;
        }

        private void OnDestroy()
        {
            DragMoney.HintDragMoney -= EffectHint;
        }

        public void OpenReceiver()
        {
            _moneyReceiverAnimator.Play(NAME_ANIM_OPEN_RECEIVER);
            _walletAnimator.Play(NAME_ANIM_OPEN_WALLET);

            _textSumWallet.text = _countWallet.ToString();
            _textSumWallet.DOFade(1f, 0.4f);

            _textGetSumBankomate.text = _bankomate.CurrentUser.CountMoney.ToString();
            _textSetSumBankomate.text = _bankomate.CurrentUser.CountMoney.ToString();
        }

        public void CloseReceiver()
        {
            _textSumWallet.DOFade(0f, 0.4f).OnComplete(() =>
            {
                _moneyReceiverAnimator.Play(NAME_ANIM_CLOSE_RECEIVER);
                _walletAnimator.Play(NAME_ANIM_CLOSE_WALLET);
            });

        }

        private void ShuffleMoney(MoneyType[] moneys)
        {
            for (int i = 0; i < moneys.Length; i++)
            {
                int randomIndex = UnityEngine.Random.Range(i, moneys.Length);
                (moneys[i], moneys[randomIndex]) = (moneys[randomIndex], moneys[i]);
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<DragMoney>())
            {
                eventData.pointerDrag.transform.position = transform.position;
                DragMoney dragMoney = eventData.pointerDrag.GetComponent<DragMoney>();

                if (dragMoney.ValueMoney == _moneyTypes[_currentMoney].MoneyValue)
                {
                    _money.Add(dragMoney);
                    PlayAudioEffect(true);
                    dragMoney.IsDrop = true;

                    dragMoney.transform.DOScale(Vector3.zero, 0.9f).OnComplete(() =>
                    {
                        SetMoney(dragMoney);
                        dragMoney.IsDrag = false;
                    });
                }
                else
                {
                    PlayAudioEffect(false);
                }
            }
        }

        private void EffectHint(bool effect)
        {
            if (_bankomate.IsSetMoney)
                _effectHint.SetActive(effect);
        }

        private void PlayAudioEffect(bool compliment)
        {
            AudioClip audioClip = compliment ? _complimentClips[UnityEngine.Random.Range(0, _complimentClips.Length)] : _mistakeClips[UnityEngine.Random.Range(0, _mistakeClips.Length)];
            _audioSource.clip = audioClip;
            _audioSource.Play();
        }

        private void SetMoney(DragMoney dragMoney)
        {
            _countWallet -= dragMoney.ValueMoney;
            _bankomate.CurrentUser.CountMoney += dragMoney.ValueMoney;

            TextAnim(true);
        }

        public bool GetMoney(DragMoney dragMoney, int _currentValue)
        {
            if (dragMoney.ValueMoney == _currentValue)
            {
                _countWallet += dragMoney.ValueMoney;
                _bankomate.CurrentUser.CountMoney -= dragMoney.ValueMoney;

                PlayAudioEffect(true);
                TextAnim(false);
                return true;
            }
            else
            {
                PlayAudioEffect(false);
                return false;
            }
        }

        private void TextAnim(bool setMoney)
        {
            TextMeshProUGUI _currentText = setMoney ? _textSetSumBankomate : _textGetSumBankomate;

            _textSumWallet.text = _countWallet.ToString();
            _currentText.text = _bankomate.CurrentUser.CountMoney.ToString();

            _textSumWallet.transform.DOScale(1.3f, 0.6f).SetEase(Ease.InOutBack).OnComplete(() =>
            {
                _textSumWallet.transform.DOScale(Vector3.one, 0.6f).SetEase(Ease.InOutBack);
            });

            _currentText.transform.DOScale(1.3f, 0.6f).SetEase(Ease.InOutBack).OnComplete(() =>
            {
                _currentText.transform.DOScale(Vector3.one, 0.6f).SetEase(Ease.InOutBack).OnComplete(() =>
                {
                    if (setMoney)
                    {
                        if (_currentMoney < _moneyTypes.Length - 1)
                        {
                            _currentMoney++;
                            RandomImage();
                        }
                        else
                        {

                            CompletedSetMoney?.Invoke();
                        }
                    }
                    else
                    {
                        _isGetMoney = false;
                        if (_countWallet >= MAX_SUM_WALLET)
                        {
                            ComletedGetMoney?.Invoke();
                        }
                    }

                });
            });
        }

        public void ResetMoneyReceiver()
        {
            _countWallet = MAX_SUM_WALLET;
        }

        public void ResetDragMoney()
        {
            ShuffleMoney(_moneyTypes);
            _currentMoney = 0;

        }

        public void RandomImage()
        {
            if (!_imageBoxMoney.gameObject.activeSelf) _imageBoxMoney.gameObject.SetActive(true);

            Color32 color = new Color(255, 255, 255, 100);

            _imageBoxMoney.sprite = _moneyTypes[_currentMoney].MoneySprite;
            _imageBoxMoney.color = color;
            _imageBoxMoney.transform.localScale = Vector3.zero;

            _imageBoxMoney.transform.DOScale(Vector3.one, 0.5f).OnComplete(() =>
            {
                _audioSource.PlayOneShot(_moneyTypes[_currentMoney].MoneyClip);
            });
        }

        public void ShowDragMoney(int sum)
        {
            for (int x = 0; x < _money.Count; x++)
            {
                if (_money[x].ValueMoney == sum)
                {
                    _money[x].transform.DOScale(Vector3.one, 0.9f).OnComplete(() =>
                    {
                        _isGetMoney = true;
                        _money[x].IsDrop = false;
                        _money.RemoveAt(x);
                    });


                    break;
                }
            }
        }

    }

}
