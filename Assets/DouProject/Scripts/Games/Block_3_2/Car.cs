using UnityEngine;
using DG.Tweening;

namespace DOU.PartThree.CashMachine
{
    public class Car : MonoBehaviour
    {
        [SerializeField] private Transform _targetTransform;
        [SerializeField] private float _durationCar = 2f;
        [SerializeField] private float _paused = 1f;

        private bool _isMove = true;
        private bool _distanceCompleted = false;

        private Vector3 _startPosition;
        private Vector3 _flipVector = new Vector3(-1f, 1, 1);

        private void Start()
        {
            _startPosition = transform.position;
            //BackGround.IsDay += SetMove;
            MoveToNextEdge();
        }

        private void OnDestroy()
        {
            //BackGround.IsDay -= SetMove;
        }

        public void MoveToNextEdge()
        {
            Vector3 targetPosition = (_distanceCompleted) ? _startPosition : _targetTransform.position;

            transform.DOMove(targetPosition, _durationCar)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    transform.localScale = _distanceCompleted ? Vector3.one : _flipVector;
                    _distanceCompleted = !_distanceCompleted;

                    if (_isMove)
                    {
                        DOVirtual.DelayedCall(_paused, () =>
                        {
                            MoveToNextEdge();
                        });
                    }
                });
        }

        private void SetMove(bool move)
        {
            _isMove = move;

        }

    }
}