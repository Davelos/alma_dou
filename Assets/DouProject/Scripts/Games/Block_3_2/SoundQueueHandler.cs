using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DOU.PartThree.CashMachine
{
    public class SoundQueueHandler : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] public AudioSource _audioSource;
        private event Action _onSoundQueueEmpty;

        private Queue<AudioClip> _soundQueue = new Queue<AudioClip>();

        public void AddSoundsToQueue(params AudioClip[] soundClips)
        {
            CanvasGroupActivate(false);

            foreach (var clip in soundClips)
            {
                _soundQueue.Enqueue(clip);
            }

            if (_soundQueue.Count > 0)
            {
                PlayNextSound();
            }
        }

        void PlayNextSound()
        {
            if (_soundQueue.Count > 0)
            {
                AudioClip nextSound = _soundQueue.Dequeue();

                StartCoroutine(WaitForSoundEnd(nextSound));
            }
            else
            {
                StartCoroutine(WaitForSoundEnd());
            }
        }

        IEnumerator WaitForSoundEnd(AudioClip nextSound = null)
        {
            if (_audioSource.isPlaying)
                yield return new WaitForSeconds(_audioSource.clip.length);
            else
                yield return new WaitForSeconds(0.8f);


            if (nextSound != null)
            {
                _audioSource.clip = nextSound;
                _audioSource.Play();
            }

            if (_soundQueue.Count > 0)
            {
                PlayNextSound();
            }
            else
            {
                if (_audioSource.isPlaying && !_canvasGroup.interactable)
                {
                    yield return new WaitForSeconds(_audioSource.clip.length);
                    CanvasGroupActivate(true);
                }
                else if (_audioSource.isPlaying)
                {
                    yield return new WaitForSeconds(_audioSource.clip.length);
                }

                _onSoundQueueEmpty?.Invoke();

                if (_onSoundQueueEmpty != null)
                {
                    _onSoundQueueEmpty = null;
                }
            }
        }

        private void CanvasGroupActivate(bool active)
        {
            _canvasGroup.blocksRaycasts = active;
            _canvasGroup.interactable = active;
        }

        public void SubscribeToSoundQueueEmpty(Action handler)
        {
            _onSoundQueueEmpty += handler;
        }
    }
}