using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections;

namespace DOU.PartThree.CashMachine
{
    public enum StateDiplayBankomate
    {
        Default,
        ActionSelection,
        SetPinCode,
        SetCard,
        SetMoney,
        GetMoney
    }

    public class Bankomate : MonoBehaviour
    {
        [Serializable]
        private struct DisplayMapping
        {
            public StateDiplayBankomate State;
            public GameObject Display;
        }

        [Serializable]
        public struct KeyboardBankomate
        {
            public int NumberKeyBoard;
            public Image ImageBox;
            public Button KeyBoardButton;
            public AudioClip ClipButton;
        }

        [Serializable]
        private class GetMoneyButton
        {
            public int Clickable;
            public int Sum;
            public Button Button;
        }

        [SerializeField] private DisplayMapping[] _displayMappings;
        [SerializeField] private KeyboardBankomate[] _keyboardBankomates;
        [SerializeField] private MoneyReceiver _moneyReceiver;
        [SerializeField] private SoundQueueHandler _soundQueueHandler;
        [SerializeField] private AudioSource _audioSource;
        [Space]
        [Header("PinCode Display")]
        [SerializeField] private TextMeshProUGUI _pinCodeLebelTMpo;
        [SerializeField] private TextMeshProUGUI _pinCodeTMPo;
        [SerializeField] private AudioClip[] _crrectPinCodeClips;
        [SerializeField] private AudioClip[] _invalidPPinCodeClips;
        [Space]
        [SerializeField] private CanvasGroup _pinCodeHint;
        [SerializeField] private TextMeshProUGUI _pinCodeHintTMPo;
        [Space]
        [Header("Selection Display")]
        [SerializeField] private Button _getMoneyButton;
        [SerializeField] private Button _setMoneyButton;
        [Header("Get Money Display")]
        [SerializeField] private GetMoneyButton[] _takeMoneyButton;

        private Dictionary<StateDiplayBankomate, GameObject> _displayDictionary;
        private GameObject _currentDisplay;
        private StateDiplayBankomate _currentStateDiplay;
        private PersonalAccount _currentUser; public PersonalAccount CurrentUser { get { return _currentUser; } set { _currentUser = value; } }

        private bool _isCard;
        private bool _isGetMoney; public bool IsGetMoney { get { return _isGetMoney; } set { _isGetMoney = value; } }
        private bool _isSetMoney; public bool IsSetMoney { get { return _isSetMoney; } set { _isSetMoney = value; } }

        private string _pinCodeResult;
        private int _inputAttempts;

        private List<AudioClip> _pinCodeClips = new List<AudioClip>();
        public List<AudioClip> PinCodeClips => _pinCodeClips;

        public Action PinCodeCreated;
        public Action SpeakPinCode;
        public Action HintPinCode;
        public Action InsertCard;
        public Action ChooseDisplay;
        public Action GetMoneyAction;
        public Action SetMoneyAction;
        private bool _isCreatingPinCode; public bool IsCreatingPinCode { set { _isCreatingPinCode = value; } }
        private int _createdPinCode; public int CreatedPinCode => _createdPinCode;

        private const int MAX_NUMBER_PINCODE = 4;
        private const char PIN_CHAR = '.';
        private const int MAX_INPUT_ATTEMPTS = 4;
        private const string DEFAULT_TEXT_PIN_CODE = "����� ���-���";
        private const string FIRST_PIN_CODE = "�������� ���-���";
        private const string AGAIN_PIN_CODE = "������� ���-���";

        #region Initialization
        private void Start()
        {
            InitializationDisplay();
            InitializationButton();
        }

        private void OnDestroy()
        {
            ClearDataButton();
        }

        private void InitializationDisplay()
        {
            _displayDictionary = new Dictionary<StateDiplayBankomate, GameObject>();

            foreach (var mapping in _displayMappings)
                _displayDictionary[mapping.State] = mapping.Display;

            SetStateDisplay(StateDiplayBankomate.Default);
        }

        private void InitializationButton()
        {
            foreach (var button in _takeMoneyButton)
                button.Button.onClick.AddListener(() => GetMoney(button));

            foreach (var keyBoardButton in _keyboardBankomates)
                keyBoardButton.KeyBoardButton.onClick.AddListener(() => SetPinCode(keyBoardButton));

            _getMoneyButton.onClick.AddListener(() => ClickChooseButton(true));
            _setMoneyButton.onClick.AddListener(() => ClickChooseButton(false));

        }

        private void ClearDataButton()
        {
            foreach (var button in _takeMoneyButton)
                button.Button.onClick.RemoveAllListeners();

            foreach (var keyBoardButton in _keyboardBankomates)
                keyBoardButton.KeyBoardButton.onClick.RemoveAllListeners();

            _getMoneyButton.onClick.RemoveAllListeners();
            _setMoneyButton.onClick.RemoveAllListeners();
        }

        public void SetStateDisplay(StateDiplayBankomate state)
        {
            if (_displayDictionary.TryGetValue(state, out GameObject nextDisplay))
            {
                if (_currentDisplay != null)
                    _currentDisplay.SetActive(false);

                nextDisplay.SetActive(true);
                _currentDisplay = nextDisplay;
                _currentStateDiplay = state;
            }
        }

        public void ResetBankomate()
        {
            _isCard = false;
            _isCreatingPinCode = false;
            _isGetMoney = false;
            _isSetMoney = false;
            _createdPinCode = 0;
        }
        #endregion

        #region PinCode
        private bool CheckPinCode(int pinCode)
        {
            int correctPincode = _isCreatingPinCode ? _createdPinCode : _currentUser.PinCode;
            bool isCorrectPin = (correctPincode == pinCode);

            AudioClip[] clips = isCorrectPin ? _crrectPinCodeClips : _invalidPPinCodeClips;
            //_soundQueueHandler.AddSoundsToQueue(clips[UnityEngine.Random.Range(0, clips.Length)]);

            _audioSource.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
            _audioSource.Play();

            return isCorrectPin;
        }

        private void SetPinCode(KeyboardBankomate keyboardBankomate)
        {
            if (_pinCodeTMPo.text.Length == MAX_NUMBER_PINCODE || _currentStateDiplay != StateDiplayBankomate.SetPinCode) return;

            //if (_audioSource.isPlaying) _audioSource.Stop();
            //_soundQueueHandler.AddSoundsToQueue(keyboardBankomate.ClipButton);
            if (!_audioSource.isPlaying)
            {
                _audioSource.clip = keyboardBankomate.ClipButton;
                _audioSource.Play();
            }
            else
            {
                _audioSource.PlayOneShot(keyboardBankomate.ClipButton);
            }

            if (_pinCodeClips.Count >= MAX_NUMBER_PINCODE) _pinCodeClips.Clear();
            _pinCodeClips.Add(keyboardBankomate.ClipButton);

            _pinCodeResult += keyboardBankomate.NumberKeyBoard.ToString();
            _pinCodeTMPo.text += PIN_CHAR;

            keyboardBankomate.ImageBox.DOFade(1f, 0.4f).OnComplete(() => keyboardBankomate.ImageBox.DOFade(0f, 0.4f));

            if (_pinCodeTMPo.text.Length == MAX_NUMBER_PINCODE)
            {
                int pinCode = int.Parse(_pinCodeResult);
                StartCoroutine(WaitClipAndStartCheckPinCode(pinCode));
            }
        }

        private IEnumerator WaitClipAndStartCheckPinCode(int pinCode)
        {
            if (_audioSource.isPlaying)
                yield return new WaitForSeconds(_audioSource.clip.length);

            if (_isCard)
            {
                if (CheckPinCode(pinCode))
                {
                    SetStateDisplay(StateDiplayBankomate.ActionSelection);

                    _getMoneyButton.interactable = _isGetMoney;
                    _setMoneyButton.interactable = _isSetMoney;

                    ChooseDisplay?.Invoke();
                }
                else
                {
                    _inputAttempts++;
                }

                if (_inputAttempts >= MAX_INPUT_ATTEMPTS)
                    ShowHintPinCode();

                ClearInputPinCode();
            }
            else if (_isCreatingPinCode)
            {
                if (_createdPinCode <= 0)
                {
                    _createdPinCode = pinCode;
                    SpeakPinCode?.Invoke();
                }
                else
                {
                    if (CheckPinCode(pinCode))
                    {
                        PinCodeCreated?.Invoke();
                    }
                    else
                    {
                        _inputAttempts++;
                        ClearInputPinCode();

                        if (_inputAttempts >= MAX_INPUT_ATTEMPTS)
                            ShowHintPinCode();
                    }
                }
            }
        }

        public void ClearInputPinCode()
        {
            _pinCodeResult = "";
            _pinCodeTMPo.text = "";
        }

        public void CheckSetPinCodeDisplay()
        {
            if (!_isCard && _createdPinCode <= 0)
                _pinCodeLebelTMpo.text = FIRST_PIN_CODE;
            else if (!_isCard && _createdPinCode > 0)
                _pinCodeLebelTMpo.text = AGAIN_PIN_CODE;
            else
                _pinCodeLebelTMpo.text = DEFAULT_TEXT_PIN_CODE;
        }

        private void ShowHintPinCode()
        {
            _inputAttempts = 0;

            if (!_isCard && _isCreatingPinCode)
                _pinCodeHintTMPo.text = _createdPinCode.ToString();
            else
                _pinCodeHintTMPo.text = _currentUser.PinCode.ToString();

            _pinCodeHint.DOFade(1f, 1.5f).OnComplete(() =>
            {
                _pinCodeHint.interactable = true;
                _pinCodeHint.blocksRaycasts = true;
            });

            HintPinCode?.Invoke();
        }

        public void HideHintPinCode()
        {
            _pinCodeHint.DOFade(0f, 1.2f).OnComplete(() =>
            {
                _pinCodeHint.interactable = false;
                _pinCodeHint.blocksRaycasts = false;
            });
        }
        #endregion

        #region SetGetMoney

        public void ManageCardData(PersonalAccount personalAccount, bool hasCard)
        {
            _isCard = hasCard;
            _currentUser = hasCard ? personalAccount : null;
            SetStateDisplay(hasCard ? StateDiplayBankomate.SetPinCode : StateDiplayBankomate.SetCard);

            if (hasCard)
                InsertCard?.Invoke();
        }

        private void ClickChooseButton(bool getMoney)
        {
            SetStateDisplay(getMoney ? StateDiplayBankomate.GetMoney : StateDiplayBankomate.SetMoney);

            (getMoney ? GetMoneyAction : SetMoneyAction)?.Invoke();
        }


        private void GetMoney(GetMoneyButton moneyButton)
        {
            if (!_moneyReceiver.IsGetMOney)
            {
                moneyButton.Clickable--;
                _moneyReceiver.ShowDragMoney(moneyButton.Sum);
                RefeshGetMoneyButton();
            }

        }

        private void RefeshGetMoneyButton()
        {
            foreach (var button in _takeMoneyButton)
            {
                button.Button.interactable = (button.Clickable > 0);
            }
        }
        #endregion
    }
}