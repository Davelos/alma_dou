using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace DOU.PartThree.CashMachine
{
    public class DragMoney : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private int _valueMoney;
        public int ValueMoney => _valueMoney;

        public static Action<bool> HintDragMoney;
        private bool _isDrag = false; public bool IsDrag { set { _isDrag = value; } }
        private bool _isDrop; public bool IsDrop { set { _isDrop = value; } }
        private Vector3 _startPosition;
        private int _currentSubIndex;

        private const float DURATION = 0.4f;

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!_isDrag)
            {
                _isDrag = true;
                _startPosition = _rectTransform.transform.position;
                _canvasGroup.blocksRaycasts = false;
                _currentSubIndex = _rectTransform.GetSiblingIndex();
                _rectTransform.SetAsLastSibling();
            }
     
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_isDrag)
            {
                _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
            _rectTransform.SetSiblingIndex(_currentSubIndex);

            if (!_isDrop)
            {
                _rectTransform.DOMove(_startPosition, DURATION * 1.5f).OnComplete(() =>
                _rectTransform.DOShakePosition(DURATION, 9, 5).OnComplete(() =>
                {
                    _isDrag = false;
                    _rectTransform.SetSiblingIndex(_currentSubIndex);
                }));
            }
        }

        public void ResetPosition()
        {
            transform.GetComponent<Image>().raycastTarget = true;
            _isDrop = false;
            _isDrag = false;
            _rectTransform.anchoredPosition = _startPosition;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            HintDragMoney?.Invoke(true);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            HintDragMoney?.Invoke(false);
        }
    }
}
