using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using DG.Tweening; 


namespace DOU.PartThree.CashMachine
{
    public class DropCell : MonoBehaviour, IDropHandler
    {
        [SerializeField] private int _countDrop;
        [SerializeField] private int _valueCell;
        [SerializeField] private MoneyReceiver _moneyReceiver;

        private int _defaultMaxDrop;
        private bool _canDrop; public bool CanDrop { set { _canDrop = value; } }

        private void Start()
        {
            _defaultMaxDrop = _countDrop;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<DragMoney>() && _canDrop)
            {
                DragMoney dragMoney = eventData.pointerDrag.GetComponent<DragMoney>();

                if (_moneyReceiver.GetMoney(dragMoney, _valueCell) && _countDrop > 0)
                {
                    eventData.pointerDrag.transform.position = transform.position;

                    _countDrop--;
                    dragMoney.IsDrop = true;
                    dragMoney.GetComponent<Image>().raycastTarget = false;

                    if (_countDrop <= 0)
                        _canDrop = false;
                }

            }
        }

        public void ResetCell()
        {
            _canDrop = true;
            _countDrop = _defaultMaxDrop;
        }

    }
}
