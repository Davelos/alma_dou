using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;


namespace DOU.PartThree.CashMachine
{
    public class BackGround : MonoBehaviour
    {
        [SerializeField] private Image _dayImage;
        [SerializeField] private Image _nightImage;
        [SerializeField] private float crossFadeDuration = 3f;

        public static Action<bool> IsDay;
        public Action IsNight;

        public void DayNight(bool isDay)
        {
            _dayImage.DOFade(isDay ? 1f : 0f, crossFadeDuration);
            _nightImage.DOFade(isDay ? 0f : 1f, crossFadeDuration).OnComplete(() => 
            {
                if (!isDay)
                {
                    DayNight(true);
                    IsNight?.Invoke();
                }
            });

            IsDay?.Invoke(isDay);
        }
    }
}
