using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace DOU.PartThree.CashMachine
{
    public class BankomateCard : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private GameObject _effectCard;

        public static Action InsertCard;
        public static Action<bool> HintEffect;
        public bool _insertCard;

        private Animator _animator;
        private const string NAME_SHOW_CARD_ANIM = "ShowCard";
        private const string NAME_INSERT_CARD_ANIM = "InsertCard";
        private const string NAME_TAKE_CARD_ANIM = "TakeCard";


        private Vector3 _startPosition;
        private int _currentSubIndex;

        private const float DURATION = 0.6f;

        private PersonalAccount _card = new PersonalAccount(); public PersonalAccount Card => _card;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void CartInBankomate()
        {
            InsertCard?.Invoke();
        }

        public void ActivatedCard()
        {
            if (!_animator.enabled) _animator.enabled = true;

            _canvasGroup.alpha = 1f;
            _canvasGroup.interactable = true;
            _canvasGroup.blocksRaycasts = true;
        }

        public void SetPinCodeCard(int pincode)
        {
            _card.PinCode = pincode;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!_insertCard)
            {
                _canvasGroup.blocksRaycasts = false;
                _currentSubIndex = _rectTransform.GetSiblingIndex();
                _rectTransform.SetAsLastSibling();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!_insertCard)
                _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
            _rectTransform.SetSiblingIndex(_currentSubIndex);

            if (!_insertCard)
            {
                _rectTransform.DOMove(_startPosition, DURATION * 1.5f).OnComplete(() =>
                _rectTransform.DOShakePosition(DURATION, 13, 2).OnComplete(() => _animator.enabled = true));
            }
        }

        public void ResetBankomateCard()
        {
            _startPosition = Vector3.zero;
            _card = null;
        }

        #region Animation

        public void PlayAnimInsertCard()
        {
            if (!_animator.enabled) _animator.enabled = true;

            _animator.Play(NAME_INSERT_CARD_ANIM);
            _effectCard.SetActive(false);
            _insertCard = true;
        }

        public void PlayAnimShowCard()
        {
            _animator.Play(NAME_SHOW_CARD_ANIM);
            _insertCard = false;
        }

        public void PlayAnimTakeCard()
        {
            _animator.Play(NAME_TAKE_CARD_ANIM);
        }

        #endregion

        #region EventAnimation
        public void SetStartPositionCard()
        {
            _startPosition = _rectTransform.position;
            _effectCard.SetActive(true);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            HintEffect?.Invoke(true);
            _effectCard.SetActive(false);
            _animator.enabled = false;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            HintEffect?.Invoke(false);
            _effectCard.SetActive(true);
        }

        #endregion
    }
}
