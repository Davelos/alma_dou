using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace DOU.PartFour.ScalesGame
{
    public enum TypeGame
    {
        Coins,
        Banknote,
        All
    }

    public class ScaleGameMenu : MonoBehaviour
    {
        [SerializeField] private MessageManager _messageManager;
        [Space]
        [SerializeField] private Button _coinsModeButton;
        [SerializeField] private Button _banknoteModeButton;
        [SerializeField] private Button _allModeButton;
        [SerializeField] private Button _returnInMenuButton;
        [Space]
        [SerializeField] private Button _easyDifficityButton;
        [SerializeField] private Button _hardDifficityButton;
        [Space]
        [SerializeField] private ScaleGameManager _scalersGameManager;
        [SerializeField] private Animator _animatorLevel;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _coinClip;
        [SerializeField] private AudioClip _banknoteClip;
        [SerializeField] private AudioClip _allClip;
        [Space]
        [SerializeField] private AudioClip _easyClip;
        [SerializeField] private AudioClip _hardClip;
        [Space]
        [SerializeField] private AudioClip _chooseTypeGameClip;
        [SerializeField] private AudioClip _choseTypeDifficityClip;

        private bool _isPush;
        private TypeGame TypeGame;

        private const string NAME_SHOW_CHOOSE_TYPE_GAME = "ChooseDiffliculty";
        private const string NAME_CLOSE_CHOOSE_TYPE_GAME = "CloseChooseLevel";
        private const string NAME_CLOSE_CHOOSE_Difficuty = "CloseChoose";

        private void Start()
        {
            _coinsModeButton.onClick.AddListener(() => ClickMode(_coinsModeButton, TypeGame.Coins));
            _banknoteModeButton.onClick.AddListener(() => ClickMode(_banknoteModeButton, TypeGame.Banknote));
            _allModeButton.onClick.AddListener(() => ClickMode(_allModeButton, TypeGame.All));

            _easyDifficityButton.onClick.AddListener(() => ClickDifficity(_easyDifficityButton, 0));
            _hardDifficityButton.onClick.AddListener(() => ClickDifficity(_hardDifficityButton, 1));

            _returnInMenuButton.onClick.AddListener(ReturnInMeu);

            _scalersGameManager.EndGame += PlayFinishGame;
            _messageManager.EndPreview += PlayStartGame;
            _messageManager.ReplayGame += ReplayGame;
        }

        private void OnDestroy()
        {
            _coinsModeButton.onClick.RemoveAllListeners();
            _banknoteModeButton.onClick.RemoveAllListeners();
            _allModeButton.onClick.RemoveAllListeners();

            _easyDifficityButton.onClick.RemoveAllListeners();
            _hardDifficityButton.onClick.RemoveAllListeners();

            _returnInMenuButton.onClick.RemoveAllListeners();

            _scalersGameManager.EndGame -= PlayFinishGame;
            _messageManager.EndPreview -= PlayStartGame;
            _messageManager.ReplayGame -= ReplayGame;
        }

        private void ClickMode(Button button, TypeGame typeGame)
        {
            if (!_isPush && !_audioSource.isPlaying)
            {
                _isPush = true;

                switch (typeGame)
                {
                    case TypeGame.Coins:
                        _audioSource.PlayOneShot(_coinClip);
                        break;
                    case TypeGame.Banknote:
                        _audioSource.PlayOneShot(_banknoteClip);
                        break;
                    case TypeGame.All:
                        _audioSource.PlayOneShot(_allClip);
                        break;
                }

                button.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
                {
                    button.transform.DOScale(1f, 0.2f).OnComplete(() =>
                    {
                        TypeGame = typeGame;
                        _animatorLevel.Play(NAME_CLOSE_CHOOSE_TYPE_GAME);
                        _isPush = false;
                    });
                });
            }
        }

        private void ClickDifficity(Button button, int difficity)
        {
            if (!_isPush && !_audioSource.isPlaying)
            {
                _isPush = true;

                switch (difficity)
                {
                    case 0:
                        _audioSource.PlayOneShot(_easyClip);
                        break;
                    case 1:
                        _audioSource.PlayOneShot(_hardClip);
                        break;
                }

                button.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
                {
                    button.transform.DOScale(1f, 0.2f).OnComplete(() =>
                    {
                        _scalersGameManager.InisilizationTypeGame(TypeGame, difficity);
                        _animatorLevel.Play(NAME_CLOSE_CHOOSE_Difficuty);
                        _isPush = false;
                    });
                });
            }
        }

        private void PlayFinishGame()
        {
            _messageManager.StartShowBox(false);
        }
        private void PlayStartGame()
        {
            _scalersGameManager.StartGame();
        }

        private void ReturnInMeu()
        {
            _returnInMenuButton.interactable = false;
            _returnInMenuButton.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _returnInMenuButton.transform.DOScale(1f, 0.2f).OnComplete(() =>
                {
                    if (_audioSource.isPlaying) _audioSource.Stop();

                    _animatorLevel.Play(NAME_SHOW_CHOOSE_TYPE_GAME);
                    _returnInMenuButton.interactable = true;
                });
            });
        }

        private void ReplayGame()
        {
            if (_audioSource.isPlaying) _audioSource.Stop();

            _scalersGameManager.ResetGame();
            _animatorLevel.Play(NAME_SHOW_CHOOSE_TYPE_GAME);
        }

        public void AnimationEventPlayClipChooseLevel()
        {
            _audioSource.PlayOneShot(_chooseTypeGameClip);
        }

        public void AnimationEventPlatClipDifficityLevel()
        {
            _audioSource.PlayOneShot(_choseTypeDifficityClip);
        }

        public void AnimationEventPreviewBox()
        {
            _messageManager.StartShowBox(true);
        }

        public void AnimationEventCompletedBox()
        {
            _messageManager.StartShowBox(false);
        }
    }
}