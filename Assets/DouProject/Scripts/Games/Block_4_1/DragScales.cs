using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace DOU.PartFour.ScalesGame
{
    public class DragScales : MonoBehaviour, IDragHandler, IBeginDragHandler, IPointerDownHandler, IEndDragHandler
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private float _dragScale;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private int _valueMoney;
        public int ValueMoney => _valueMoney;
        private Vector3 _startPosition;
        private int _currentSubIndex;

        private const float DURATION = 0.4f;

        private void Start()
        {
            _startPosition = _rectTransform.transform.position;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = false;
            _currentSubIndex = _rectTransform.GetSiblingIndex();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }

        public void ResetPosition(bool reset)
        {
            _canvasGroup.blocksRaycasts = true;

            if (reset)
            {
                _rectTransform.position = _startPosition;
                _rectTransform.localScale = Vector3.one;
            }
            else
            {
                _rectTransform.DOScale(Vector3.one, DURATION);
                _rectTransform.DOMove(_startPosition, DURATION * 1.5f).OnComplete(() =>
                    _rectTransform.SetSiblingIndex(_currentSubIndex)
                );
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _rectTransform.DOScale(_dragScale, DURATION);
            _rectTransform.SetAsLastSibling();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ResetPosition(false);
        }
    }
}
