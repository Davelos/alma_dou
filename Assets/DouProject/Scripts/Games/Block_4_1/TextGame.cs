using UnityEngine;
using TMPro;
using DG.Tweening;


namespace DOU.PartFour.ScalesGame
{
    public class TextGame : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Color _defaultColor;
        [SerializeField] private Color _colorAnim;
        [SerializeField] private GameObject _perffectEffect;
        [SerializeField] private float _duration;

        public TextMeshProUGUI Text { get { return _text; }  set { _text = value; } }

        public void AnimText()
        {
            _text.color = _colorAnim;
            _text.transform.DOScale(1.2f, _duration).OnComplete(() =>
            {
                _text.transform.DOScale(1f, _duration).OnComplete(() => _text.color = _defaultColor);
            });
        }

        public void ActivateEffect(bool efect)
        {
            _perffectEffect.SetActive(efect);
        }

    }
}
