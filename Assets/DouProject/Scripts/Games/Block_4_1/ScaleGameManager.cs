using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


namespace DOU.PartFour.ScalesGame
{
    public class ScaleGameManager : MonoBehaviour
    {
        [SerializeField] private int _counåLevel;
        [SerializeField] private Animator _gameAnim;
        [Space]
        [SerializeField] private DragScales[] _dragScales;
        [SerializeField] private Button _hintButton;
        [SerializeField] private GameObject _hintEffectButton;
        [Space]
        [SerializeField] private Image _leftScalesImageBox;
        [SerializeField] private Image _rightScalesImageBox;
        [SerializeField] private Sprite _coinsSprite;
        [SerializeField] private Sprite _banknoteSprite;
        [SerializeField] private Sprite _allMoneySprite;
        [Space]
        [SerializeField] private GameObject[] _dragBanknote;
        [SerializeField] private GameObject[] _dragCoins;
        [SerializeField] private TextGame _leftScalesText;
        [SerializeField] private TextGame _rightScalesText;
        [Space]
        [SerializeField] private GameObject _coinsPanel;
        [SerializeField] private GameObject _banknotePanel;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _compliments;

        private int _currentLevel;
        private int _leftSum;
        private int _rightSum;
        private bool _isHint;

        private Vector3 _startPositionRightImageBox;

        private const string NAME_START_GAME_ANIM = "StartScalesGame";
        private const string NAME_REPLAY_GAME_ANIM = "ReplayGame";

        private const int MAX_SUM_SCALES = 35000;
        private const int MIN_SUM_SCALES = 7000;
        private int[] DiffirenceCoins = new int[] { 10, 50, 100, 200, 400, 1000 };
        private int[] DiffirenceBanknote = new int[] { 1000, 1900, 3900, 4900 };
        private int[] DiffirenceAll = new int[] { 220, 530, 850, 930, 1020, 1330, 1510 };
        private int[] CurrentDiffirence;

        public Action EndGame;

        private void OnEnable()
        {
            _hintButton.onClick.AddListener(HintButton);
        }
        private void OnDisable()
        {
            _hintButton.onClick.RemoveListener(HintButton);
        }

        public void InisilizationTypeGame(TypeGame typeGame, int difflicity)
        {
            _leftScalesText.gameObject.SetActive(false);
            _rightScalesText.gameObject.SetActive(false);

            _startPositionRightImageBox = _rightScalesImageBox.transform.position;

            switch (typeGame)
            {
                case TypeGame.Coins:
                    _banknotePanel.SetActive(false);
                    _coinsPanel.SetActive(true);

                    CurrentDiffirence = DiffirenceCoins;

                    _rightScalesImageBox.sprite = _coinsSprite;
                    _leftScalesImageBox.sprite = _coinsSprite;
                    break;
                case TypeGame.Banknote:
                    _banknotePanel.SetActive(true);
                    _coinsPanel.SetActive(false);

                    CurrentDiffirence = DiffirenceBanknote;

                    _rightScalesImageBox.sprite = _banknoteSprite;
                    _leftScalesImageBox.sprite = _banknoteSprite;
                    break;
                case TypeGame.All:
                    _banknotePanel.SetActive(true);
                    _coinsPanel.SetActive(true);

                    CurrentDiffirence = DiffirenceAll;

                    _rightScalesImageBox.sprite = _allMoneySprite;
                    _leftScalesImageBox.sprite = _allMoneySprite;
                    break;
            }
        }

        public void StartGame()
        {
            _rightScalesImageBox.transform.position = _startPositionRightImageBox;
            RandomSum();

            if (_currentLevel <= 0)
                _gameAnim.Play(NAME_START_GAME_ANIM);
            else
                _gameAnim.Play(NAME_REPLAY_GAME_ANIM);

            _leftScalesText.AnimText();
            _rightScalesText.AnimText();

            _leftScalesText.gameObject.SetActive(true);
            _rightScalesText.gameObject.SetActive(true);
        }

        private void RandomSum()
        {
            _leftSum = UnityEngine.Random.Range(MIN_SUM_SCALES, MAX_SUM_SCALES);
            _rightSum = _leftSum - CurrentDiffirence[UnityEngine.Random.Range(0, CurrentDiffirence.Length)];

            _leftScalesText.Text.text = _leftSum.ToString();
            _rightScalesText.Text.text = _rightSum.ToString();
        }


        public bool CheckLevel(int sum)
        {
            if ((_rightSum + sum) > _leftSum) return false;

            _rightSum += sum;
            _rightScalesText.Text.text = _rightSum.ToString();
            _rightScalesText.AnimText();

            if (_rightSum == _leftSum)
            {
                _audioSource.PlayOneShot(_compliments[UnityEngine.Random.Range(0, _compliments.Length)]);
                _rightScalesImageBox.transform.DOMoveY(_leftScalesImageBox.transform.position.y, 0.5f);
                _currentLevel++;

                StartCoroutine(CompletedLevel());
            }
            else if (_rightScalesImageBox.transform.position.y > _leftScalesImageBox.transform.position.y)
            {
                _rightScalesImageBox.transform.DOMoveY(_rightScalesImageBox.transform.position.y - 0.05f, 0.5f);
            }

            if (_isHint)
            {
                _isHint = false;
                _hintEffectButton.SetActive(_isHint);
                DisableHint();
            }

            return true;
        }

        private IEnumerator CompletedLevel()
        {
            _leftScalesText.ActivateEffect(true);
            _rightScalesText.ActivateEffect(true);

            yield return new WaitForSeconds(2.5f);

            _leftScalesText.ActivateEffect(false);
            _rightScalesText.ActivateEffect(false);

            if (_currentLevel < _counåLevel)
                StartGame();
            else
                CompletedGame();
        }

        public void ResetGame()
        {
            ActivateDragMoney(_dragCoins);
            ActivateDragMoney(_dragBanknote);

            _rightScalesImageBox.transform.position = _startPositionRightImageBox;
            _currentLevel = 0;
        }

        private void ActivateDragMoney(GameObject[] gameObjects)
        {
            foreach (var obj in gameObjects)
                obj.SetActive(true);
        }

        private void CompletedGame()
        {
            EndGame?.Invoke();
        }

        private void HintButton()
        {
            _isHint = !_isHint;

            _hintButton.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _hintEffectButton.SetActive(_isHint);
                _hintButton.transform.DOScale(1f, 0.2f);

                if (_isHint)
                    Hint();
                else
                    DisableHint();
            });

        }

        private void Hint()
        {
            foreach (var money in _dragScales)
            {
                if ((money.ValueMoney + _rightSum) > _leftSum)
                {
                    money.GetComponent<Image>().color = Color.grey;
                    money.GetComponent<Image>().raycastTarget = false;
                }
                else
                {
                    money.GetComponent<Image>().color = Color.white;
                    money.GetComponent<Image>().raycastTarget = true;
                }
            }
        }

        private void DisableHint()
        {
            foreach (var money in _dragScales)
            {
                money.GetComponent<Image>().color = Color.white;
                money.GetComponent<Image>().raycastTarget = true;
            }
        }
    }
}
