using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;


namespace DOU.PartFour.ScalesGame
{
    public class DropScales : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private ScaleGameManager _scaleGameManager;
        [SerializeField] private CanvasGroup _canvasGroup;

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<DragScales>())
            {
                DragScales dragScales = eventData.pointerDrag.GetComponent<DragScales>();

                if (_scaleGameManager.CheckLevel(dragScales.ValueMoney))
                {
                    dragScales.ResetPosition(true);
                }
                else
                {
                    dragScales.ResetPosition(false);
                }

                FadeDrop(false);
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                if (eventData.pointerDrag.GetComponent<DragScales>())
                {
                    FadeDrop(true);
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                if (eventData.pointerDrag.GetComponent<DragScales>())
                {
                    FadeDrop(false);
                }
            }
        }


        private void FadeDrop(bool inFade)
        {
            _canvasGroup.DOFade(inFade ? 0.5f : 1f, 0.2f);
        }
    }
}
