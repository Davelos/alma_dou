using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DOU.PartFive.StoreGame
{
    public class BtnAnimator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Button _btn;
        [SerializeField] private float _animateDur = 0.25f;
        [SerializeField] private float _endScaleVal = 0.9f;

        private Tween _pressAnimate;

        private void OnValidate()
        {
            if (_btn == null)
                _btn = GetComponent<Button>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!_btn.interactable)
                return;

            _pressAnimate?.Kill();
            _pressAnimate = transform.DOScale(_endScaleVal, _animateDur);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_btn.interactable)
                return;

            _pressAnimate?.Kill();
            _pressAnimate = transform.DOScale(1, _animateDur);
        }
    }
}