using DOU.PartFive.StoreGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ActorsInstaller : MonoInstaller
{
    [SerializeField] private DifficultPanel _difficultPanel;
    [SerializeField] private WinPanel _winPanel;
    [SerializeField] private InfoPanel _infoPanel;
    [SerializeField] private PiggyBank _piggyBank;
    [SerializeField] private RoomsController _roomsController;
    [SerializeField] private InterfaceManager _interfaceManager;

    public override void InstallBindings()
    {
        Container
            .BindInstance(_difficultPanel)
            .AsSingle();
        Container
            .BindInstance(_winPanel)
            .AsSingle();
        Container
            .BindInstance(_infoPanel)
            .AsSingle();
        Container
            .BindInstance(_piggyBank)
            .AsSingle();
        Container
            .BindInstance(_roomsController)
            .AsSingle();
        Container
            .BindInstance(_interfaceManager)
            .AsSingle();
    }
}
