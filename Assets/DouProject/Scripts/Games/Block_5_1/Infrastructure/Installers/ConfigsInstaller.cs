﻿using ALMA.VoiceoversService;
using DOU.PartFive.StoreGame;
using System;
using UnityEngine;
using Zenject;

public class ConfigsInstaller : MonoInstaller
{
    [SerializeField] private LevelDifficult[] _difficults;
    [SerializeField] private string _difficultConfigDirPath;
    [SerializeField] private string _gameItemsConfigPath;
    [SerializeField] private string _gameMoneyConfigPath;
    [SerializeField] private string _localizeVoiceoversConfigPath;

    public override void InstallBindings()
    {
        InstallDifficultConfigs();
        InstallGameMoneyConfigs();
        InstallGameItemConfigs();
        InstallVoiceoversConfig();
    }

    private void InstallVoiceoversConfig()
    {
        var config = Resources.Load<LocalizeVoiceoversPaths>(_localizeVoiceoversConfigPath);
        if (config == null)
            Debug.LogError("LocalizeVoiceoversPaths is not loaded!");

        Container
            .Bind<LocalizeVoiceoversPaths>()
            .FromInstance(config)
            .AsCached();
    }

    private void InstallDifficultConfigs()
    {
        foreach (var difficult in _difficults)
        {
            string configPath = $"{_difficultConfigDirPath}/{difficult}Difficult";
            var config = Resources.Load<LevelDifficultConfig>(configPath);
            if (config == null)
                Debug.LogError("LevelDifficultConfig is not loaded!");

            Container
                .Bind<LevelDifficultConfig>()
                .FromInstance(config) 
                .AsCached();
        }
    }

    private void InstallGameItemConfigs()
    {
        var config = Resources.Load<GameItemsConfig>(_gameItemsConfigPath);
        if (config == null)
            Debug.LogError("GameItemsConfig is not loaded!");

        Container
            .Bind<GameItemsConfig>()
            .FromInstance(config)
            .AsCached();
    }

    private void InstallGameMoneyConfigs()
    {
        var config = Resources.Load<GameMoneyConfig>(_gameMoneyConfigPath);
        if (config == null)
            Debug.LogError("GameMoneyConfig is not loaded!");

        Container
            .Bind<GameMoneyConfig>()
            .FromInstance(config)
            .AsCached();
    }
}
