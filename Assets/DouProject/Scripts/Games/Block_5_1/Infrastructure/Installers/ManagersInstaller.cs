﻿using DOU.PartFive.StoreGame;
using Zenject;

public class ManagersInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container
            .BindInterfacesTo<LevelManager>()
            .AsSingle();
        Container
            .BindInterfacesTo<RoomsAndBankMediator>()
            .AsSingle();
    }
}
