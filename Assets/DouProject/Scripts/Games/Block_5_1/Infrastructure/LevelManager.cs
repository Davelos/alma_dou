using ALMA.VoiceoversService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Zenject;

using Debug = UnityEngine.Debug;

namespace DOU.PartFive.StoreGame
{
    public class LevelManager : IInitializable, IDisposable
    {
        private readonly DifficultPanel _difficultPanel;
        private readonly WinPanel _winPanel;
        private readonly InfoPanel _infoPanel;
        private readonly PiggyBank _piggyBank;
        private readonly RoomsController _roomsController;
        private readonly InterfaceManager _interfaceManager;
        private readonly VoiceoverPlayer _voiceoverPlayer;

        private int _roundsCount;
        private int _curRound;

        public LevelManager(DifficultPanel difficultPanel, WinPanel winPanel, InfoPanel infoPanel, PiggyBank piggyBank, RoomsController roomsController, 
            InterfaceManager interfaceManager, VoiceoverPlayer voiceoverPlayer, LocalizeVoiceoversPaths localizeVoiceovers)
        {
            _difficultPanel = difficultPanel;
            _difficultPanel.OnDifficultySelected += DifficultySelected;
            _difficultPanel.gameObject.SetActive(false);

            _winPanel = winPanel;
            _winPanel.OnRestartBtnPressed += RestartGame;
            _winPanel.gameObject.SetActive(false);

            _infoPanel = infoPanel;
            _infoPanel.gameObject.SetActive(false);

            _piggyBank = piggyBank;
            _piggyBank.OnRightAnswerSelected += RightAnswerSelected;
            _piggyBank.gameObject.SetActive(false);

            _roomsController = roomsController;
            _roomsController.GoToStartPosition();

            _interfaceManager = interfaceManager;
            _interfaceManager.OnRestartBtnPress += RestartGame;
            _interfaceManager.RestartGameBtn.gameObject.SetActive(false);

            _voiceoverPlayer = voiceoverPlayer;
            _voiceoverPlayer.LoadLessonVoiceovers(localizeVoiceovers);
        }

        public void Dispose()
        {
            _difficultPanel.OnDifficultySelected -= DifficultySelected;
            _piggyBank.OnRightAnswerSelected -= RightAnswerSelected;
            _interfaceManager.OnRestartBtnPress -= RestartGame;
            _voiceoverPlayer.UnloadLessonVoiceovers();
        }

        public void Initialize()
        {
            _difficultPanel.Show(false);
        }

        private void DifficultySelected(LevelDifficultConfig levelDifficultConfig)
        {
            _roundsCount = levelDifficultConfig.RoundsCount;
            _piggyBank.Initialize(levelDifficultConfig.MoneyCount, levelDifficultConfig.MinRangeCost);
            _roomsController.Initialize(levelDifficultConfig.MinRangeCost, levelDifficultConfig.MaxRangeCost);

            StartGame();
        }

        private void RightAnswerSelected()
        {
            if (!TryStartNextRound())
                FinishGame();
        }

        private void StartGame()
        {
            _curRound = -1;
            if (!TryStartNextRound(false))
                throw new Exception("It is impossible to start the game, _roundsCount is not initialized!");

            _difficultPanel.Hide(() =>
            {
                _interfaceManager.RestartGameBtn.gameObject.SetActive(true);
                _roomsController.MoveToCurRoom(false);
                _piggyBank.Show();
                _infoPanel.Show();
            });
        }

        private void FinishGame()
        {
            _winPanel.Show();
        }

        private void RestartGame()
        {
            _interfaceManager.RestartGameBtn.gameObject.SetActive(false);
            _roomsController.GoToStartPosition(true);
            _winPanel.Hide();
            _piggyBank.Hide();
            _difficultPanel.Show(true);
        }

        private bool TryStartNextRound(bool autoMove = true)
        {
            if (++_curRound == _roundsCount)
                return false;

            _roomsController.GoToNextRoom();

            if (autoMove)
                _roomsController.MoveToCurRoom();

            return true;
        }
    }
}