using DOU.PartFive.StoreGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsAndBankMediator : IDisposable
{
    private RoomsController _roomsController;
    private PiggyBank _piggyBank;

    public RoomsAndBankMediator(RoomsController roomsController, PiggyBank piggyBank)
    {
        _roomsController = roomsController;
        _piggyBank = piggyBank;

        _roomsController.OnRoomSwitched += RoomsController_OnRoomSwitched;
        foreach (var room in _roomsController.Rooms)
            room.Rack.OnItemSelected += Rack_OnItemSelected;
    }

    public void Dispose()
    {
        _roomsController.OnRoomSwitched -= RoomsController_OnRoomSwitched;
        foreach (var room in _roomsController.Rooms)
            room.Rack.OnItemSelected -= Rack_OnItemSelected;
    }

    private void Rack_OnItemSelected(int selectedCost, int[] otherCosts)
    {
        _piggyBank.CheckAnswer(selectedCost, otherCosts);
    }

    private void RoomsController_OnRoomSwitched(RoomSwichEventArgs args)
    {
        var newBankCost = _piggyBank.RefreshMoney();
        args.PiggyBankCosts = newBankCost;
    }
}
