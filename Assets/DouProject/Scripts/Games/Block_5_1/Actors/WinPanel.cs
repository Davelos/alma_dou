﻿using Assets.SimpleLocalization.Scripts;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFive.StoreGame
{
    public class WinPanel : MonoBehaviour
    {
        public event Action OnRestartBtnPressed;

        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private string _gameNameKey;
        [SerializeField] private string _titleKey;
        [SerializeField] private TextMeshProUGUI _titleTMP;
        [SerializeField] private Button _backToMenuBtn;
        [SerializeField] private Button _restartBtn;
        [SerializeField, Range(0.2f, 2f)] private float _openCloseAnimDur = 0.5f;

        private Tween _openCloseAnim;

        [Inject]
        private void Construct(ISceneLoader sceneLoader)
        {
            InitializeBtns(sceneLoader);
        }

        private void Start()
        {
            Localize();
            LocalizationManager.OnLocalizationChanged += Localize;
        }

        private void OnValidate()
        {
            if (_canvasGroup == null)
                _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnDestroy()
        {
            LocalizationManager.OnLocalizationChanged -= Localize;
        }

        public void Show()
        {
            if (gameObject.activeSelf)
                return;

            gameObject.SetActive(true);
            transform.localScale = new Vector3(1f, 0f, 1f);
            _canvasGroup.alpha = 1f;

            _openCloseAnim?.Kill();
            _openCloseAnim = transform.DOScaleY(1f, _openCloseAnimDur).SetEase(Ease.OutBack);
        }

        public void Hide()
        {
            if (!gameObject.activeSelf)
                return;

            transform.localScale = Vector3.one;

            _openCloseAnim?.Kill();
            _openCloseAnim = _canvasGroup.DOFade(0f, _openCloseAnimDur)
                .OnComplete(() => gameObject.SetActive(false));
        }

        private void InitializeBtns(ISceneLoader sceneLoader)
        {
            _backToMenuBtn.onClick.AddListener(sceneLoader.UnloadSceneAndGoToMenu);
            _restartBtn.onClick.AddListener(() => OnRestartBtnPressed?.Invoke());
        }

        private void Localize()
        {
            string gameName = LocalizationManager.Localize(_gameNameKey);
            _titleTMP.text = LocalizationManager.Localize(_titleKey, gameName);
        }
    }
}