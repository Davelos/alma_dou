﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFive.StoreGame
{
    public class DifficultPanel : MonoBehaviour
    {
        public event Action<LevelDifficultConfig> OnDifficultySelected;

        [SerializeField] private CanvasGroup _thisCanvasGroup;
        [SerializeField] private Button _eazeDifficultBtn;
        [SerializeField] private Button _hardDifficultBtn;
        [SerializeField, Range(0.1f, 1f)] private float _activateDelay = 1f;
        [SerializeField, Range(0.1f, 2f)] private float _fadeAnimateDur = 1f;

        [Inject]
        private void Construct(ISceneLoader sceneLoader, LevelDifficultConfig[] difficultConfigs)
        {
            InitializeBtns(sceneLoader, difficultConfigs);
        }

        private void OnValidate()
        {
            if (_thisCanvasGroup == null)
                _thisCanvasGroup = GetComponent<CanvasGroup>();
        }

        public void Show(bool useAnim, Action onAnimComlete = null)
        {
            _thisCanvasGroup.blocksRaycasts = true;
            gameObject.SetActive(true);
            if (useAnim)
            {
                _thisCanvasGroup.alpha = 0f;
                _thisCanvasGroup.DOKill();
                _thisCanvasGroup.DOFade(1f, _fadeAnimateDur)
                    .OnComplete(() => onAnimComlete?.Invoke());
            }
        }

        public void Hide(Action onAnimComlete = null)
        {
            _thisCanvasGroup.DOKill();
            _thisCanvasGroup.DOFade(0f, _fadeAnimateDur)
                .OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    onAnimComlete?.Invoke(); 
                });
        }

        private void InitializeBtns(ISceneLoader sceneLoader, LevelDifficultConfig[] difficultConfigs)
        {
            InitializeDifficultBtns(difficultConfigs);
        }

        private void InitializeDifficultBtns(LevelDifficultConfig[] difficultConfigs)
        {
            foreach (var difficultConfig in difficultConfigs)
            {
                Button curDifficultBtn = difficultConfig.Difficult switch
                {
                    LevelDifficult.Eaze => _eazeDifficultBtn,
                    LevelDifficult.Hard => _hardDifficultBtn,
                    _ => throw new ArgumentException($"Difficult {difficultConfig.Difficult} is not found!"),
                };

                curDifficultBtn.onClick.AddListener(() =>
                {
                    _thisCanvasGroup.blocksRaycasts = false;
                    StartCoroutine(DifficultBtnPressRoutine(difficultConfig));
                });
            }
        }

        private IEnumerator DifficultBtnPressRoutine(LevelDifficultConfig difficultConfig)
        {
            yield return new WaitForSeconds(_activateDelay);
            OnDifficultySelected?.Invoke(difficultConfig);
        }
    }
}