using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour
{
    public event Action OnRestartBtnPress;

    [field: SerializeField] public Button RestartGameBtn { get; private set; }

    private void Awake()
    {
        RestartGameBtn.onClick.AddListener(RestrtBtnPressed);
    }

    private void OnDestroy()
    {
        RestartGameBtn.onClick.RemoveListener(RestrtBtnPressed);
    }

    private void RestrtBtnPressed()
    {
        OnRestartBtnPress?.Invoke();
    }
}
