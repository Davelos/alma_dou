﻿using ALMA.VoiceoversService;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFive.StoreGame
{
    public class InfoPanel : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
    {
        [SerializeField] private TextMeshProUGUI _textMeshPro;
        [SerializeField, Range(0.2f, 2f)] private float _openCloseAnimDur = 1f;

        private Sequence _openCloseAnim;
        private VoiceoverPlayer _voiceoverPlayer;

        [Inject]
        private void Construct(VoiceoverPlayer voiceoverPlayer)
        {
            _voiceoverPlayer = voiceoverPlayer;
        }

        private void OnValidate()
        {
            if (_textMeshPro == null)
                _textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
        }

        public void Show()
        {
            if (gameObject.activeSelf)
                return;

            gameObject.SetActive(true);
            transform.localScale = new Vector3(1f, 0f, 1f);
            _textMeshPro.alpha = 1f;

            _openCloseAnim?.Kill();
            _openCloseAnim = DOTween.Sequence()
                .Append(transform.DOScaleY(1f, _openCloseAnimDur).SetEase(Ease.OutBack))
                .OnComplete(() => _voiceoverPlayer.Play("Ch5_Game1_brief"));
        }

        public void Hide()
        {
            if (!gameObject.activeSelf)
                return;

            float firstAnimDur = _openCloseAnimDur * 0.33f;
            float secondAnimDur = _openCloseAnimDur - firstAnimDur;
            transform.localScale = Vector3.one;
            _textMeshPro.alpha = 1f;

            _openCloseAnim?.Kill();
            _openCloseAnim = DOTween.Sequence()
                .Append(_textMeshPro.DOFade(0f, firstAnimDur))
                .Append(transform.DOScaleY(0f, secondAnimDur).SetEase(Ease.OutBounce))
                .OnComplete(() => gameObject.SetActive(false));
            _voiceoverPlayer.Stop();
        }

        public void OnPointerDown(PointerEventData eventData) { }

        public void OnPointerUp(PointerEventData eventData) => Hide();
    }
}