﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RackItemBtn : MonoBehaviour
{
    [field:SerializeField] public Button Btn {get; private set; }
    [field: SerializeField] public TextMeshProUGUI PriceText { get; private set; }

    private void OnValidate()
    {
        if (Btn == null)
            Btn = GetComponent<Button>();
        if (PriceText == null)
            PriceText = GetComponentInChildren<TextMeshProUGUI>();
    }
}