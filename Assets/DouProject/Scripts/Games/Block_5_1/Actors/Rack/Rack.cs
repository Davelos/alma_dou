﻿using System;
using System.Linq;
using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    public class Rack : MonoBehaviour
    {
        public event Action<int, int[]> OnItemSelected;

        [SerializeField] private RackItemBtn[] _itemsBtns;

        private Product[] _products;

        private void Awake()
        {
            for (int i = 0; i < _itemsBtns.Length; i++)
            {
                int selectedProductId = i;
                _itemsBtns[i].Btn.onClick.AddListener(() => ProductWasSelected(selectedProductId));
            }
        }

        public void Refresh(Product[] products)
        {
            if (products.Length == 0 || products.Length > _itemsBtns.Length)
                throw new ArgumentOutOfRangeException("products.Length is incorrect!");

            _products = products;
            for (int i = 0; i < _itemsBtns.Length; i++)
            {
                var btn = _itemsBtns[i];
                Product product = products[i];

                btn.Btn.image.sprite = product.Sprite;
                btn.PriceText.text = product.Cost.ToString();
                btn.Btn.interactable = true;
            }
        }

        public void Block()
        {
            foreach (var btn in _itemsBtns)
                btn.Btn.interactable = false;
        }

        private void ProductWasSelected(int productId)
        {
            int selectedCost = 0;
            int[] otherCosts = new int[_products.Length - 1];

            for (int i = 0, j = 0; i < _products.Length; i++)
            {
                if (i == productId)
                    selectedCost = _products[i].Cost;
                else
                    otherCosts[j++] = _products[i].Cost;
            }

            OnItemSelected?.Invoke(selectedCost, otherCosts);
        }
    }
}
