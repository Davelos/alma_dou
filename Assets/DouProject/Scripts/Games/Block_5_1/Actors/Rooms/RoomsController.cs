﻿using System;
using UnityEngine;
using Zenject;
using DG.Tweening;

using Random = UnityEngine.Random;
using ALMA.VoiceoversService;

namespace DOU.PartFive.StoreGame
{
    public class RoomsController : MonoBehaviour
    {
        public delegate void RoomSwichEvent(RoomSwichEventArgs args);
        public event RoomSwichEvent OnRoomSwitched;

        public Room[] Rooms => _rooms;

        [SerializeField] private Transform _startPositionPoint;
        [SerializeField] private Room[] _rooms;
        [SerializeField] private int _productOnRackCount = 3;
        [SerializeField, Range(2, 10000)] private int _minRangeOffset = 50;
        [SerializeField, Range(0.2f, 2f)] private float _moveAnimationDuration = 1f;

        private RoomSwichEventArgs _swichEventArgs = new();
        private Sprite[] _allItemsSprites;
        private Room _curRoom;
        private VoiceoverPlayer _voiceoverPlayer;
        private int _minRangeCost;
        private int _maxRangeCost;
        private int _currentSpawnItemId = 0;
        private int _currentRoomId = 0;
        private string[] _voiceoverQuestionKeys = new string[]
        {
            "Ch5_Game1_q1",
            "Ch5_Game1_q2",
            "Ch5_Game1_q3",
            "Ch5_Game1_q4",
        };

        [Inject]
        private void Construct(GameItemsConfig gameItemsConfig, VoiceoverPlayer voiceoverPlayer)
        {
            _allItemsSprites = new Sprite[gameItemsConfig.AllItemsSprites.Length];
            gameItemsConfig.AllItemsSprites.CopyTo(_allItemsSprites, 0);

            _voiceoverPlayer = voiceoverPlayer;
        }

        public void Initialize(int minRangeCost, int maxRangeCost)
        {
            _minRangeCost = minRangeCost;
            _maxRangeCost = maxRangeCost;

            Utils.ShuffleArray(_allItemsSprites);
            _currentSpawnItemId = -1;
        }

        public void GoToNextRoom()
        {
            if (_curRoom != null)
                _curRoom.Rack.Block();

            SetNextRoom();
            RefreshRack();
        }

        public void MoveToCurRoom(bool playVoiceover = true)
        {
            if (playVoiceover)
            {
                int randomId = Random.Range(0, _voiceoverQuestionKeys.Length);
                _voiceoverPlayer.Play(_voiceoverQuestionKeys[randomId]);
            }

            transform.DOMove(_curRoom.PositionPoint.position, _moveAnimationDuration);
        }

        public void GoToStartPosition(bool useAnim = false)
        {
            if (useAnim)
                transform.DOMove(_startPositionPoint.position, _moveAnimationDuration * 0.5f);
            else
                transform.position = _startPositionPoint.position;

            _currentRoomId = -1;
        }

        private void RefreshRack()
        {
            _curRoom.Rack.Refresh(GetRandomProducts(_productOnRackCount, _swichEventArgs.PiggyBankCosts));
        }

        private void SetNextRoom()
        {
            _currentRoomId = (_currentRoomId + 1) % _rooms.Length;
            _curRoom = _rooms[_currentRoomId];
            OnRoomSwitched.Invoke(_swichEventArgs);
        }

        private Product[] GetRandomProducts(int count, int rigthAnswer)
        {
            if (_currentSpawnItemId + count > _allItemsSprites.Length)
                throw new IndexOutOfRangeException("More items than possible will be taken from the _allItemsSprites!");

            Product[] products = new Product[count];
            int randomRightId = Random.Range(0, count);

            for (int i = 0; i < count; i++)
            {
                _currentSpawnItemId++;
                Sprite productSprite = _allItemsSprites[_currentSpawnItemId];
                int productCost = GenerateRandomCost(i == randomRightId, rigthAnswer);
                products[i] = new Product(productSprite, productCost);
            }
            return products;
        }

        private int GenerateRandomCost(bool isRightAnswer, int rightAnswer)
        {
            int maxRangeOffset = rightAnswer / 2;
            maxRangeOffset = maxRangeOffset < _minRangeOffset ? _minRangeOffset : maxRangeOffset;

            int minCost = rightAnswer - maxRangeOffset;
            minCost = minCost < _minRangeCost ? _minRangeCost : minCost;

            int maxCost = (isRightAnswer ? rightAnswer : rightAnswer + maxRangeOffset) + 1;
            return Random.Range(minCost, maxCost);
        }
    }
}