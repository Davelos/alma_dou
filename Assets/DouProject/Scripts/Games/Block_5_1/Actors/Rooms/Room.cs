﻿using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    public class Room : MonoBehaviour
    {
        [field:SerializeField] public Transform PositionPoint {get; private set;}
        [field: SerializeField] public Rack Rack { get; private set; }
    }
}