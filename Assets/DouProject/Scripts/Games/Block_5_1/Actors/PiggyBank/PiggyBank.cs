﻿using ALMA.VoiceoversService;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

using Random = UnityEngine.Random;

namespace DOU.PartFive.StoreGame
{
    public class PiggyBank : MonoBehaviour
    {
        public event Action OnRightAnswerSelected;

        [SerializeField] private PiggyBankBtn _showAllMoneyCostBtn;
        [SerializeField] private RectTransform _moneyParent;
        [SerializeField] private CanvasGroup _moneyCanvasGroup;
        [SerializeField, Range(0.2f, 2f)] private float _showAnimateDuration = 1f;
        [SerializeField] private float _switchAnimDur = 0.6f;

        private Money[] _allMoneyInGame;
        private List<Image> _spawnedMoneyImages;
        private VoiceoverPlayer _voiceoverPlayer;
        private int _allMoneyCost;
        private int _minMoneyCost;
        private bool _initialized;
        private string[] _voiceoverPosKeys = new string[]
        {
            "Ch5_Game1_react_pos1",
            "Ch5_Game1_react_pos2",
            "Ch5_Game1_react_pos3",
            "Ch5_Game1_react_pos4",
        };

        private string[] _voiceoverNegKeys = new string[]
        {
            "Ch5_Game1_react_neg3",
            "Ch5_Game1_react_neg4",
            "Ch5_Game1_react_neg5",
        };

        private string[] _voiceoverHasMoreKeys = new string[]
        {
            "Ch5_Game1_react_has_more1",
            "Ch5_Game1_react_has_more2",
        };

        [Inject]
        private void Construct(GameMoneyConfig gameMoneyConfig, VoiceoverPlayer voiceoverPlayer)
        {
            _allMoneyInGame = new Money[gameMoneyConfig.AllMoney.Length];
            gameMoneyConfig.AllMoney.CopyTo(_allMoneyInGame, 0);

            _voiceoverPlayer = voiceoverPlayer;
        }

        private void Awake()
        {
            _showAllMoneyCostBtn.OnClick.AddListener(ShowAllMoneyCost);
        }

        private void OnValidate()
        {
            if (_moneyCanvasGroup == null && _moneyParent != null)
                _moneyCanvasGroup = _moneyParent.GetComponent<CanvasGroup>();
        }

        public void Initialize(int moneyCount, int minMoneyCost)
        {
            if (_initialized)
                Reinitialize(moneyCount);
            else
                FirstInitialize(moneyCount);

            _minMoneyCost = minMoneyCost;
            Utils.RefreshLayout(_moneyParent);
        }

        public int RefreshMoney()
        {
            _allMoneyCost = 0;
            _showAllMoneyCostBtn.ResetText();

            (Money[], int) moneyAndCost = GenerateRandomMoney();
            SwitchMoneyAnimate(() => SetRandomMoney(moneyAndCost));

            return moneyAndCost.Item2;
        }

        public void CheckAnswer(int curItemCost, int[] otherItemsCosts)
        {
            if (IsExpensive(curItemCost))
            {
                //Debug.Log("Этот предмет слишком дорогой!");
                PlayRandomVoiceover(_voiceoverNegKeys);
                return;
            }

            if (IsCheap(curItemCost, otherItemsCosts))
            {
                //Debug.Log("Этот предмет не самый дорогой!");
                PlayRandomVoiceover(_voiceoverHasMoreKeys);
                return;
            }

            //Debug.Log("Этот подходит");
            float clipLength = PlayRandomVoiceover(_voiceoverPosKeys);
            StartCoroutine(RightAnswerSelectedRoutine(clipLength));
        }

        public void Show()
        {
            gameObject.SetActive(true);

            RectTransform rectTransform = (transform as RectTransform);
            rectTransform.anchoredPosition = new Vector2(Screen.currentResolution.width, rectTransform.anchoredPosition.y);
            rectTransform.DOKill();
            rectTransform.DOAnchorPosX(0, _showAnimateDuration);
        }

        public void Hide()
        {
            RectTransform rectTransform = (transform as RectTransform);
            rectTransform.DOKill();
            rectTransform.DOAnchorPosX(Screen.currentResolution.width, _showAnimateDuration)
                .OnComplete(() => gameObject.SetActive(false));
        }

        private float PlayRandomVoiceover(string[] voiceoversKeys)
        {
            int randomId = Random.Range(0, voiceoversKeys.Length);
            return _voiceoverPlayer.Play(voiceoversKeys[randomId]);
        }

        private IEnumerator RightAnswerSelectedRoutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            OnRightAnswerSelected?.Invoke();
        }

        private void Reinitialize(int moneyCount)
        {
            int sizeDiff = _spawnedMoneyImages.Count - moneyCount;
            if (sizeDiff == 0)
                return;

            if (sizeDiff > 0)// need to remove the excess
            {
                int removeCount = sizeDiff, curDelId;
                for (curDelId = _spawnedMoneyImages.Count - 1; sizeDiff > 0; curDelId--, sizeDiff--)
                    Destroy(_spawnedMoneyImages[curDelId].gameObject);

                _spawnedMoneyImages.RemoveRange(curDelId + 1, removeCount);
            }
            else// need to build the drawback
            {
                int buildCount = Math.Abs(sizeDiff);
                BuildMoney(buildCount, _spawnedMoneyImages.Count);
            }
        }

        private void FirstInitialize(int moneyCount)
        {
            _spawnedMoneyImages = new(moneyCount);
            BuildMoney(moneyCount);
            _initialized = true;
        }

        private void BuildMoney(int moneyCount, int startId = 0)
        {
            int endId = startId + moneyCount;
            for (int i = startId; i < endId; i++)
            {
                var moneyImage = BuildMoney($"Money_{i}");
                _spawnedMoneyImages.Add(moneyImage);
            }
        }

        private bool IsExpensive(int curItemCost) => curItemCost > _allMoneyCost;

        private bool IsCheap(int curItemCost, int[] otherItemsCosts)
        {
            foreach (var otherItemCost in otherItemsCosts)
            {
                if (otherItemCost > curItemCost && !IsExpensive(otherItemCost))
                    return true;
            }
            return false;
        }

        private void SetRandomMoney((Money[], int) moneyAndCost)
        {
            for (int i = 0; i < _spawnedMoneyImages.Count; i++)
            {
                Money money = moneyAndCost.Item1[i];
                _spawnedMoneyImages[i].sprite = money.Sprite;
            }
            _allMoneyCost = moneyAndCost.Item2;
        }

        private (Money[], int) GenerateRandomMoney()
        {
            int moneyCount = _spawnedMoneyImages.Count,
                curMoneyCost = 0;
            Money[] resultMoney = new Money[moneyCount];

            for (int i = 0; i < moneyCount; i++)
            {
                Money randomMoney; int moneyCost;

                do
                {
                    randomMoney = GetRandomCoin();
                    moneyCost = curMoneyCost + randomMoney.Cost;
                } while (i == moneyCount - 1 && moneyCost < _minMoneyCost);

                curMoneyCost = moneyCost;
                resultMoney[i] = randomMoney;
            }

            return (resultMoney, curMoneyCost);
        }

        private Money GetRandomCoin(int minInclusive = 0)
        {
            int randomId = Random.Range(minInclusive, _allMoneyInGame.Length);
            return _allMoneyInGame[randomId];
        }

        private void ShowAllMoneyCost()
        {
            _showAllMoneyCostBtn.ShowCost(_allMoneyCost);
        }

        private Image BuildMoney(string name)
        {
            Image image = new GameObject(name)
                            .AddComponent<Image>();
            image.transform.SetParent(_moneyParent);
            image.preserveAspect = true;
            image.transform.localScale = Vector3.one;
            return image;
        }

        private void SwitchMoneyAnimate(Action onHided)
        {
            float oneAnimDur = _switchAnimDur * 0.5f;
            _moneyCanvasGroup.DOFade(0f, oneAnimDur)
                .OnComplete(() =>
                {
                    onHided();
                    _moneyCanvasGroup.DOFade(1f, oneAnimDur);
                });
        }
    }
}