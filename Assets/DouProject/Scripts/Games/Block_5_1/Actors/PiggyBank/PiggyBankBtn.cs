﻿using Assets.SimpleLocalization.Scripts;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartFive.StoreGame
{
    public class PiggyBankBtn : MonoBehaviour
    {
        public Button.ButtonClickedEvent OnClick => _showAllMoneyCostBtn.onClick;

        [SerializeField] private Button _showAllMoneyCostBtn;
        [SerializeField] private TextMeshProUGUI _allMoneyCostText;
        [SerializeField] private string _showAllMoneyCostBtnLocalizeKey;
        [SerializeField] private float _switchAnimDur = 0.6f;

        private bool _isCostShowed;

        private void Start()
        {
            Localize();
            LocalizationManager.OnLocalizationChanged += Localize;
        }

        private void OnDestroy()
        {
            LocalizationManager.OnLocalizationChanged -= Localize;
        }

        #region ONLY UNITY EDITOR
#if UNITY_EDITOR
        private void OnValidate()
        {
            ValidateShowAllMoneyCostBtn();
        }

        private void ValidateShowAllMoneyCostBtn()
        {
            if (_showAllMoneyCostBtn == null)
                _showAllMoneyCostBtn = GetComponent<Button>();
            if (_allMoneyCostText == null && _showAllMoneyCostBtn != null)
                _allMoneyCostText = GetComponentInChildren<TextMeshProUGUI>();
            if (_showAllMoneyCostBtn == null && _allMoneyCostText != null)
                _allMoneyCostText = null;
        }
#endif
        #endregion

        public void ShowCost(int allMoneyCost)
        {
            _isCostShowed = true;
            _showAllMoneyCostBtn.interactable = false;

            SwitchTextAnimate(() => _allMoneyCostText.text = allMoneyCost.ToString());
        }

        public void ResetText()
        {
            if (!_isCostShowed)
                return;

            _showAllMoneyCostBtn.interactable = true;
            _isCostShowed = false;
            SwitchTextAnimate(Localize);
        }

        private void SwitchTextAnimate(Action onHided)
        {
            float oneAnimDur = _switchAnimDur * 0.5f;
            _allMoneyCostText.DOFade(0f, oneAnimDur)
                .OnComplete(() =>
                {
                    onHided();
                    _allMoneyCostText.DOFade(1f, oneAnimDur);
                });
        }

        private void Localize()
        {
            if (_isCostShowed)
                return;

            _allMoneyCostText.text = LocalizationManager.Localize(_showAllMoneyCostBtnLocalizeKey);
        }
    }
}