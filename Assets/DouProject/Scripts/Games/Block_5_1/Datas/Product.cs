﻿using System;
using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    public class Product : FinancialObject
    {
        public Product(Sprite sprite, int cost) : base(sprite, cost)
        {
        }
    }
}