﻿using System;
using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    [Serializable]
    public abstract class FinancialObject
    {
        protected FinancialObject(Sprite sprite, int cost)
        {
            Sprite = sprite;
            Cost = cost;
        }

        [field:SerializeField] public Sprite Sprite { get; private set; }
        [field:SerializeField] public int Cost{ get; private set; }
    }
}