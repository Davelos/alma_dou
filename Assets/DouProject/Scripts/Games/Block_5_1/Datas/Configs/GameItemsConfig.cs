﻿using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    [CreateAssetMenu(fileName = "NewGameItemsConfig", menuName = "Settings/Part5/StoreGame/GameItemsConfig")]
    public class GameItemsConfig : ScriptableObject
    {
        [field: SerializeField] public Sprite[] AllItemsSprites { get; private set; }
    }
}