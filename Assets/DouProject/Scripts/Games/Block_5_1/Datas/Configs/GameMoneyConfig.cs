﻿using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    [CreateAssetMenu(fileName = "NewGameMoneyConfig", menuName = "Settings/Part5/StoreGame/GameMoneyConfig")]
    public class GameMoneyConfig : ScriptableObject
    {
        [field: SerializeField] public Money[] AllMoney { get; private set; }
    }
}