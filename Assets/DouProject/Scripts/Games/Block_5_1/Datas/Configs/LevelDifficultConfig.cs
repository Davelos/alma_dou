﻿using UnityEngine;

namespace DOU.PartFive.StoreGame
{
    [CreateAssetMenu(fileName = "NewLevelDifficultConfig", menuName = "Settings/Part5/StoreGame/LevelDifficultConfig")]
    public class LevelDifficultConfig : ScriptableObject
    {
        [field: SerializeField] public LevelDifficult Difficult { get; private set; }
        [field: SerializeField] public int MinRangeCost { get; private set; } = 2;
        [field: SerializeField] public int MaxRangeCost { get; private set; }
        [field: SerializeField] public int MoneyCount { get; private set; }
        [field: SerializeField] public int RoundsCount { get; private set; } = 6;
    }
}