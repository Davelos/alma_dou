﻿using System;
using UnityEngine;

namespace DOU.PartFive.StoreGame
{

    [Serializable]
    public class Money : FinancialObject
    {
        public Money(Sprite sprite, int cost) : base(sprite, cost)
        {
        }
    }
}