using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class MenuBlockOne : Singleton<MenuBlockOne>
{
    [SerializeField] private LoaderScene _loaderScene;
    [Space]
    [SerializeField] private Button _informationButton;
    [SerializeField] private Button _puzzleMoneyButton;
    [SerializeField] private Button _changeMoneyButton;
    [SerializeField] private Button _mainMenuButton;
    [Space]
    [SerializeField] private AudioClip _clipInformation;
    [SerializeField] private AudioClip _clipPuzzleMoney;
    [SerializeField] private AudioClip _clipChangeMoney;
    [SerializeField] private AudioSource _audioSource;

    private const string INFORMATION_SCENE = "InformationMoney";
    private const string PUZZLE_SCENE = "PuzzleMoney";
    private const string CHANGE_SCENE = "ChangeMoney";

    private Sequence _sequence;

    private bool _isPush = false;

    void Start()
    {
        _informationButton.onClick.AddListener(() => PlayGame(INFORMATION_SCENE, _informationButton, _clipInformation));
        _puzzleMoneyButton.onClick.AddListener(() => PlayGame(PUZZLE_SCENE, _puzzleMoneyButton, _clipPuzzleMoney));
        _changeMoneyButton.onClick.AddListener(() => PlayGame(CHANGE_SCENE, _changeMoneyButton, _clipChangeMoney));
        //_mainMenuButton.onClick.AddListener(() => ReturnInMenu(_mainMenuButton));

        //_mainMenuButton.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        _informationButton.onClick.RemoveAllListeners();
        _puzzleMoneyButton.onClick.RemoveAllListeners();
        _changeMoneyButton.onClick.RemoveAllListeners();
        //_mainMenuButton.onClick.RemoveAllListeners();

        _loaderScene.UnloadScene();
    }

    private void PlayGame(string nameSceneGame, Button button, AudioClip audioClip)
    {
        if (!_isPush)
        {
            _sequence = DOTween.Sequence();
            _isPush = true;
            float audioDuration = audioClip.length;

            _audioSource.clip = audioClip;
            _audioSource.Play();

            _sequence.Append(button.transform.DOScale(0.7f, 0.2f).SetAutoKill(false).From()).AppendInterval(audioDuration)
            .OnComplete(() =>
            {
                _loaderScene.LoadScene(nameSceneGame);
                _isPush = false;
            //_mainMenuButton.gameObject.SetActive(true);
        });
        }
    }

    public void ReturnInMenu(Button button)
    {
        button.transform.DOScale(0.7f, 0.3f).OnComplete(() =>
        {
            button.transform.DOScale(Vector3.one, 0.3f).OnComplete(() => 
            {
                _loaderScene.UnloadScene();
            });
        });
    }
}
