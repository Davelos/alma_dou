using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPuzzle : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private string _nameAnimation;
    [SerializeField] private int _countAnimation;

    public void PlayAnim()
    {
        _animator.enabled = true;
        _animator.Play(_nameAnimation.Trim() + Random.Range(1, _countAnimation), 0, 0);
    }

    public void StopAnim()
    {
        _animator.enabled = false;
    }
}
