using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;


namespace DOU.PartOne.PuzzleMoney
{
    public class CellDropPuzzle : MonoBehaviour, IDropHandler
    {
        [SerializeField] private Image _imageCell;
        [SerializeField] private int _partCell;

        private Vector3 _defaltScaleCell = new Vector3(0.2f, 0.2f, 0.2f);
        public void ResetCell()
        {
            _imageCell.sprite = null;
            _imageCell.transform.localScale = _defaltScaleCell;
            _imageCell.gameObject.SetActive(false);
        }

        private void ActivateCell(Sprite sprite)
        {
            _imageCell.sprite = sprite;
            _imageCell.gameObject.SetActive(true);
            _imageCell.transform.DOScale(1f, 0.4f);
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<PuzzleImage>())
            {
                PuzzleImage puzzleImage = eventData.pointerDrag.GetComponent<PuzzleImage>();

                if (puzzleImage.PartImage == _partCell)
                    TrueImage(puzzleImage);
                else
                    FalseImage(puzzleImage);
            }
        }

        private void TrueImage(PuzzleImage puzzleImage)
        {
            Debug.Log("���������");

            ActivateCell(puzzleImage.ImageBox.sprite);
            PuzzleManager.Instance.CompletedCell();

            puzzleImage.ResetDrag();
            puzzleImage.gameObject.SetActive(false);
        }

        private void FalseImage(PuzzleImage puzzleImage)
        {
            Debug.Log("�� ���������");
        }

    }
}