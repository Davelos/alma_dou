using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DOU.PartOne.PuzzleMoney
{
    public class PuzzleManager : Singleton<PuzzleManager>
    {
        [SerializeField] private Sprite[] _sprites;
        [SerializeField] private Image _referenceImageBox;
        [Space]
        [SerializeField] private CellDropPuzzle[] _cellDrops;
        [SerializeField] private PuzzleImage[] _puzzleImages;
        [Space]
        [SerializeField] private AnimationPuzzle _puzzleAnimator;
        [SerializeField] private ImageSplitter _imageSplitter;
        [SerializeField] private MessageManager _messageGameManager;
        [SerializeField] private int _rows = 2;
        [SerializeField] private int _cols = 3;
        [SerializeField] private int _countLevels = 3;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _clipCompliments;

        private int _currentLevel;
        private int _completedCell;

        private const float COUNT_DOWN = 0.8f;
        private void OnEnable()
        {
            ResetPuzzle();

            _messageGameManager.EndPreview += StartPuzzle;
            _messageGameManager.ReplayGame += StartPuzzle;
        }

        private void OnDisable()
        {
            _messageGameManager.EndPreview -= StartPuzzle;
            _messageGameManager.ReplayGame -= StartPuzzle;
        }

        public void StartPuzzle()
        {
            int randomIndex = Random.Range(0, _sprites.Length - 1);
            _referenceImageBox.sprite = _sprites[_currentLevel];

            _referenceImageBox.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            _referenceImageBox.gameObject.SetActive(true);

            _referenceImageBox.transform.DOScale(1f, 0.9f).OnComplete(() =>
            {
                StartCoroutine(SplitImage());
            });
        }

        private IEnumerator SplitImage()
        {
            yield return new WaitForSeconds(COUNT_DOWN);

            _referenceImageBox.gameObject.SetActive(false);
            _imageSplitter.Splitter(_referenceImageBox.sprite.texture, _rows, _cols, _puzzleImages);
            _puzzleAnimator.PlayAnim();
        }

        private IEnumerator ViewReferenceCompleted()
        {
            yield return new WaitForSeconds(COUNT_DOWN);
            NextLevel();
        }

        private void NextLevel()
        {
            _currentLevel++;

            if (_currentLevel >= _countLevels)
                CompletedGame();
            else
                StartPuzzle();
        }

        public void ResetPuzzle()
        {
            for (int x = 0; x < _cellDrops.Length; x++)
                _cellDrops[x].ResetCell();

            for (int x = 0; x < _puzzleImages.Length; x++)
                _puzzleImages[x].ResetPuzzle();
        }

        public void CompletedCell()
        {
            if (_audioSource.isPlaying) _audioSource.Stop();
            _audioSource.PlayOneShot(_clipCompliments[Random.Range(0, _clipCompliments.Length -1)]);

            _completedCell++;

            if (_completedCell > _rows + _cols)
            {
                _completedCell = 0;
                ResetPuzzle();
                CompletedLevel();
            }
        }

        private void CompletedLevel()
        {
            Color colorReferenceImage = _referenceImageBox.color;
            colorReferenceImage.a = 0f;

            _referenceImageBox.color = colorReferenceImage;
            _referenceImageBox.gameObject.SetActive(true);

            _referenceImageBox.DOFade(1f, 1.7f).OnComplete(() =>
            {
                StartCoroutine(ViewReferenceCompleted());
            });
        }

        private void CompletedGame()
        {
            Debug.Log("������");
            ResetPuzzle();

            _referenceImageBox.DOFade(0f, 1.7f);
            _completedCell = 0;
            _currentLevel = 0;

            _messageGameManager.StartShowBox(false);
        }
    }
}