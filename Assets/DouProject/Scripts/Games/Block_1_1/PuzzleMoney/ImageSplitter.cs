using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartOne.PuzzleMoney
{
    public class ImageSplitter : MonoBehaviour
    {
        public void Splitter(Texture2D texture, int rows, int cols, PuzzleImage[] puzzleImages)
        {
            int pieceWidth = texture.width / cols;
            int pieceHeight = texture.height / rows;

            int pieceIndex = 0;
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Texture2D pieceTexture = new Texture2D(pieceWidth, pieceHeight);
                    Color[] pixels = texture.GetPixels(col * pieceWidth, (rows - row - 1) * pieceHeight, pieceWidth, pieceHeight);
                    pieceTexture.SetPixels(pixels);
                    pieceTexture.Apply();

                    Sprite pieceSprite = Sprite.Create(pieceTexture, new Rect(0, 0, pieceWidth, pieceHeight), new Vector2(0.5f, 0.5f));

                    if (pieceIndex < puzzleImages.Length)
                    {
                        puzzleImages[pieceIndex].ImageBox.sprite = pieceSprite;
                        puzzleImages[pieceIndex].ImageBox.preserveAspect = true;
                        puzzleImages[pieceIndex].gameObject.SetActive(true);
                        pieceIndex++;
                    }
                }
            }
        }
    }
}
