using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DOU.PartOne.PuzzleMoney
{
    [RequireComponent(typeof(CanvasGroup))]
    public class PuzzleImage : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Image _imageBox;
        [SerializeField] private int _partImage;

        private RectTransform _rectTransform;
        private CanvasGroup _canvasGroup;
        private Vector3 _startPosition;
        private int _currentSubIndex;

        public Image ImageBox => _imageBox;
        public int PartImage => _partImage;

        void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();
            _startPosition = _rectTransform.anchoredPosition;

        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            _canvasGroup.alpha = 0.5f;
            _canvasGroup.blocksRaycasts = false;
            _currentSubIndex = _rectTransform.GetSiblingIndex();
            _rectTransform.SetAsLastSibling();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ResetDrag();
        }

        public void ResetPuzzle()
        {
            if (_rectTransform != null)
                _rectTransform.anchoredPosition = _startPosition;

            _imageBox.sprite = null;
            gameObject.SetActive(false);
        }

        public void ResetDrag()
        {
            _canvasGroup.alpha = 1f;
            _canvasGroup.blocksRaycasts = true;
            _rectTransform.SetSiblingIndex(_currentSubIndex);
        }
    }
}
