using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ReturnButton : MonoBehaviour
{
    [SerializeField] private Button _returnInMenu;


    private void OnEnable()
    {
        _returnInMenu.onClick.AddListener(ReturnInMenu);
    }

    private void OnDisable()
    {
        _returnInMenu.onClick.RemoveListener(ReturnInMenu);
    }

    private void ReturnInMenu()
    {
        MenuBlockOne.Instance?.ReturnInMenu(_returnInMenu);
    }
}
