using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace DOU.PartOne.InformationMoney
{
    public enum TypeMoney
    {
        Banknote,
        Coin
    }

    public class ButtonMoney : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private TypeMoney _typeButton;
        [SerializeField] private InspectionMoney _banknote;
        [SerializeField] private InspectionMoney _coin;
        [SerializeField] private Sprite _frontSprite;
        [SerializeField] private Sprite _backSprite;

        private Vector3 _defaultScaleMoney = new Vector3(0.2f, 0.2f, 0.2f);

        private void OnEnable()
        {
            _button.onClick.AddListener(ClickMoney);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(ClickMoney);
        }

        private void ClickMoney()
        {
   
                if (_button.interactable)
                {
                    _button.interactable = false;
                    ActivatorMoney();

                    transform.DOScale(0.7f, 0.3f).SetAutoKill(false).OnComplete(() =>
                    {
                        transform.DOScale(Vector3.one, 0.3f);
                        _button.interactable = true;
                    });
                }
        }

        private void ActivatorMoney()
        {
            InspectionMoney targetMoney;

            if (_typeButton == TypeMoney.Banknote)
            {
                _coin.gameObject.SetActive(false);
                targetMoney = _banknote;

                if (GetComponent<HistoryBanknote>())
                {
                    targetMoney.ResetMoney();
                    targetMoney.SubscribeHideToEvent(GetComponent<HistoryBanknote>().HideHistoryBanknoteEvent);
                    targetMoney.SubscribeShowToEvent(GetComponent<HistoryBanknote>().ShowHistoryBanknoteEvent);
                }
            }
            else
            {
                _banknote.gameObject.SetActive(false);
                targetMoney = _coin;

                if (GetComponent<HistoryCoin>())
                {
                    targetMoney.ResetMoney();
                    targetMoney.SubscribeShowToEvent(GetComponent<HistoryCoin>().PlaySound);
                }
            }

            targetMoney.SetTexture(_frontSprite, _backSprite);
            targetMoney.transform.localScale = _defaultScaleMoney;
            targetMoney.gameObject.SetActive(true);
            targetMoney.transform.DOScale(1f, 0.7f); ;
            targetMoney.ShowHistory.Invoke(true);
        }
    }
}