using UnityEngine;
using Assets.SimpleLocalization.Scripts;

namespace DOU.PartOne.InformationMoney
{
    public class HistoryCoin : MonoBehaviour
    {
        [SerializeField] private InspectionMoney _rotation�oins;
        [SerializeField] private AudioSource _audioSource;
        [Space]
        [Header("Front")]
        [SerializeField] private AudioClip _rusSoundFront;
        [SerializeField] private AudioClip _engSoundFront;
        [SerializeField] private AudioClip _kzhSoundFront;
        [Space]
        [Header("Back")]
        [SerializeField] private AudioClip _rusSoundBack;
        [SerializeField] private AudioClip _engSoundBack;
        [SerializeField] private AudioClip _kzhSoundBack;


        private void Start()
        {
            InspectionMoney.Rotation += _audioSource.Stop;
        }

        private void OnDestroy()
        {
            InspectionMoney.Rotation -= _audioSource.Stop;
        }


        public void PlaySound(bool IsFront)
        {
            if (_audioSource.isPlaying) _audioSource.Stop();
            Debug.Log("PlaySound");
            switch (LocalizationManager.Language)
            {
                case "Russian":
                    _audioSource.PlayOneShot(IsFront ? _rusSoundFront : _rusSoundBack);
                    break;
                case "English":
                    _audioSource.PlayOneShot(IsFront ? _engSoundFront : _engSoundBack);
                    break;
                case "Kazakh":
                    _audioSource.PlayOneShot(IsFront ? _kzhSoundFront : _kzhSoundBack);
                    break;
                default:
                    _audioSource.PlayOneShot(IsFront ? _rusSoundFront : _rusSoundBack);
                    break;
            }
        }
    }
}
