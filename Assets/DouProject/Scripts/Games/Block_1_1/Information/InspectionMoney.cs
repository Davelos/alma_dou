using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

namespace DOU.PartOne.InformationMoney
{
    public class InspectionMoney : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Image _targetImage;
        [SerializeField] private float _duration = 2f;
        [SerializeField] private Vector3 _vectorRotation;

        public Action<bool> ShowHistory;
        public Action HideHistory;
        public Action ResetHistory;

        private Sprite _frontSprite;
        private Sprite _backSprite;

        private bool _isRotation = false;
        private bool _isFront = true;

        private Vector3 _frontScale = new Vector3(1, 1, 1);
        private Vector3 _backScale = new Vector3(-1, 1, 1);

        private const float FLIP_VALUE = 0.70f;

        public static Action Rotation;

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        public void SubscribeShowToEvent(Action<bool> subscriber)
        {
            ShowHistory = subscriber;
        }

        public void SubscribeHideToEvent(Action subscriber)
        {
            HideHistory = subscriber;
        }

        private void UnsubscribeFromEvents()
        {
            HideHistory = null;
            ShowHistory = null;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_isRotation)
                StartCoroutine(RotationCoin());
        }

        private IEnumerator RotationCoin()
        {
            _isRotation = true;
            HideHistory?.Invoke();
            Rotation?.Invoke();

            _targetImage.transform.DORotate(_isFront ? _vectorRotation : Vector3.zero, _duration).OnComplete
                (() =>
                {
                    _isRotation = false;
                    ShowHistory?.Invoke(_isFront);
                });

            while (_isFront ? _targetImage.transform.rotation.y < FLIP_VALUE : _targetImage.transform.rotation.y > FLIP_VALUE)
                yield return null;

            Flip();
        }

        private void Flip()
        {
            _isFront = !_isFront;
            RefeshImage();
        }

        public void SetTexture(Sprite FrontSprite, Sprite BackSprite)
        {
            _frontSprite = FrontSprite;
            _backSprite = BackSprite;

            RefeshImage();
        }

        private void RefeshImage()
        {
            _targetImage.sprite = _isFront ? _frontSprite : _backSprite;
            _targetImage.transform.localScale = _isFront ? _frontScale : _backScale;
        }

        public void ResetMoney()
        {
            _isFront = true;
            _targetImage.transform.rotation = Quaternion.identity;
            _targetImage.sprite = _frontSprite;

            ResetHistory?.Invoke();
        }
    }
}