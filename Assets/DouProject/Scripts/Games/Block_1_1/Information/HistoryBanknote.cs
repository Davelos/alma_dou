using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Assets.SimpleLocalization.Scripts;

namespace DOU.PartOne.InformationMoney
{
    [Serializable]
    public class ObjectHistory
    {
        public CanvasGroup CanvasGroup;
        public ButtonHistorys[] ButtonAsks;
    }

    [Serializable]
    public class ButtonHistorys
    {
        public Button PlayHistoryButton;
        public AudioClip RusHistoryClip;
        public AudioClip EngHistoryClip;
        public AudioClip KzhHistoryClip;
    }

    public class HistoryBanknote : MonoBehaviour
    {
        [SerializeField] private InspectionMoney _rotationBanknote;
        [SerializeField] private AudioSource _audioSource;
        [Space]
        [SerializeField] private ObjectHistory _historyBanknotesFront;
        [SerializeField] private ObjectHistory _historyBanknotesBack;

        private float _fadeDuration = 0.8f;
        private bool _isFront = true;
        private void OnEnable()
        {
            Initialization();
            InspectionMoney.Rotation += _audioSource.Stop;
        }
        private void OnDisable()
        {
            DeInitialization();
            InspectionMoney.Rotation -= _audioSource.Stop;
        }

        private void Initialization()
        {
            _rotationBanknote.ResetHistory += ResetHistiory;

            for (int x = 0; x < _historyBanknotesFront.ButtonAsks.Length; x++)
            {
                int temp = x;
                _historyBanknotesFront.ButtonAsks[x].PlayHistoryButton.onClick.AddListener(() => PlayHistoryClip(temp, _historyBanknotesFront));
            }

            for (int x = 0; x < _historyBanknotesBack.ButtonAsks.Length; x++)
            {
                int temp = x;
                _historyBanknotesBack.ButtonAsks[x].PlayHistoryButton.onClick.AddListener(() => PlayHistoryClip(temp, _historyBanknotesBack));
            }
        }

        private void DeInitialization()
        {
            _rotationBanknote.ResetHistory -= ResetHistiory;

            for (int x = 0; x < _historyBanknotesFront.ButtonAsks.Length; x++)
                _historyBanknotesFront.ButtonAsks[x].PlayHistoryButton.onClick.RemoveAllListeners();

            for (int x = 0; x < _historyBanknotesBack.ButtonAsks.Length; x++)
                _historyBanknotesBack.ButtonAsks[x].PlayHistoryButton.onClick.RemoveAllListeners();
        }

        public void ShowHistoryBanknoteEvent(bool IsFront)
        {
            if (_audioSource.isPlaying) _audioSource.Stop();
            _isFront = IsFront;
            ShowHistory(_isFront ? _historyBanknotesFront : _historyBanknotesBack);
        }

        public void HideHistoryBanknoteEvent()
        {
            HideHistory(_isFront ? _historyBanknotesFront : _historyBanknotesBack);
        }

        private void ShowHistory(ObjectHistory historyBanknote)
        {
            historyBanknote.CanvasGroup.DOFade(1.0f, _fadeDuration);
            historyBanknote.CanvasGroup.interactable = true;
            historyBanknote.CanvasGroup.blocksRaycasts = true;
        }

        private void HideHistory(ObjectHistory historyBanknote)
        {
            historyBanknote.CanvasGroup.DOFade(0.0f, _fadeDuration);
            historyBanknote.CanvasGroup.interactable = false;
            historyBanknote.CanvasGroup.blocksRaycasts = false;
        }

        private void PlayHistoryClip(int indexButton, ObjectHistory historyBanknote)
        {
            if (_audioSource.isPlaying) _audioSource.Stop();
            Debug.Log("PlaySound");
            switch (LocalizationManager.Language)
            {
                case "Russian":
                    _audioSource.PlayOneShot(historyBanknote.ButtonAsks[indexButton].RusHistoryClip);
                    break;
                case "English":
                    _audioSource.PlayOneShot(historyBanknote.ButtonAsks[indexButton].EngHistoryClip);
                    break;
                case "Kazakh":
                    _audioSource.PlayOneShot(historyBanknote.ButtonAsks[indexButton].KzhHistoryClip);
                    break;
            }
        }

        private void ResetHistiory()
        {
            _isFront = true;
            HideHistory(_historyBanknotesFront);
            HideHistory(_historyBanknotesBack);
        }
    }
}
