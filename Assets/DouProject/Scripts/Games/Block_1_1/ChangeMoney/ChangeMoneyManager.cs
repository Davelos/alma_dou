using UnityEngine;
using System;
using UnityEngine.UI;

namespace DOU.PartOne.ChangeMoney
{
    [Serializable]

    public class ChangeMoneyManager : Singleton<ChangeMoneyManager>
    {
        [SerializeField] private MessageManager _messageGameManager;
        [SerializeField] private AskPanel _askPanel;
        [SerializeField] private AnswerPanel _answerPanel;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _goodAnswer;
        [SerializeField] private AudioClip[] _badAnswer;
        [Space]
        [SerializeField] private Button _returnInMenu;

        private int _countLevel;
        private int _currentLevel;
        private int _rightAnswer;

        public int RishtAnswer => _rightAnswer;

        private void OnEnable()
        {
            _messageGameManager.EndPreview += StartGame;
            _messageGameManager.ReplayGame += StartGame;
        }

        private void OnDisable()
        {
            _messageGameManager.EndPreview -= StartGame;
            _messageGameManager.ReplayGame -= StartGame;
        }

        public bool CheckAnswer(int result)
        {
            Debug.Log(result);

            if (result != _rightAnswer)
            {
                if (!_audioSource.isPlaying)
                    _audioSource.PlayOneShot(_badAnswer[UnityEngine.Random.Range(0, _badAnswer.Length - 1)]);

                return false;
            }

            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_goodAnswer[UnityEngine.Random.Range(0, _goodAnswer.Length - 1)]);

            _currentLevel++;

            if (_currentLevel < _countLevel)
                StartGame();
            else
                CompletedGame();

            return true;
        }

        public void StartGame()
        {
            _countLevel = _askPanel.CountAsk;
            _askPanel.GenerationAsk(_currentLevel);
            _rightAnswer = _askPanel.CurrentSum;
            _answerPanel.MoveAnswerPanel(true);
        }

        public void CompletedGame()
        {
            _currentLevel = 0;
            _askPanel.ResetAsk();
            _answerPanel.MoveAnswerPanel(false);
            _messageGameManager.StartShowBox(false);
        }
    }
}