using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

namespace DOU.PartOne.ChangeMoney
{
    public class AnswerPanel : MonoBehaviour
    {
        [SerializeField] private MoneyValue[] _moneyValue;
        [SerializeField] private Button[] _answersButtons;
        [SerializeField] private Transform _answerPanel;
        [SerializeField] private Transform _startTarget;
        [SerializeField] private Transform _moveTarget;
        [SerializeField] private float _duration = 1f;

        private void OnEnable()
        {
            _answerPanel.position = _startTarget.position;
        }

        private void CLickAnswer(Button button, int value)
        {
            if (button.interactable)
            {
                button.interactable = false;
                button.transform.DOScale(0.7f, 0.3f).OnComplete(() =>
                {
                    button.transform.DOScale(Vector3.one, 0.3f).OnComplete(() =>
                    {
                        bool answer = ChangeMoneyManager.Instance.CheckAnswer(value);
                        button.interactable = true;
                    });
                });
            }
        }

        public void MoveAnswerPanel(bool move)
        {
            _answerPanel.DOMove(_startTarget.position, _duration)
            .OnComplete(() => 
            {
                ChangeAnswers();
                if (move) _answerPanel.DOMove(_moveTarget.position, _duration);
            }); 
        }

        public void ChangeAnswers()
        {
            int rightAnswer = ChangeMoneyManager.Instance.RishtAnswer;
            int rightAnswerButtonIndex = Random.Range(0, _answersButtons.Length - 1);
            List<MoneyValue> moneyValues = new List<MoneyValue>();

            ResetAnswerPanel();

            for (int x = 0; x < _moneyValue.Length; x++)
            {
                int temp = x;

                if (_moneyValue[x].Value == rightAnswer)
                {
                    _answersButtons[rightAnswerButtonIndex].image.sprite = _moneyValue[temp].MoneySprite;
                    _answersButtons[rightAnswerButtonIndex].onClick.AddListener(() => CLickAnswer(_answersButtons[rightAnswerButtonIndex], _moneyValue[temp].Value));
                }
                else
                {
                    moneyValues.Add(_moneyValue[temp]);
                }
            }

            for (int x = 0; x < _answersButtons.Length; x++)
            {
                int temp = x;

                if (temp != rightAnswerButtonIndex)
                {
                    _answersButtons[x].image.sprite = moneyValues[x].MoneySprite;
                    _answersButtons[x].onClick.AddListener(() => CLickAnswer(_answersButtons[temp], moneyValues[x].Value));

                }
            }
        }

        private void ResetAnswerPanel()
        {
            foreach (var button in _answersButtons)
            {
                button.onClick.RemoveAllListeners();
                button.image.sprite = null;
            }
        }
    }
}