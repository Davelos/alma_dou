using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace DOU.PartOne.ChangeMoney
{
    public class AskPanel : MonoBehaviour
    {
        [SerializeField] private MoneyValue[] _moneyValue;
        [SerializeField] private Image[] _askImage;
        [SerializeField] private CanvasGroup _canvasGroupForImages;

        private int[] sums = { 1000, 2000, 5000, 10000 };
        private int _currentSum;
        public int CurrentSum => _currentSum;
        public int CountAsk { get { return _moneyValue.Length - 1; } }

        private void Shuffle<T>(T[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                int randomIndex = UnityEngine.Random.Range(i, array.Length);
                T temp = array[randomIndex];
                array[randomIndex] = array[i];
                array[i] = temp;
            }
        }

        public void ResetAsk()
        {
            FadeAsk(false);
        }

        public void GenerationAsk(int level)
        {
            Shuffle(_moneyValue);
            Array.Reverse(_moneyValue);

            _currentSum = sums[level];
            Debug.Log(_currentSum);

            foreach (var ask in _askImage)
                ask.gameObject.SetActive(false);

            StartCoroutine(CheckSum());   
        }

        public void FadeAsk(bool fade)
        {
            float fadeValue = fade ? 1f : 0f;
            _canvasGroupForImages.alpha = fade ? 0f : 1f;
            _canvasGroupForImages.DOFade(fadeValue, 1.5f);
        }

        private IEnumerator CheckSum()
        {
            int sum = 0;
            List<MoneyValue> resultArray = new List<MoneyValue>();

            while (sum != _currentSum)
            {
                bool found = false;
                foreach (var number in _moneyValue)
                {
                    if (number.Value < _currentSum && sum + number.Value <= _currentSum)
                    {
                        resultArray.Add(number);
                        sum += number.Value;
                        found = true;
                        break;
                    }
                }

                yield return null;
                if (!found) break;
            }

            for (int x = 0; x < resultArray.Count; x++)
            {
                _askImage[x].sprite = resultArray[x].MoneySprite;
                _askImage[x].gameObject.SetActive(true);
            }

            Array.Reverse(_moneyValue);
            FadeAsk(true);
        }
    }
}