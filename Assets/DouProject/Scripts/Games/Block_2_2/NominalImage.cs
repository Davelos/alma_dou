using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace DOU.PartTwo.SortMoney
{
    public class NominalImage : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Image _imageBox;
        [Space]
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private CanvasGroup _canvasGroup;

        public Image ImageBox => _imageBox;
        private int _valueNominal; public int ValueNominal => _valueNominal;
        private Quaternion _startRotation;
        private Vector3 _startPosition;
        private int _currentSubIndex;

        private bool _isDrag = false;
        private const float DRAG_SCALE = 2f;
        private const float DURATION = 0.2f;

        private void Start()
        {
            _startPosition = _rectTransform.position;
            _startRotation = _rectTransform.rotation;
        }

        public void SetNominal(Sprite spriteMoiney, int valueMoney)
        {
            _imageBox.sprite = spriteMoiney;
            _valueNominal = valueMoney;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _isDrag = true;
            _canvasGroup.alpha = 0.5f;
            _canvasGroup.blocksRaycasts = false;
            _currentSubIndex = _rectTransform.GetSiblingIndex();
            _rectTransform.SetAsLastSibling();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _isDrag = false;
            ResetDrag();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_isDrag)
                ResetDrag();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _rectTransform.DOScale(DRAG_SCALE, DURATION);
            _rectTransform.DORotateQuaternion(Quaternion.identity, DURATION);
        }

        public void ResetMoney()
        {
            _rectTransform.position = _startPosition;
            _rectTransform.rotation = _startRotation;

            _canvasGroup.alpha = 1f;
            _canvasGroup.blocksRaycasts = true;

            _imageBox.sprite = null;

            _rectTransform.localScale = Vector3.one;
            _rectTransform.gameObject.SetActive(false);
        }

        public void ResetDrag()
        {
            _canvasGroup.alpha = 1f;
            _canvasGroup.blocksRaycasts = true;
            _rectTransform.SetSiblingIndex(_currentSubIndex);

            _rectTransform.DOScale(Vector3.one, DURATION);
            _rectTransform.DOMove(_startPosition, DURATION * 1.5f).OnComplete(() => _rectTransform.DOShakePosition(DURATION, 13, 3));
            _rectTransform.DORotateQuaternion(_startRotation, DURATION);
        }
    }
}