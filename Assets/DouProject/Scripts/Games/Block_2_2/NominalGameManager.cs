using UnityEngine;

namespace DOU.PartTwo.SortMoney
{
    public enum TypeGame
    {
        Coins,
        Banknote
    }

    public class NominalGameManager : Singleton<NominalGameManager>
    {
        [Header("Main settings")]
        [SerializeField] private MessageManager _messageManager;
        [SerializeField] private MoneyValue[] _coins;
        [SerializeField] private MoneyValue[] _banknote;
        [Header("Cells settings")]
        [Space]
        [SerializeField] private CellDropNominal[] _cellDropNominalsBanknote;
        [SerializeField] private CellDropNominal[] _cellDropNominalsCoins;
        [Header("Image Drag settings")]
        [Space]
        [SerializeField] private NominalImage[] _coinsImage;
        [SerializeField] private NominalImage[] _banknotesImage;
        [Header("Other")]
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _clipsComplements;
        [SerializeField] private AudioClip[] _clipsMistake;

        private CellDropNominal[] _currentCellsDrop;
        private MoneyValue[] _currentMoneys;
        private NominalImage[] _currentImages;
        private TypeGame _currentTypeGame;
        private int _completedCells;

        private void OnEnable()
        {
            _messageManager.ReplayGame += ReplayGame;
        }
        private void OnDisable()
        {
            _messageManager.ReplayGame -= ReplayGame;
        }

        public void SetTypeGame(TypeGame typeGame)
        {
            _currentTypeGame = typeGame;

            if (_currentTypeGame == TypeGame.Coins)
            {
                _currentMoneys = _coins;
                _currentImages = _coinsImage;
                _currentCellsDrop = _cellDropNominalsCoins;
            }
            else if (_currentTypeGame == TypeGame.Banknote)
            {
                _currentMoneys = _banknote;
                _currentImages = _banknotesImage;
                _currentCellsDrop = _cellDropNominalsBanknote;
            }
        }

        private void ShuffleMoney(MoneyValue[] moneyValue)
        {
            for (int i = 0; i < moneyValue.Length; i++)
            {
                MoneyValue temp = moneyValue[i];
                int randomIndex = Random.Range(i, moneyValue.Length);
                moneyValue[i] = moneyValue[randomIndex];
                moneyValue[randomIndex] = temp;
            }
        }

        public void StartGame()
        {
            _completedCells = 0;

            ShuffleMoney(_currentMoneys);

            for (int x = 0; x < _currentImages.Length; x++)
            {
                _currentImages[x].SetNominal(_currentMoneys[x].MoneySprite, _currentMoneys[x].Value);
                _currentImages[x].gameObject.SetActive(true);
            }

            foreach (var cell in _currentCellsDrop)
            {
                cell.gameObject.SetActive(true);
            }
        }

        public void CheckCell(bool completed)
        {
            if (completed)
            {
                _audioSource.PlayOneShot(_clipsComplements[UnityEngine.Random.Range(0, _clipsComplements.Length)]);
                _completedCells++;
            }
            else
            {
                _audioSource.PlayOneShot(_clipsMistake[UnityEngine.Random.Range(0, _clipsMistake.Length)]);
            }

            if (_completedCells >= _currentMoneys.Length)
            {
                CompletedGame();
            }
        }

        public void ShowClueCell()
        {
            if (_currentCellsDrop == null) return;

            foreach (var cell in _currentCellsDrop)
                cell.ShowClue();
        }

        private void ReplayGame()
        {
            ResetGame();
            StartGame();
        }

        public void ResetGame()
        {
            if (_audioSource.isPlaying) _audioSource.Stop();

            foreach (var image in _currentImages)
                image.ResetMoney();

            foreach (var cell in _currentCellsDrop)
                cell.ResetCell();

            foreach (var cell in _currentCellsDrop)
                cell.gameObject.SetActive(false);
        }

        private void CompletedGame()
        {
            _messageManager.StartShowBox(false);
        }
    }
}