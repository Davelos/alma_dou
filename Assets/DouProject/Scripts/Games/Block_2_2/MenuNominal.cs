using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


namespace DOU.PartTwo.SortMoney
{
    public class MenuNominal : MonoBehaviour
    {
        [SerializeField] private GameObject _game;
        [Space]
        [SerializeField] private Button _banknoteButton;
        [SerializeField] private Button _coinsButton;
        [SerializeField] private Button _returnButton;
        [SerializeField] private CanvasGroup _canvasGroup;

        private void Start()
        {
            _banknoteButton.onClick.AddListener(ClickBanknote);
            _coinsButton.onClick.AddListener(ClickCoins);
            _returnButton.onClick.AddListener(() => Menu(true));
        }

        private void OnDestroy()
        {
            _banknoteButton.onClick.RemoveAllListeners();
            _coinsButton.onClick.RemoveAllListeners();
            _returnButton.onClick.RemoveAllListeners();
        }

        private void ClickCoins()
        {
            _coinsButton.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _coinsButton.transform.DOScale(1f, 0.2f).OnComplete(() =>
                {
                    NominalGameManager.Instance.SetTypeGame(TypeGame.Coins);
                    Menu(false);
                });

            });
        }

        private void ClickBanknote()
        {
            _banknoteButton.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _banknoteButton.transform.DOScale(1f, 0.2f).OnComplete(() =>
                {
                    NominalGameManager.Instance.SetTypeGame(TypeGame.Banknote);
                    Menu(false);
                });

            });
        }

        private void Menu(bool show)
        {
            _returnButton.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _returnButton.transform.DOScale(1f, 0.2f).OnComplete(() =>
                {
                    if (show)
                        NominalGameManager.Instance?.ResetGame();
                    else
                        NominalGameManager.Instance?.StartGame();


                    _game.SetActive(show ? false : true);
                    _canvasGroup.DOFade(show ? 1f : 0, 1.5f);
                    _canvasGroup.blocksRaycasts = show ? true : false;
                    _canvasGroup.interactable = show ? true : false;
                    _returnButton.gameObject.SetActive(show ? false : true);
                });

            });
        }
    }
}