using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;

namespace DOU.PartTwo.SortMoney
{
    public class CellDropNominal : MonoBehaviour, IDropHandler
    {
        [SerializeField] private Image _imageCell;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private Sprite _defaultSprite;
        [SerializeField] private int _value;
        [SerializeField] private TextMeshProUGUI _textClue;

        private bool _isComletedCell = false;
        private bool _isShhowClue = false;


        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag.GetComponent<NominalImage>())
            {
                NominalImage nominal = eventData.pointerDrag.GetComponent<NominalImage>();
                CheckMoney(nominal);
            }
        }

        private void CheckMoney(NominalImage nominalImage)
        {
            if (_value == nominalImage.ValueNominal)
            {
                _isComletedCell = true;
                _textClue.text = null;

                _canvasGroup.alpha = 0;
                _canvasGroup.blocksRaycasts = false;
                _canvasGroup.interactable = false;

                _imageCell.sprite = nominalImage.ImageBox.sprite;
                nominalImage.gameObject.SetActive(false);

                _canvasGroup.DOFade(1f, 0.4f).OnComplete(() =>
                {
                    NominalGameManager.Instance?.CheckCell(true);
                });
            }
            else
            {
                NominalGameManager.Instance?.CheckCell(false);
            }
        }

        public void ResetCell()
        {
            _imageCell.sprite = _defaultSprite;

            _isComletedCell = false;
            _isShhowClue = false;
            _canvasGroup.blocksRaycasts = true;
            _canvasGroup.interactable = true;
        }

        public void ShowClue()
        {
            if (_isComletedCell) return;

            _isShhowClue = !_isShhowClue;
            _textClue.text = _isShhowClue ? _value.ToString() : null;
        }
    }
}
