using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace DOU.PartOne.CoinBanknote
{
    public class CoinBanknoteManager : Singleton<CoinBanknoteManager>
    {
        [SerializeField] private MessageManager _messageGameManager;
        [SerializeField] private ÑoinBankoneController _ñoinBankoneController;
        [Space]
        [SerializeField] private GameObject[] _stars;
        [SerializeField] private Slider _sliderProcess;
        [SerializeField] private int _countLevels;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _compliments;
        [SerializeField] private AudioClip[] _mistake;


        private int _currentLevel;
        private int _countLives;

        private void OnEnable()
        {
            _messageGameManager.EndPreview += StartGame;
            _messageGameManager.ReplayGame += ReplayGame;

            ResetGame();
        }

        private void OnDisable()
        {
            _messageGameManager.EndPreview -= StartGame;
            _messageGameManager.ReplayGame -= ReplayGame;
        }

        public bool CheckAnswer(Money money, TypeSwipe typeSwipe)
        {
            if (money.Type == TypeMoney.Banknote && typeSwipe == TypeSwipe.Left ||
                money.Type == TypeMoney.Coin && typeSwipe == TypeSwipe.Right)
            {
                if (_audioSource.isPlaying) _audioSource.Stop();

                _audioSource.PlayOneShot(_mistake[Random.Range(0, _mistake.Length - 1)]);

                _stars[_countLives - 1].SetActive(false);
                _countLives--;
                return false;
            }

            _currentLevel++;
            DOTween.To(() => _sliderProcess.value, x => _sliderProcess.value = x, _currentLevel, 1f).SetEase(Ease.Linear);
            _audioSource.PlayOneShot(_compliments[Random.Range(0, _compliments.Length - 1)]);

            return true;
        }

        public void CheckGame()
        {
            if (_currentLevel >= _countLevels || _countLives <= 0)
                CompletedGame();
            else
                StartGame();

        }

        public void StartGame()
        {
            _ñoinBankoneController.ResetMoney();
            _ñoinBankoneController.SetMoney(_currentLevel);
            _ñoinBankoneController.ShowMoney();
        }

        private void ResetGame()
        {
            _currentLevel = 0;
            _countLives = _stars.Length;

            _sliderProcess.maxValue = _countLevels;
            _sliderProcess.value = _currentLevel;

            foreach (var star in _stars)
                star.SetActive(true);
        }

        private void ReplayGame()
        {
            ResetGame();
            StartGame();
        }

        public void CompletedGame()
        {
            _ñoinBankoneController.ResetMoney();
            _messageGameManager.StartShowBox(false);
        }
    }
}