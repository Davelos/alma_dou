using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace DOU.PartOne.CoinBanknote
{
    [Serializable]
    public class Money
    {
        public TypeMoney Type;
        public Sprite SpriteMoney;
    }

    public enum TypeMoney
    {
        Coin,
        Banknote
    }

    public enum TypeSwipe
    {
        Left,
        Right
    }

    public class СoinBankoneController : MonoBehaviour, IPointerDownHandler, IDragHandler
    {
        [SerializeField] private Money[] _moneys;
        [SerializeField] private Image _imageBoxForCoins;
        [SerializeField] private Image _imageBoxForBanknote;
        [Space]
        [SerializeField] private Transform _coiner;
        [SerializeField] private float targetScaleCoin = 0.5f;
        [SerializeField] private Vector3[] _leftTargetPathAnimation;
        [Space]
        [SerializeField] private Transform _walletFront;
        [SerializeField] private float _targetScaleBanknote = 0.5f;
        [SerializeField] private Vector3[] _rightTargetPathAnimation;
        [Space]
        [SerializeField] private float _duration = 1f;
        [SerializeField] private float swipeThreshold = 80f;

        private Money _currentMoney;
        private Transform _currentMoneyTransform;
        private Vector3 _startPositionMoney = new Vector3(0, 270, 0);
        private Vector3 _vectorFall = new Vector3(1500, -100, 0);

        private Vector2 fingerDownPosition;
        private Vector2 fingerUpPosition;
        private bool isDragging = false;

        private Tweener _swipeTween;

        public void ResetMoney()
        {
            if (_currentMoneyTransform != null)
            {
                _currentMoneyTransform.gameObject.SetActive(false);
                _currentMoneyTransform.localPosition = _startPositionMoney;

                _currentMoneyTransform = null;
                _currentMoney = null;
            }
        }
        public void SetMoney(int level)
        {
            Shuffle(_moneys);
            _currentMoney = _moneys[level];

            if (_currentMoney.Type == TypeMoney.Coin)
            {
                _imageBoxForCoins.sprite = _currentMoney.SpriteMoney;
                _currentMoneyTransform = _imageBoxForCoins.transform;
            }
            else
            {
                _imageBoxForBanknote.sprite = _currentMoney.SpriteMoney;
                _currentMoneyTransform = _imageBoxForBanknote.transform;
            }
        }

        public void ShowMoney()
        {
            _currentMoneyTransform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            _currentMoneyTransform.gameObject.SetActive(true);
            _currentMoneyTransform.DOScale(1f, _duration);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            fingerDownPosition = eventData.position;
            isDragging = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (isDragging)
            {
                Vector2 currentFingerPosition = eventData.position;

                if (Vector2.Distance(fingerDownPosition, currentFingerPosition) > swipeThreshold)
                {
                    fingerUpPosition = currentFingerPosition;
                    isDragging = false;
                    CheckSwipe();
                }
            }
        }

        private void CheckSwipe()
        {
           
            if (Vector2.Distance(fingerDownPosition, fingerUpPosition) > swipeThreshold)
            {
               
                    if (fingerUpPosition.x - fingerDownPosition.x > 0)
                    {
                        StartSwipeAnimation(_rightTargetPathAnimation, TypeSwipe.Right, _walletFront);
                    }
                    else
                    {
                        StartSwipeAnimation(_leftTargetPathAnimation, TypeSwipe.Left, _coiner);
                    }
                
            }
        }

        private void StartSwipeAnimation(Vector3[] path, TypeSwipe typeSwipe, Transform target)
        {
            if (_swipeTween != null && _swipeTween.IsActive()) _swipeTween.Kill();

            if (_currentMoney.Type == TypeMoney.Coin)
                _currentMoneyTransform.DOScale(new Vector3(targetScaleCoin, targetScaleCoin, targetScaleCoin), _duration);
            else
                _currentMoneyTransform.DOScale(new Vector3(_targetScaleBanknote, _targetScaleBanknote, _targetScaleBanknote), _duration);

            _swipeTween = _currentMoneyTransform.DOLocalPath(path, _duration, PathType.Linear).OnComplete(() =>
            {

                if (CoinBanknoteManager.Instance.CheckAnswer(_currentMoney, typeSwipe))
                {
                    _swipeTween = _currentMoneyTransform.DOLocalMove(target.localPosition, _duration).OnComplete(() =>
                    {
                        CoinBanknoteManager.Instance.CheckGame();
                    });
                }
                else
                {
                    _swipeTween = _currentMoneyTransform.DOLocalMove(typeSwipe == TypeSwipe.Right ? _vectorFall : -_vectorFall, 1f).OnComplete(()
                        => CoinBanknoteManager.Instance.CheckGame());
                    _currentMoneyTransform.DOScale(Vector3.one, _duration);
                }

                target.DOScale(new Vector3(0.9f, 0.9f, 0.2f), 0.4f).OnComplete(() => target.DOScale(new Vector3(1f, 1f, 1f), 0.4f));

            });
        }

        private void Shuffle<T>(T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int randomIndex = UnityEngine.Random.Range(i, array.Length);
                T temp = array[randomIndex];
                array[randomIndex] = array[i];
                array[i] = temp;
            }
        }
    }
}