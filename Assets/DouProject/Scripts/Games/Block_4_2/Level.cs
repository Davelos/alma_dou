﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using UnityEngine;

namespace DOU.PartFour.Shop
{
    public class Level : MonoBehaviour
    {
        public event Action OnLevelComlete;

        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private MessageManager _messageManager;
        [SerializeField] private CanvasGroup _levelGroup;
        [SerializeField] private float _closeAnimDur;
        [SerializeField] private PartBase[] _parts;

        private Action[] _partsCompletedMethods;
        private int _currentPartId = -1;

        private void Awake()
        {
            AddListenersToParts();
        }

        private void OnDestroy()
        {
            RemoveListenersFromParts();
            _messageManager.EndPreview -= StartLevelOne;

        }

        private void Start()
        {
            _messageManager.EndPreview += StartLevelOne;
        }

        private void StartLevelOne()
        {
           StartGame();
        }

        public async UniTask StartGame()
        {
            _levelGroup.alpha = 1.0f;
            gameObject.SetActive(true);
            await SwitchParts();
        }

        public void Close()
        {
            _levelGroup.DOFade(0f, _closeAnimDur)
                .OnComplete(() => gameObject.SetActive(false));
        }

        public void CollapceAll()
        {
            foreach (var part in _parts)
                part.Hide();
            gameObject.SetActive(false);
        }

        private void AddListenersToParts()
        {
            _partsCompletedMethods = new Action[_parts.Length];
            for (int i = 0; i < _parts.Length; i++)
            {
                Action partCompletedMethod;

                if (i == _parts.Length - 1)
                    partCompletedMethod = LevelFinished;
                else
                    partCompletedMethod = () => SwitchParts().Forget();

                _parts[i].OnPartCompleted += partCompletedMethod;
                _partsCompletedMethods[i] = partCompletedMethod;
            }
        }

        private void RemoveListenersFromParts()
        {
            for (int i = 0; i < _parts.Length; i++)
                _parts[i].OnPartCompleted -= _partsCompletedMethods[i];
            _partsCompletedMethods = null;
        }

        private async UniTask SwitchParts()
        {
            PartBase previosPart = _currentPartId >= 0 ? _parts[_currentPartId] : null;

            PartBase nextPArt = _parts[++_currentPartId];

            await nextPArt.Show();

            if (previosPart != null)
            {
                previosPart.Hide();
            }
        }

        private void LevelFinished()
        {
            OnLevelComlete?.Invoke();
            _messageManager.StartShowBox(false);
        }
    }
}