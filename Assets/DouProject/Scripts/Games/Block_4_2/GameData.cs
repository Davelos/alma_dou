﻿using System;
using System.Collections.Generic;

namespace DOU.PartFour.Shop
{
    public class GameData
    {
        public event Action<int> OnItemsCostChanged;
        public event Action<int> OnMoneyEarnedChanged;

        private List<ProductItem> _productItems = new List<ProductItem>();
        public List<ProductItem> ProductItems { get { return _productItems; } set { _productItems = value; } }
        public int ItemsCost 
        { 
            get => _itemsCost;
            set
            {
                _itemsCost = value;
                OnItemsCostChanged?.Invoke(_itemsCost);
            }
        }

        public int MoneyEarned 
        { 
            get => _moneyEarned;
            set
            {
                _moneyEarned = value;
                OnMoneyEarnedChanged?.Invoke(_moneyEarned);
            }
        }

        private int _itemsCost = 0;
        private int _moneyEarned = 0;
    }
}