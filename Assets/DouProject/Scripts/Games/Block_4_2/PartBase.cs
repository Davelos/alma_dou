﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using UnityEngine;

namespace DOU.PartFour.Shop
{ 

    [RequireComponent(typeof(CanvasGroup))]
    public class PartBase : MonoBehaviour
    {
        public event Action OnPartCompleted;

        [SerializeField] private CanvasGroup _partGroup;
        [SerializeField] private float _showAnimDur = 0.5f;

        private Sequence _showAnim;

        protected virtual void OnValidate()
        {
            if (_partGroup == null)
                _partGroup = GetComponent<CanvasGroup>();
        }

        public virtual async UniTask Show()
        {
            gameObject.SetActive(true);
            _partGroup.alpha = 0f;
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);

            _showAnim?.Kill();
            _showAnim = DOTween.Sequence()
                .Append(_partGroup.DOFade(1f, _showAnimDur))
                .Insert(0, transform.DOScale(1f, _showAnimDur));

            await _showAnim;
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        protected void PartCompleted()
        {
            OnPartCompleted?.Invoke();
            Debug.Log("Part comleted");
        }
    }
}