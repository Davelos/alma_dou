﻿using System;
using UnityEngine;

namespace DOU.PartFour.Shop
{
    [Serializable]
    public class ItemTo<T>
    {
        [field: SerializeField] public DragAndDropProduct DesiredItem { get; set; }
        [field: SerializeField] public T Data { get; set; }
    }
}