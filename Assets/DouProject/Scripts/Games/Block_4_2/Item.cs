using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace DOU.PartFour.Shop
{
    public class Item : MonoBehaviour
    {
        [SerializeField] private Image _iamgexBox;

        private int _idItem; public int IDitem => _idItem;
        
        public void SetDataItem(Sprite sprite, int ID)
        {
            _idItem = ID;
            _iamgexBox.sprite = sprite;
        }
    }
}
