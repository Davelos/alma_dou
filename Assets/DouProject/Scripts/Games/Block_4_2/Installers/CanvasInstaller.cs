﻿using UnityEngine;
using Zenject;

namespace DOU.PartFive.BusinessmanGame
{
    public class CanvasInstaller : MonoInstaller
    {
        [SerializeField] private Canvas _canvas;

        public override void InstallBindings()
        {
            Container
                .BindInstance(_canvas)
                .AsSingle();
        }
    }
}