﻿using ALMA.VoiceoversService;
using DOU.PartFive.StoreGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DOU.PartFive.BusinessmanGame
{
    public class ConfigsInstaller : MonoInstaller
    {
        [SerializeField] private string _localizeVoiceoversConfigPath;
        [SerializeField] private string _gameMoneyConfigPath;

        public override void InstallBindings()
        {
            InstallVoiceoversConfig();
            //InstallGameMoneyConfigs();
        }

        private void InstallVoiceoversConfig()
        {
            var config = Resources.Load<LocalizeVoiceoversPaths>(_localizeVoiceoversConfigPath);
            if (config == null)
                Debug.LogError("LocalizeVoiceoversPaths is not loaded!");

            Container
                .Bind<LocalizeVoiceoversPaths>()
                .FromInstance(config)
                .AsCached();
        }

        private void InstallGameMoneyConfigs()
        {
            var config = Resources.Load<GameMoneyConfig>(_gameMoneyConfigPath);
            if (config == null)
                Debug.LogError("GameMoneyConfig is not loaded!");

            Container
                .Bind<GameMoneyConfig>()
                .FromInstance(config)
                .AsCached();
        }

    }
}