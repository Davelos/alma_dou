﻿using Zenject;

namespace DOU.PartFour.Shop
{
    public class SceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameData>()
                .AsSingle();
        }
    }
}