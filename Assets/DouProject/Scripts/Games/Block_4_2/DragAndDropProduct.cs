using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace DOU.PartFour.Shop
{
    public class DragAndDropProduct : DragAndDrop
    {
        [SerializeField] private Image _imageBox;
        [SerializeField] protected float AddToBasketAnimDur = 0.5f;
        [SerializeField] private float _comebackMoveSpeed = 1500f;
        [SerializeField] private Ease _ease = Ease.OutQuint;

        private int _price;
        private int _idItem;
        public int IDitem { get { return _idItem; } set { _idItem = value; } }
        public int Price { get { return _price; } set { _price = value; } }

        private Tween _comebackTween;
        private Vector2 StartPos;

        private void Awake()
        {
            UpdateStartPos();
        }

        public void UpdateStartPos()
        {
            StartPos = RectTransform.anchoredPosition;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);

            StopMove();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);

            ComebackAnim();
        }

        public void StopMove()
        {
            if (_comebackTween != null)
            {
                _comebackTween.Kill();
                _comebackTween = null;
            }
        }

        public void SetImage(ProductItem product)
        {
            _imageBox.sprite = product.SpriteItem;
            _imageBox.SetNativeSize();
            _price = product.PriceItem;
        }

        public virtual void AddToBasket(RectTransform targetPlace, bool useAnim = true)
        {
            //DiactivateComebackAnim();
            RectTransform.SetParent(targetPlace);

            if (useAnim)
            {
                PlayAddToBasketAnim(() =>
                {
                    ResetRectTransformToTarget(targetPlace);
                    RectTransform.localScale = Vector3.zero;
                });
            }
            else ResetRectTransformToTarget(targetPlace);
        }

        public void AddToBasket(Action<float> onHideComplete = null)
        {
            //DiactivateComebackAnim();

            float halfDur = AddToBasketAnimDur * 0.5f;
            DoHide(halfDur)
                .OnComplete(() => onHideComplete?.Invoke(halfDur));
        }

        public void DiactivateComebackAnim(bool fade)
        {
            if (fade)
                Utils.SetColorAlpha(GetComponent<Image>(), 0);

            StopMove();
            Interactable = false;
        }

        public void Activate()
        {
            if (!Interactable)
            {
                Utils.SetColorAlpha(GetComponent<Image>(), 1);
                ComebackAnim();
                Interactable = true;
            }
        }

        public Sequence DoHide(float dur)
        {
            return DOTween.Sequence()
                .Append(MyGraphic.DOFade(0f, dur))
                .Insert(0, transform.DOScale(0.5f, dur));
        }

        protected virtual Tween ComebackAnim()
        {
            _comebackTween = RectTransform
                .DOAnchorPos(StartPos, _comebackMoveSpeed)
                .SetSpeedBased()
                .SetEase(_ease);

            return _comebackTween;
        }

        private void ResetRectTransformToTarget(RectTransform targetPlace)
        {
            RectTransform.localPosition = Vector3.zero;
        }

        protected void PlayAddToBasketAnim(UnityAction onFadeActionComleted)
        {
            float halfDur = AddToBasketAnimDur * 0.5f;

            var fadeItemAnim = DoHide(halfDur)
                .OnComplete(onFadeActionComleted.Invoke)
                .Pause();
            var addToBasketAnim = DOTween.Sequence()
                .Append(fadeItemAnim)
                .Append(MyGraphic.DOFade(1f, halfDur))
                .Insert(fadeItemAnim.Duration(), transform.DOScale(1f, halfDur));
        }
    }
}