﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartFour.Shop
{
    public class ItemOnSheet : MonoBehaviour
    {
        [SerializeField] private Image _target;
        [SerializeField] private Color _removedColor = Color.white;
        [SerializeField] private Image _cross;
        [SerializeField] private float _removeAnimDuration = 0.5f;

        private Color _originalColor = Color.white;
        private Sequence _removeAnim;
        private int _idItem; public int IDitem => _idItem;
        private AudioClip _clipItem; public AudioClip ClipItem => _clipItem;

        public void Restart()
        {
            _target.color = _originalColor;
            _cross.gameObject.SetActive(false);
        }

        public void RemoveFromList()
        {
            _target.color = _originalColor;
            Utils.SetColorAlpha(_cross, 0f);
            _cross.gameObject.SetActive(true);

            _removeAnim?.Kill();
            _removeAnim = DOTween.Sequence()
                .Append(_target.DOColor(_removedColor, _removeAnimDuration))
                .Insert(0, _cross.DOFade(0.5f, _removeAnimDuration));
        }

        public void SetDataItem(Sprite sprite, int ID, AudioClip audioClip)
        {
            _idItem = ID;
            _target.sprite = sprite;
            _clipItem = audioClip;
        }
    }
}