﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Cysharp.Threading.Tasks;
using System;

namespace DOU.PartFour.Shop
{
    public class ItemsList : MonoBehaviour
    {
        [SerializeField] private ItemBasketStatic _itemBasket;
        [SerializeField] private ItemOnSheet[] _itemToItemOnSheetList;
        [SerializeField] private float _showHideAnimDur = 0.5f;
        [SerializeField] private AudioSource _audioSource;

        private GameData _gameData;
        private RectTransform _rectTransform;

        public void SetItemList(ProductItem[] productItems)
        {
            if (_gameData.ProductItems.Count > 0) _gameData.ProductItems.Clear();

            for (int x = 0; x < _itemToItemOnSheetList.Length; x++)
            {
                _itemToItemOnSheetList[x].SetDataItem
                    (productItems[x].SpriteItem, productItems[x].ID, productItems[x].audioClip);

                _gameData.ProductItems.Add(productItems[x]);
            }
        }

        [Inject]
        public void Construct(GameData gameData)
        {
            _rectTransform = transform as RectTransform;
            _gameData = gameData;

            gameObject.SetActive(false);
        }

        private void Awake()
        {
            _itemBasket.OnItemAdded.AddListener(ItemWasAddedToBasket);
        }

        private void OnDestroy()
        {
            _itemBasket.OnItemAdded.RemoveAllListeners();
        }

        public async UniTask Show()
        {
            gameObject.SetActive(true);
            float halfSizeX = _rectTransform.sizeDelta.x * 0.5f;
            _rectTransform.anchoredPosition = new Vector3(-halfSizeX, _rectTransform.anchoredPosition.y);

            _rectTransform.DOKill();
            await _rectTransform.DOAnchorPosX(halfSizeX, _showHideAnimDur);
        }

        public async UniTask Hide()
        {
            float halfSizeX = _rectTransform.sizeDelta.x * 0.5f;
            _rectTransform.anchoredPosition = new Vector3(halfSizeX, _rectTransform.anchoredPosition.y);

            _rectTransform.DOKill();
            await _rectTransform.DOAnchorPosX(-halfSizeX, _showHideAnimDur)
                .OnComplete(() => gameObject.SetActive(false));
        }

        private void ItemWasAddedToBasket(DragAndDropProduct item)
        {
            foreach (var product in _itemToItemOnSheetList)
            {
                if (product.IDitem == item.IDitem)
                {
                    product.RemoveFromList();
                    _audioSource.PlayOneShot(product.ClipItem);
                    _itemBasket.TryAddToBasket(item);
                    return;
                }
            }

            Debug.LogError($"ID : {item.IDitem} has not found");
        }
    }
}