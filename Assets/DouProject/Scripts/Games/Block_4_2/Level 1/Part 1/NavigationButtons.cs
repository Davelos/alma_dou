using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using DanielLochner.Assets.SimpleScrollSnap;

public class NavigationButtons : MonoBehaviour
{
    [SerializeField] private SimpleScrollSnap _simpleScroll;
    [SerializeField] private Button _prevButton;
    [SerializeField] private Button _nextButton;


    private void Start()
    {
        _prevButton.onClick.AddListener(() => Navigation(_prevButton.transform, false));
        _nextButton.onClick.AddListener(() => Navigation(_nextButton.transform, true));
    }

    private void OnDestroy()
    {
        _prevButton.onClick.RemoveAllListeners();
        _nextButton.onClick.RemoveAllListeners();
    }

    private void Navigation(Transform buttonTransform, bool next)
    {
        Vector3 defaultLocalScale = buttonTransform.transform.localScale;

        buttonTransform.DOScale(buttonTransform.transform.localScale / 1.25f, 0.2f).OnComplete(() =>
        {
            if (next)
                _simpleScroll.GoToNextPanel();
            else
                _simpleScroll.GoToPreviousPanel();


            buttonTransform.DOScale(defaultLocalScale, 0.2f);
        });
    }

    public void RefeshButton()
    {
        _prevButton.gameObject.SetActive(_simpleScroll.CenteredPanel > 0);
        _nextButton.gameObject.SetActive(_simpleScroll.CenteredPanel < _simpleScroll.NumberOfPanels -1);
    }
}
