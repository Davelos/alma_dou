﻿using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFour.Shop
{
    public class Buyer : MonoBehaviour
    {
        public event Action OnBuyBtnPressed;

        [SerializeField] private TextMeshProUGUI _summText;
        [SerializeField] private Button _buyBtn;
        [SerializeField] private float _editTextAnimDur = 0.6f;
        [SerializeField] private float _startTextAnimScale = 1.5f;

        private GameData _gameData;

        [Inject]
        private void Construct(GameData gameData)
        {
            _gameData = gameData;
            OnItemsCostChanged(0);

            _buyBtn.onClick.AddListener(BuyBtnPressed);
        }

        private void OnEnable()
        {
            _gameData.OnItemsCostChanged += OnItemsCostChanged;
        }

        private void OnDisable()
        {
            _gameData.OnItemsCostChanged -= OnItemsCostChanged;
        }

        private void OnDestroy()
        {
            _buyBtn.onClick.RemoveAllListeners();
        }

        public void ShowBuyBtn()
        {
            _buyBtn.gameObject.SetActive(true);
        }

        public void HideBuyBtn()
        {
            _buyBtn.gameObject.SetActive(false);
        }

        private void OnItemsCostChanged(int newCost)
        {
            _summText.transform.localScale = Vector3.one * _startTextAnimScale;
            _summText.text = $"{newCost} ₸";

            _summText.transform.DOKill();
            _summText.transform.DOScale(1f, _editTextAnimDur)
                .SetEase(Ease.InOutBack);
        }

        private void BuyBtnPressed()
        {
            OnBuyBtnPressed?.Invoke();
        }
    }
}