﻿using Assets.SimpleLocalization.Scripts;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DOU.PartFour.Shop
{
    public class ItemBasketStatic : MonoBehaviour, IDropHandler
    {
        public UnityEvent OnBasketFilled => _onBasketFilled;
        public UnityEvent<DragAndDropProduct> OnItemAdded => _onItemAdded;

        [SerializeField] private Image[] _positionPlaces;
        [SerializeField] private Image _targetImage;
        [SerializeField] private bool _editAlphaHitTest = false;

        private UnityEvent _onBasketFilled = new();
        private UnityEvent<DragAndDropProduct> _onItemAdded = new();
        private int itemsAddedCount;

        private void Awake()
        {
            if (_editAlphaHitTest)
                _targetImage.alphaHitTestMinimumThreshold = 0.3f;

            ResetBasket();
        }

        private void OnValidate()
        {
            if (_editAlphaHitTest && _targetImage == null)
                _targetImage = GetComponent<Image>();
        }

        public void OnDrop(PointerEventData eventData)
        {
            DragAndDropProduct product;

            if (!eventData.pointerDrag.GetComponent<DragAndDropProduct>())
                return;
            else
                product = eventData.pointerDrag.GetComponent<DragAndDropProduct>();

            OnItemAdded?.Invoke(product);
        }

        public void TryAddToBasket(DragAndDropProduct item)
        {
            if (itemsAddedCount < _positionPlaces.Length)
            {
                item.DiactivateComebackAnim(true);
                Image productImage = item.GetComponent<Image>();

                Utils.SetColorAlpha(_positionPlaces[itemsAddedCount], 0);
                _positionPlaces[itemsAddedCount].sprite = productImage.sprite;
                _positionPlaces[itemsAddedCount].SetNativeSize();
                _positionPlaces[itemsAddedCount].gameObject.SetActive(true);
                
                ShowItemAnim(_positionPlaces[itemsAddedCount], 0.7f);
                itemsAddedCount++;

                if (itemsAddedCount >= _positionPlaces.Length)
                    _onBasketFilled?.Invoke();
            }
        }

        private void ShowItemAnim(Image image, float dur)
        {
            DOTween.Sequence()
                .Append(image.DOFade(1f, dur))
                .Insert(0, image.transform.DOScale(1f, dur));
        }

        public void ResetBasket()
        {
            itemsAddedCount = 0;

            foreach (var place in _positionPlaces)
                place.gameObject.SetActive(false);
        }
    }
}