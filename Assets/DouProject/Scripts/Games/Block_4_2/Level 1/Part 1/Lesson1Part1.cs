﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Rendering;

namespace DOU.PartFour.Shop
{
    public class Lesson1Part1 : PartBase
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _clip;
        [Space]
        [SerializeField] private ItemBasketStatic _itemBasket;
        [SerializeField] private ItemsList _itemsList;
        [SerializeField] private Buyer _buyer;
        [SerializeField] private CanvasGroup _itemsGroup;
        [SerializeField] private float _gameStartDelay = 5f;
        [SerializeField] private ProductItem[] _productItems;
        private void Awake()
        {
            _itemBasket.OnBasketFilled.AddListener(_buyer.ShowBuyBtn);
            _buyer.OnBuyBtnPressed += PartFinishedForget;
            _buyer.HideBuyBtn();
        }

        private void OnEnable()
        {
            Inisilization();
        }

        private void OnDestroy()
        {
            _buyer.OnBuyBtnPressed -= PartFinishedForget;
        }

        public override async UniTask Show()
        {
            //await base.Show();

            // start voiceover
            StartCoroutine(StartGameRoutine());
        }

        private void Inisilization()
        {
            foreach (var product in _productItems)
            {
                product.OriginalItem.IDitem = product.ID;
                product.OriginalItem.Activate();
                product.TextPrice.text = product.PriceItem.ToString();
            }
        }

        public void StartGame()
        {
            StartCoroutine(StartGameRoutine());
        }

        public void RepplayGame()
        {
            _itemBasket.ResetBasket();
        }

        private IEnumerator StartGameRoutine()
        {
            yield return new WaitForSeconds(_gameStartDelay);
            ShuffleProductItems(_productItems);
            _itemsList.SetItemList(_productItems);
            _itemsList.Show().Forget();
            _itemsGroup.blocksRaycasts = true;
        }

        protected void PartFinishedForget() => PartFinished().Forget();

        protected async UniTaskVoid PartFinished()
        {
            _audioSource.PlayOneShot(_clip);
            _itemsGroup.blocksRaycasts = false;
            //await _itemsList.Hide();
            await UniTask.WaitForSeconds(_clip.length);

            PartCompleted();
        }

        private void ShuffleProductItems(ProductItem[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int randomIndex = UnityEngine.Random.Range(i, array.Length);
                (array[i], array[randomIndex]) = (array[randomIndex], array[i]);
            }
        }
    }
}