﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartFour.Shop
{
    public class Wallet : MonoBehaviour
    {
        [SerializeField] private Image _forvardFace;
        [SerializeField] private GameObject _backFace;
        [SerializeField] private RectTransform _moneyPart;

        [Header("Animate setting")]
        [SerializeField] private float _showHideAnimDuration = 4f;

        private Sequence _openCloseAnim;
        private RectTransform _rectTransform;
        private Vector3 _startPos;
        private Vector3 _moneyPartStartPos;


        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _startPos = _rectTransform.anchoredPosition;
            _moneyPartStartPos = _moneyPart.anchoredPosition;
        }

        public Sequence Open()
        {
            _openCloseAnim?.Kill();

            _moneyPart.anchoredPosition = _moneyPartStartPos;
            float animTime = _showHideAnimDuration * 0.33f;

            _openCloseAnim = DOTween.Sequence()
                .Append(_rectTransform.DOAnchorPosX(_startPos.x - _rectTransform.sizeDelta.x, animTime)
                    .OnComplete(() =>
                    {
                        _backFace.SetActive(true);
                        _forvardFace.enabled = false;
                    }))
                .Append(_rectTransform.DOAnchorPosX(_startPos.x, animTime))
                .Append(_moneyPart.DOAnchorPosY(_moneyPartStartPos.y + _moneyPart.sizeDelta.y, animTime));

            return _openCloseAnim;
        }

        public Sequence Close()
        {
            _openCloseAnim?.Kill();

            _moneyPart.anchoredPosition = new Vector2(_moneyPartStartPos.x, _moneyPartStartPos.y + _moneyPart.sizeDelta.y);
            float animTime = _showHideAnimDuration * 0.33f;

            _openCloseAnim = DOTween.Sequence()
                .Append(_moneyPart.DOAnchorPosY(_moneyPartStartPos.y, animTime))
                .Append(_rectTransform.DOAnchorPosX(_startPos.x - _rectTransform.sizeDelta.x, animTime)
                    .OnComplete(() =>
                    {
                        _backFace.SetActive(false);
                        _forvardFace.enabled = true;
                    }))
                .Append(_rectTransform.DOAnchorPosX(_startPos.x, animTime));

            return _openCloseAnim;
        }
    }
}