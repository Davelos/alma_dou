﻿using ALMA.SoundService;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFour.Shop
{
    public class Scanner : MonoBehaviour, IDropHandler
    {
        public UnityEvent OnBasketFilled => _onBasketFilled;
        public UnityEvent OnItemAdded => _onItemAdded;
        public UnityEvent<int> OnItemScaned => _onItemScaned;

        [SerializeField] private Image _redScanner;
        [SerializeField] private AudioClip _scannerSound;
        [SerializeField] private List<ItemTo<int>> _desiredItems;
        [SerializeField] private Transform _dropPoint;
        [SerializeField] private Transform _packagePoint;
        [SerializeField] private float _moveToScannerDuration = 0.6f;
        [SerializeField] private float _actvationScanerDuration = 0.3f;
        [SerializeField] private float _moveToPackageDuration = 2f;

        private UnityEvent _onBasketFilled = new();
        private UnityEvent _onItemAdded = new();
        private UnityEvent<int> _onItemScaned = new();
        private int itemsAddedCount = 0;
        private SoundGroup _soundFxGroup;

        [Inject]
        private void Construct(SoundGroupsController soundGroupsController)
        {
            _soundFxGroup = soundGroupsController.GetGroup(SoundGroups.SoundFx);
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (!eventData.pointerDrag.TryGetComponent<DragAndDropProduct>(out var dropItem))
                return;

            TryAddToBasket(dropItem).Forget();
        }

        private bool IsDesiredItem(DragAndDropProduct item, out int itemCost)
        {
            itemCost = item.Price;
            return true;
        }

        private async UniTask TryAddToBasket(DragAndDropProduct item)
        {
            if (!IsDesiredItem(item, out int itemCost))
                return;

            item.DiactivateComebackAnim(false);
            await item.transform.DOMove(transform.position, _moveToScannerDuration);
            await ScanerAnim(itemCost);
            await MoveToPackageAnim(item.transform);

            _onItemAdded.Invoke();
            if (++itemsAddedCount == _desiredItems.Count)
                _onBasketFilled.Invoke();
        }

        private async UniTask ScanerAnim(int itemCost)
        {
            float halfAnimDur = _actvationScanerDuration * 0.5f;
            await DOTween.Sequence(_redScanner)
                .Append(_redScanner.DOFade(1f, halfAnimDur)
                    .OnComplete(() =>
                    {
                        _soundFxGroup.Play(_scannerSound);
                        _onItemScaned.Invoke(itemCost);
                    }))
                .Append(_redScanner.DOFade(0f, halfAnimDur));
        }

        private async UniTask MoveToPackageAnim(Transform item)
        {
            float firstAnimDur = _moveToPackageDuration * 0.6f;
            await DOTween.Sequence(item)
                .Append(item.DOMove(_dropPoint.position, firstAnimDur)
                    .SetEase(Ease.Linear)
                    .OnComplete(() => item.SetParent(_dropPoint)))
                .Insert(0, item.DOScale(0.8f, firstAnimDur))
                .Append(item.DOMove(_packagePoint.position, _moveToPackageDuration - firstAnimDur)
                    .SetEase(Ease.InBack));
        }
    }
}