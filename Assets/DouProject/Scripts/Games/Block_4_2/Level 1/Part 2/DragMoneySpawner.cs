﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace DOU.PartFour.Shop
{
    public class DragMoneySpawner : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] private MoneyTaker _moneyTaker;
        [SerializeField] private DragAndDropMoney _spawnItemPrefab;
        [SerializeField] private RectTransform _spawnItemParent;

        private Canvas _canvas;
        private DragAndDropMoney _spawnedItem;

        [Inject]
        public void Construct(Canvas canvas)
        {
            _canvas = canvas;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_spawnedItem != null)
                return;

            BuildItem();
            SetupSpawnedItem(eventData);
            _spawnedItem.OnPointerDown(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.pointerId > 0)
                return;

            _spawnedItem.OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.pointerId > 0)
                return;

            if (!TryAddItemToBasketOnDrop(eventData))
                _spawnedItem.OnPointerUp(eventData);
            _spawnedItem = null;
        }

        private bool TryAddItemToBasketOnDrop(PointerEventData eventData)
        {
            GameObject hitObj = eventData.pointerCurrentRaycast.gameObject;
            bool dropOnBasket = hitObj && hitObj.GetComponentInParent<MoneyTaker>() == _moneyTaker;

            return dropOnBasket && _moneyTaker.TryAddToBasket(_spawnedItem);
        }

        private void BuildItem()
        {
            var spawnedItem = Instantiate(_spawnItemPrefab, transform.position, Quaternion.identity, _spawnItemParent);
            spawnedItem.Construct(_canvas);
            spawnedItem.Parent = _spawnItemParent;
            spawnedItem.OnCombackComlete = () => DestroySpawnedItem(spawnedItem);
            _spawnedItem = spawnedItem;
        }

        private void SetupSpawnedItem(PointerEventData eventData)
        {
            Vector3 worldMousePos = eventData.pointerPressRaycast.worldPosition;
            _spawnedItem.transform.position = worldMousePos;
        }

        private void DestroySpawnedItem(DragAndDropMoney spawnedItem)
        {
            Destroy(spawnedItem.gameObject);
        }
    }
}