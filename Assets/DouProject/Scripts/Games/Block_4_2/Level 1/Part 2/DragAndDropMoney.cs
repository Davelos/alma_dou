﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DOU.PartFour.Shop
{
    public class DragAndDropMoney : DragAndDropItem
    {
        public Action OnCombackComlete;
        [field: SerializeField] public int Cost { get; private set; }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            transform.localScale = Vector3.one * 1.5f;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            transform.localScale = Vector3.one;
        }

        protected override Tween ComebackAnim()
        {
            return base.ComebackAnim()
                .OnComplete(() => OnCombackComlete?.Invoke());
        }
    }
}