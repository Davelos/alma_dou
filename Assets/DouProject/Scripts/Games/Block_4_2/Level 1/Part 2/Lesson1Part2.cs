using ALMA.SoundService;
using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using UnityEngine;

namespace DOU.PartFour.Shop
{
    public class Lesson1Part2 : PartBase
    {
        [SerializeField] private ProductConveer _productConveer;
        [SerializeField] private BoxOffice _boxOffice;
        [SerializeField] private Wallet _wallet;
        [SerializeField] private Scanner _scanner;
        [SerializeField] private MoneyTaker _moneyTaker;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _clips;

        private void Awake()
        {
            _scanner.OnItemAdded.AddListener(OnScanerItemAdded);
            _scanner.OnItemScaned.AddListener(OnScanerItemScaned);
            _scanner.OnBasketFilled.AddListener(FirstPartFinished);

            _boxOffice.OnStartBtnPress += BoxOffice_OnStartBtnPress;

            _moneyTaker.OnMoneyAdded.AddListener(OnMoneyTakerMoneyAdded);
            _moneyTaker.OnBasketFilled.AddListener(() => SecondPartFinished().Forget());
        }

        private void OnDestroy()
        {
            _scanner.OnItemAdded.RemoveAllListeners();
            _scanner.OnItemScaned.RemoveAllListeners();
            _scanner.OnBasketFilled.RemoveAllListeners();
            _boxOffice.OnStartBtnPress -= BoxOffice_OnStartBtnPress;
        }

        private void OnEnable()
        {
            BoxOffice_OnStartBtnPress();
        }

        private void OnScanerItemAdded()
        {
            _productConveer.GiveItem();
        }

        private void OnScanerItemScaned(int itemCost)
        {
            _boxOffice.AddScannedItemCost(itemCost);
        }

        private void OnMoneyTakerMoneyAdded(int moneyCost)
        {
            _boxOffice.AddMoneyCost(moneyCost);
        }

        private void BoxOffice_OnStartBtnPress()
        {
            _audioSource.PlayOneShot(_clips[0]);
            _productConveer.GiveItem();
            _boxOffice.GoToStage2();
        }

        private void FirstPartFinished()
        {
            _audioSource.PlayOneShot(_clips[1]);
            _boxOffice.GoToStage2Part2();
            _moneyTaker.OpenAnim();
            _wallet.Open();
        }

        private async UniTaskVoid SecondPartFinished()
        {
            _audioSource.PlayOneShot(_clips[2]);
            _boxOffice.GoToFinish();
            var moneyTakerTask = _moneyTaker.CloseAnim().AwaitForComplete();
            var _walletTask = _wallet.Close().AwaitForComplete();

            await UniTask.WhenAll(moneyTakerTask, _walletTask);
            PartCompleted();
        }
    }
}