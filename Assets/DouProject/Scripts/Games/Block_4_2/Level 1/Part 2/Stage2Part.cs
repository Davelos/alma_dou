﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;

namespace DOU.PartFour.Shop
{
    public class Stage2Part : MonoBehaviour
    {
        [SerializeField] private Image[] _editigImages;
        [SerializeField] private Material _editMaterial;
        [SerializeField] private Image _progressBarImg;
        [SerializeField] private TextMeshProUGUI _progressBarText;
        [SerializeField] private float _progresBarAnimDur = 0.3f;

        private Material _targetMaterial;
        private GameData _gameData;
        private int _currentCost;

        [Inject]
        private void Construct(GameData gameData)
        {
            _gameData = gameData;
        }

        private void Awake()
        {
            _targetMaterial = Instantiate(_editMaterial);
            foreach (var image in _editigImages)
                image.material = _targetMaterial;
            SetProgressBarVal(0);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void MarkAsComled()
        {
            _targetMaterial.SetFloat("_SetGrayscale", 1);
        }

        public void MarkAsUncomled()
        {
            _targetMaterial.SetFloat("_SetGrayscale", 0);
        }

        public void SetProgressBarVal(int val)
        {
            _currentCost += val;

            _progressBarText.text = $"{_currentCost} ₸";
            _progressBarText.transform.localScale = new Vector3(1.3f, 1.3f, 1f);
            _progressBarText.transform.DOScale(1f, _progresBarAnimDur).SetEase(Ease.OutBack);

            float costPercent = (float)_currentCost / _gameData.ItemsCost;
            _progressBarImg.DOFillAmount(costPercent, _progresBarAnimDur).SetEase(Ease.OutQuart);
        }
    }
}