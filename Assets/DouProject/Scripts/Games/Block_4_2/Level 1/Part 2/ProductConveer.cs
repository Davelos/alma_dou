﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DOU.PartFour.Shop
{
    public class ProductConveer : MonoBehaviour
    {
        [SerializeField] private Transform _startPoint;
        [SerializeField] private Transform _endPoint;
        [SerializeField] private Transform _itemsParent;
        [SerializeField] private DragAndDropProduct[] _items;
        [SerializeField] private float _showItemAnimDur = 2f;

        private DragAndDropProduct[] _spawnItems;
        private int _currentItemId;


        private GameData _gameData;

        [Inject]
        public void Construct(GameData gameData)
        {
            _gameData = gameData;
            InisilizationItem();
        }

        private void InisilizationItem()
        {
            for (int x = 0; x < _gameData.ProductItems.Count; x++)
            {
                _items[x].SetImage(_gameData.ProductItems[x]);
                _gameData.ItemsCost += _items[x].Price;
            }
        }


        private void Awake()
        {
            _spawnItems = new DragAndDropProduct[_items.Length];
            _items.CopyTo(_spawnItems, 0);
            InisilizationItem();
            InitItems();
        }

        public void AddItem()
        {

        }

        public void GiveItem()
        {
            if (_currentItemId == _spawnItems.Length)
                return;

            var curItem = _spawnItems[_currentItemId++];

            curItem.transform.DOMove(_endPoint.position, _showItemAnimDur)
                .OnComplete(() =>
                {
                    curItem.Interactable = true;
                    curItem.UpdateStartPos();
                });
        }

        private void InitItems()
        {
            _currentItemId = 0;
            Utils.ShuffleArray(_spawnItems);
            foreach (var item in _spawnItems)
            {
                item.transform.position = _startPoint.position;
                item.transform.SetParent(_itemsParent);
                item.transform.localScale = Vector3.one;
                item.Interactable = false;
            }
        }
    }
}