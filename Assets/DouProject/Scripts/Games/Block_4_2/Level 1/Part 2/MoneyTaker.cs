﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace DOU.PartFour.Shop
{
    public class MoneyTaker : MonoBehaviour, IDropHandler
    {
        public UnityEvent OnBasketFilled => _onBasketFilled;
        public UnityEvent<int> OnMoneyAdded => _onMoneyAdded;

        [SerializeField] private Image _getterImage;
        [SerializeField] private RectTransform _door;
        [SerializeField] private float _doorOpenCloseAnim = 1f;
        [SerializeField] private float _moveToGetterDuration = 0.25f;
        [SerializeField] private float _hideMoneyDuration = 0.5f;
        [SerializeField] private int _minMoneyCost = 500;

        private UnityEvent _onBasketFilled = new();
        private UnityEvent<int> _onMoneyAdded = new();
        private GameData _gameData;
        private Sequence _openCloseAnim;
        private Vector3 _doorStartPosition;
        private int _moneyReceived;

        [Inject]
        private void Construct(GameData gameData)
        {
            _gameData = gameData;
            _doorStartPosition = _door.anchoredPosition;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (!eventData.pointerDrag.TryGetComponent<DragAndDropMoney>(out var dropItem))
                return;

            TryAddToBasket(dropItem);
        }

        public Sequence OpenAnim()
        {
            _openCloseAnim?.Kill();

            _door.anchoredPosition = _doorStartPosition;
            Utils.SetColorAlpha(_getterImage, 0);

            _openCloseAnim = DOTween.Sequence()
                .Append(_door.DOAnchorPosX(_doorStartPosition.x + _door.sizeDelta.x, _doorOpenCloseAnim))
                .Insert(0, _getterImage.DOFade(1f, _doorOpenCloseAnim));

            return _openCloseAnim;
        }

        public Sequence CloseAnim()
        {
            _openCloseAnim?.Kill();

            _door.anchoredPosition = new Vector3(_doorStartPosition.x + _door.sizeDelta.x, _doorStartPosition.y);
            Utils.SetColorAlpha(_getterImage, 1);

            _openCloseAnim = DOTween.Sequence()
                .Append(_door.DOAnchorPosX(_doorStartPosition.x, _doorOpenCloseAnim))
                .Insert(0, _getterImage.DOFade(0f, _doorOpenCloseAnim));

            return _openCloseAnim;
        }

        private bool IsDesiredItem(DragAndDropMoney item)
        {
            int moneyReceivedNow = _moneyReceived + item.Cost;
            if (moneyReceivedNow > _gameData.ItemsCost)
                return false;

            int needMoney = _gameData.ItemsCost - moneyReceivedNow;
            int Digit3 = needMoney / 100;
            if (Digit3 % 2 != 0 && needMoney < _minMoneyCost)
                return false;

            _moneyReceived = moneyReceivedNow;
            return true;
        }

        public bool TryAddToBasket(DragAndDropMoney item)
        {
            if (!IsDesiredItem(item))
                return false;

            item.Diactivate();
            item.transform.DOMove(transform.position, _moveToGetterDuration)
                .OnComplete(() => item.DoHide(_hideMoneyDuration)
                    .OnComplete(() =>
                    {
                        _onMoneyAdded.Invoke(item.Cost);

                        if (_moneyReceived == _gameData.ItemsCost)
                            _onBasketFilled.Invoke();

                        Destroy(item.gameObject);
                    }));
            return true;
        }
    }
}