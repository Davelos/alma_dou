﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartFour.Shop
{
    public class BoxOffice : MonoBehaviour
    {
        public event Action OnStartBtnPress;

        [Header("Stage 1 setting")]
        [SerializeField] private GameObject _stage1;
        [SerializeField] private Button _startGameBtn;
        [SerializeField] private Stage2Part _stage2Part1;

        [Header("Stage 2 setting")]
        [SerializeField] private GameObject _stage2;
        [SerializeField] private Stage2Part _stage2Part2;

        [Header("Stage 2 setting")]
        [SerializeField] private GameObject _finishStage;

        private void Awake()
        {
            _startGameBtn.onClick.AddListener(StartBtnPressed);
        }

        private void OnDestroy()
        {
            _startGameBtn.onClick.RemoveAllListeners();
        }

        public void AddScannedItemCost(int itemCost)
        {
            _stage2Part1.SetProgressBarVal(itemCost);
        }

        public void AddMoneyCost(int moneyCost)
        {
            _stage2Part2.SetProgressBarVal(moneyCost);
        }

        public void GoToStage2()
        {
            _stage1.SetActive(false);
            _stage2.SetActive(true);
            _stage2Part1.Show();
        }

        public void GoToStage2Part2()
        {
            _stage2Part1.MarkAsComled();
            _stage2Part2.Show();
        }

        public void GoToFinish()
        {
            _stage2.SetActive(false);
            _finishStage.SetActive(true);
        }

        private void StartBtnPressed()
        {
            OnStartBtnPress.Invoke();
        }
    }
}