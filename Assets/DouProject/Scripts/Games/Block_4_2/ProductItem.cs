using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;



namespace DOU.PartFour.Shop
{
    [Serializable]
    public class ProductItem
    {
        public int ID;
        public Sprite SpriteItem;
        public int PriceItem;
        public DragAndDropProduct OriginalItem;
        public AudioClip audioClip;
        public TextMeshProUGUI TextPrice;
    }
}