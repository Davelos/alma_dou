using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DOU.PartTwo.FindPaired
{
    public enum TypeGame
    {
        Coins,
        Banknote,
        All
    }

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private MoneyType[] _coins;
        [SerializeField] private MoneyType[] _bancnotes;
        [SerializeField] private LevelGame[] _levelsGame;
        [SerializeField] private float _timeShowCard = 3f;

        private MoneyType[] _currentMoneys;
        private int _currentLevel;
        private int _pairsGuessed;

        private List<CardController> _activeCards = new List<CardController>();

        public Action<int> NumberLevel;
        public Action<int, int> GuessedPair;
        public Action CompletedLevel;
        public Action CompletedGame;
        public Action<bool> IsShowCard;

        private bool _isShowCard = false;

        void Start()
        {
            foreach (var level in _levelsGame)
            {
                foreach (var card in level.Cards)
                    card.Init(this);
            }
        }

        public void GameSelection(TypeGame typeGame, int numberLevel)
        {
            switch (typeGame)
            {
                case TypeGame.Coins:
                    _currentMoneys = _coins;
                    break;
                case TypeGame.Banknote:
                    _currentMoneys = _bancnotes;
                    break;
                case TypeGame.All:
                    _currentMoneys = new MoneyType[_coins.Length + _bancnotes.Length];
                    _coins.CopyTo(_currentMoneys, 0);
                    _bancnotes.CopyTo(_currentMoneys, _coins.Length);
                    break;
            }

            InitializeLevel(_currentMoneys, _levelsGame?[numberLevel]);
            StartGame();
        }

        private void InitializeLevel(MoneyType[] moneys, LevelGame level)
        {
            if (moneys.Length <= 0 || level == null) return;

            List<MoneyType> _selectedMoneys = new List<MoneyType>();
            moneys = ShuffleMoney(moneys);

            for (int i = 0; i < level.CountPaired; i++)
            {
                MoneyType money = moneys[i];
                _selectedMoneys.Add(money);
                _selectedMoneys.Add(money);
            }

            _selectedMoneys = ShuffleMoney(_selectedMoneys.ToArray()).ToList();

            for (int i = 0; i < _selectedMoneys.Count; i++)
            {
                level.Cards[i].SetCardSettings(_selectedMoneys[i]);
            }

            _currentLevel = Array.IndexOf(_levelsGame, level);
            level.LevelObject.SetActive(true);
        }

        private MoneyType[] ShuffleMoney(MoneyType[] moneys)
        {
            for (int i = 0; i < moneys.Length; i++)
            {
                MoneyType temp = moneys[i];
                int randomIndex = UnityEngine.Random.Range(i, moneys.Length);
                moneys[i] = moneys[randomIndex];
                moneys[randomIndex] = temp;
            }

            return moneys;
        }

        public void CheckPair(CardController card)
        {
            if (card == null) return;

            _activeCards.Add(card);

            if (_activeCards.Count == 2)
            {
                if (_activeCards[0].CompareCard(_activeCards[1]))
                {
                    foreach (var activeCard in _activeCards)
                    {
                        activeCard.PairCard();
                    }

                    _pairsGuessed++;
                    GuessedPair?.Invoke(_pairsGuessed, _levelsGame[_currentLevel].CountPaired);

                    if (_pairsGuessed >= _levelsGame[_currentLevel].CountPaired && _currentLevel < _levelsGame.Length - 1)
                    {
                        CompletedLevel?.Invoke();
                    }
                    else if (_currentLevel >= _levelsGame.Length - 1 && _pairsGuessed >= _levelsGame[_currentLevel].CountPaired)
                    {
                        CompletedGame?.Invoke();
                    }
                }
                else
                {
                    foreach (var activeCard in _activeCards)
                    {
                        activeCard.FlipCard();
                    }
                }

                _activeCards.Clear();
            }
        }

        private IEnumerator ShowCard()
        {
            _isShowCard = true;
            IsShowCard?.Invoke(!_isShowCard);
            yield return new WaitForSeconds(0.4f);

            for (int x = 0; x < 2; x++)
            {
                foreach (var card in _levelsGame[_currentLevel].Cards)
                {
                    card.FlipCard();
                    yield return new WaitForSeconds(0.08f);
                }

                if (x == 0)
                    yield return new WaitForSeconds(_timeShowCard);
            }

            _isShowCard = false;
            IsShowCard?.Invoke(!_isShowCard);
        }

        public void StartGame()
        {
            GuessedPair?.Invoke(_pairsGuessed, _levelsGame[_currentLevel].CountPaired);
            NumberLevel.Invoke(_currentLevel);

            StartCoroutine(ShowCard());
        }

        public void NextLevel()
        {
            _pairsGuessed = 0;
            _currentLevel++;
            PlayerPrefs.SetInt("CountLevel", _currentLevel);

            _levelsGame[_currentLevel].LevelObject.SetActive(false);
            InitializeLevel(_currentMoneys, _levelsGame[_currentLevel]);
            StartGame();
        }

        public void ReplayGame()
        {
            if (!_isShowCard)
            {
                ResetGame();
                InitializeLevel(_currentMoneys, _levelsGame[_currentLevel]);
                StartGame();
            }
        }

        public void ResetGame()
        {
            _pairsGuessed = 0;
            _levelsGame[_currentLevel].LevelObject.SetActive(false);

            foreach (var card in _levelsGame[_currentLevel].Cards)
                card.ResetCard();
        }
    }
}