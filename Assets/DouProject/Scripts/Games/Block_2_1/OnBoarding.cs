using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using DanielLochner.Assets.SimpleScrollSnap;
using Assets.SimpleLocalization.Scripts;

namespace DOU.PartTwo.FindPaired
{
    public class OnBoarding : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _clipsOnBoarding;
        [Space]
        [SerializeField] private SimpleScrollSnap _simpleScrollSnap;
        [SerializeField] private AnimationClip[] _animationClips;
        [SerializeField] private Transform _messageBox;
        [SerializeField] private TextMeshProUGUI _contentTMPo;
        [SerializeField] private string[] _contentMessageBox;

        private Animator _animator;
        private const string NAME_BOOL = "Part";
        private const float DURATION = 0.5f;
        private int _currentIndex;
        private Sequence _sequence;

        public void StartOnBoarding()
        {
            _currentIndex = 0;
            _sequence = DOTween.Sequence();
            _animator = GetComponent<Animator>();

            _animator.SetBool("Start", true);
        }

        public void ShowMessageBox()
        {
            _sequence.Kill();
            _sequence = DOTween.Sequence();

            _contentTMPo.text = LocalizationManager.Localize(_contentMessageBox[_currentIndex]);

            _messageBox.localScale = Vector3.zero;
            _messageBox.gameObject.SetActive(true);

            _sequence.Append(_messageBox.DOScale(Vector3.one, DURATION).SetEase(Ease.OutCirc).OnComplete(() =>
            {
                _audioSource.PlayOneShot(_clipsOnBoarding[_currentIndex]);
            }));
        }

        public void ClickPanel()
        {
            _currentIndex++;
            HideMessageBox();
        }

        private void HideMessageBox()
        {
            if (_audioSource.isPlaying) _audioSource.Stop();
            _sequence.Kill();
            _sequence = DOTween.Sequence();

            _messageBox.localScale = Vector3.one;

            _sequence.Append(_messageBox.DOScale(Vector3.zero, DURATION)).OnComplete(() =>
            {
                _messageBox.gameObject.SetActive(false);

                if (_currentIndex >= _contentMessageBox.Length)
                {
                    _simpleScrollSnap.GoToNextPanel();
                    ResetOnBOarding();
                }
                else
                    _animator.SetBool(NAME_BOOL + _currentIndex.ToString(), true);
                
            });
        }

        private void ResetOnBOarding()
        {
            _animator.SetBool("Start", false);

            for (int x = 1; x <= _currentIndex; x++)
                _animator.SetBool(NAME_BOOL + x.ToString(), false);
        }
    }
}
