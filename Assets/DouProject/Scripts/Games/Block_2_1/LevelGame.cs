using System;
using UnityEngine;

namespace DOU.PartTwo.FindPaired
{
    [Serializable]
    public class LevelGame
    {
        public GameObject LevelObject;
        public int CountPaired;
        public CardController[] Cards;
    }
}
