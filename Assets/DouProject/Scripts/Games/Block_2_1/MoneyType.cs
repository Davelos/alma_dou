using System;
using UnityEngine;

namespace DOU.PartTwo.FindPaired
{
    [Serializable]
    public class MoneyType
    {
        public AudioClip Clip;
        public Sprite Sprite;
    }
}