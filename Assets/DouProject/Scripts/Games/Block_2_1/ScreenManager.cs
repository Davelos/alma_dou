using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using DanielLochner.Assets.SimpleScrollSnap;
using TMPro;

namespace DOU.PartTwo.FindPaired
{
    public class ScreenManager : MonoBehaviour
    {
        [SerializeField] private SimpleScrollSnap _simpleScrollSnap;
        [SerializeField] private GameManager _findPairedManager;
        [SerializeField] private OnBoarding _onBoarding;
        [SerializeField] private MessageManager _messageManager;
        [SerializeField] private AudioSource _audioSource;
        [Space]
        [Header("Menu Screen")]
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _onBoardingButton;
        [SerializeField] private AudioClip _clipPreview;
        [Header("Chouse Level Screen")]
        [SerializeField] private LevelButton[] _levelsButtons;
        [SerializeField] private Button _returnButton;
        [Space]
        [SerializeField] private Button _coinsButton;
        [SerializeField] private AudioClip _coinsClip;
        [SerializeField] private Button _banknoteButton;
        [SerializeField] private AudioClip _banknoteClip;
        [SerializeField] private Button _allButton;
        [SerializeField] private AudioClip _allClip;
        [SerializeField] private Color _defaultColor;
        [SerializeField] private Color _pressedColor;
        [Header("Game Screen")]
        [SerializeField] private Sprite[] _spritesImage;
        [SerializeField] private Image _imageBoxLevel;
        [SerializeField] private TextMeshProUGUI _pairedGuessedText;
        [SerializeField] private Button _exitInMenuButton;
        [SerializeField] private Button _restartButtons;
        [Space]
        [SerializeField] private GameObject _completedLevelBox;
        [SerializeField] private CanvasGroup _completedLevelCanvasGroup;
        [SerializeField] private Button _yesButton;
        [SerializeField] private Button _noButton;

        private const float DURATION = 0.2f;
        private TypeGame _typeGame;


        private void Start()
        {
            Initialization();
        }

        private void OnDestroy()
        {
            ClearData();
        }

        private void Initialization()
        {
            #region Menu Screen

            _playButton.onClick.AddListener(ClickPlay);
            _onBoardingButton.onClick.AddListener(ClickOnBoarding);


            #endregion

            #region Chouse Level Screen

            for (int x = 0; x < _levelsButtons.Length; x++)
            {
                int temp = x;
                _levelsButtons[x].Button.onClick.AddListener(() => ClickLevel(_levelsButtons[temp]));
            }

            _returnButton.onClick.AddListener(() => ClickReturn(_returnButton));

            _coinsButton.onClick.AddListener(() => ClickTypeGame(TypeGame.Coins, _coinsButton, _coinsClip));
            _banknoteButton.onClick.AddListener(() => ClickTypeGame(TypeGame.Banknote, _banknoteButton, _banknoteClip));
            _allButton.onClick.AddListener(() => ClickTypeGame(TypeGame.All, _allButton, _allClip));

            _typeGame = TypeGame.Coins;
            CheckTypeGameButton(_coinsButton);
            #endregion

            #region GameScreen

            _messageManager.ReplayGame += _findPairedManager.ReplayGame;

            _findPairedManager.GuessedPair += SetTextGuessedPair;
            _findPairedManager.CompletedLevel += ShowComletedLevel;
            _findPairedManager.NumberLevel += SetImageLevel;
            _findPairedManager.CompletedGame += ShowCompletedGame;
            _findPairedManager.IsShowCard += InteractableRestartButton;

            _exitInMenuButton.onClick.AddListener(() => ExitInMenu(_exitInMenuButton));
            _noButton.onClick.AddListener(() => ExitInMenu(_noButton));
            _yesButton.onClick.AddListener(NextLevel);
            _restartButtons.onClick.AddListener(RestartGame);

            #endregion
        }

        private void ClearData()
        {
            #region Menu Screen

            _playButton.onClick.RemoveListener(ClickPlay);
            _onBoardingButton.onClick.RemoveListener(ClickOnBoarding);

            #endregion

            #region Chouse Level Screen
            for (int x = 0; x < _levelsButtons.Length; x++)
                _levelsButtons[x].Button.onClick.RemoveAllListeners();

            _returnButton.onClick.RemoveAllListeners();
            _coinsButton.onClick.RemoveAllListeners();
            _banknoteButton.onClick.RemoveAllListeners();
            _allButton.onClick.RemoveAllListeners();

            #endregion

            #region GameScreen

            _messageManager.ReplayGame -= _findPairedManager.ReplayGame;

            _findPairedManager.GuessedPair -= SetTextGuessedPair;
            _findPairedManager.CompletedLevel -= ShowComletedLevel;
            _findPairedManager.NumberLevel -= SetImageLevel;
            _findPairedManager.CompletedGame -= ShowCompletedGame;
            _findPairedManager.IsShowCard -= InteractableRestartButton;

            _exitInMenuButton.onClick.RemoveListener(() => ExitInMenu(_exitInMenuButton));
            _noButton.onClick.RemoveListener(() => ExitInMenu(_noButton));
            _yesButton.onClick.RemoveListener(NextLevel);
            _restartButtons.onClick.RemoveListener(RestartGame);

            #endregion
        }

        #region MenuScreen
        private void ClickPlay()
        {
            _playButton.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                _playButton.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _audioSource.PlayOneShot(_clipPreview);
                    _simpleScrollSnap.GoToNextPanel();
                    CheckAccessButtons();
                });
            });
        }

        private void ClickOnBoarding()
        {
            _onBoardingButton.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                _onBoardingButton.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _simpleScrollSnap.GoToPreviousPanel();
                    _onBoarding.StartOnBoarding();
                });
            });
        }

        #endregion

        #region Chouse Level Screen

        private void CheckAccessButtons()
        {
            int countLevel = PlayerPrefs.GetInt("CountLevel");

            for (int x = 0; x < _levelsButtons.Length; x++)
            {
                if (x <= countLevel)
                {
                    _levelsButtons[x].ImageBox.sprite = _levelsButtons[x].UnLockSprite;
                    _levelsButtons[x].Button.interactable = true;
                }
                else
                {
                    _levelsButtons[x].ImageBox.sprite = _levelsButtons[x].LockSprite;
                    _levelsButtons[x].Button.interactable = false;
                }
            }
        }

        private void ClickLevel(LevelButton levelButton)
        {
            levelButton.Button.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                levelButton.Button.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _findPairedManager.GameSelection(_typeGame, levelButton.NumberLevel);
                    _simpleScrollSnap.GoToNextPanel();

                });
            });
        }

        private void ClickTypeGame(TypeGame typeGame, Button button, AudioClip clip)
        {
            CheckTypeGameButton(button);

            if (!_audioSource.isPlaying) _audioSource.Stop();

            _audioSource.PlayOneShot(clip);

            _typeGame = typeGame;

            button.transform.DOScale(0.7f, DURATION).SetEase(Ease.OutCubic).OnComplete(() =>
           {
               button.transform.DOScale(1f, DURATION).SetEase(Ease.InCubic);
           });
        }

        private void ClickReturn(Button button)
        {
            button.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                button.transform.DOScale(1f, DURATION).OnComplete(() => _simpleScrollSnap.GoToPreviousPanel());
            });
        }

        private void CheckTypeGameButton(Button currentButton = null)
        {
            _coinsButton.GetComponent<Image>().color = _defaultColor;
            _banknoteButton.GetComponent<Image>().color = _defaultColor;
            _allButton.GetComponent<Image>().color = _defaultColor;

             if (currentButton != null)
                currentButton.GetComponent<Image>().color = _pressedColor;
        }

        #endregion

        #region Game Screen

        private void SetTextGuessedPair(int pair, int countPaired)
        {
            _pairedGuessedText.text = $"{pair}/{countPaired}";
        }

        private void SetImageLevel(int level)
        {
            _imageBoxLevel.sprite = _spritesImage[level];
        }

        private void ShowComletedLevel()
        {
            _completedLevelBox.SetActive(true);
            _completedLevelCanvasGroup.DOFade(1f, DURATION);
            _completedLevelCanvasGroup.blocksRaycasts = true;
            _completedLevelCanvasGroup.interactable = true;
        }
        private void ShowCompletedGame()
        {
            _messageManager.StartShowBox(false);
        }

        private void RestartGame()
        {
            _restartButtons.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                _restartButtons.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _completedLevelCanvasGroup.DOFade(0f, DURATION).OnComplete(() =>
                    {
                        _completedLevelBox.SetActive(false);
                        _findPairedManager.ReplayGame();
                    });

                });
            });
        }

        private void NextLevel()
        {
            _yesButton.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                _yesButton.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _completedLevelCanvasGroup.DOFade(0f, DURATION).OnComplete(() =>
                    {
                        _completedLevelBox.SetActive(false);
                        _findPairedManager.NextLevel();
                    });
                });
            });
        }

        private void ExitInMenu(Button button)
        {
            button.transform.DOScale(0.7f, DURATION).OnComplete(() =>
            {
                button.transform.DOScale(1f, DURATION).OnComplete(() =>
                {
                    _completedLevelCanvasGroup.DOFade(0f, DURATION).OnComplete(() =>
                    {
                        _completedLevelBox.SetActive(false);
                        _findPairedManager.ResetGame();
                        CheckAccessButtons();
                        _simpleScrollSnap.GoToPreviousPanel();
                    });
                });
            });
        }

        private void InteractableRestartButton(bool activate)
        {
            _restartButtons.interactable = activate;
        }

        #endregion
    }
}
