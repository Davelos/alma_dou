using UnityEngine;
using UnityEngine.UI;
using System;

namespace DOU.PartTwo.FindPaired
{
    [Serializable]
    public class LevelButton
    {
        public int NumberLevel;
        public Image ImageBox;
        public Sprite UnLockSprite;
        public Sprite LockSprite;
        public Button Button;
    }
}
