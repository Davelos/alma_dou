using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace DOU.PartTwo.FindPaired
{
    [RequireComponent(typeof(CanvasGroup))]
    public class CardController : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Image _cardImage;
        [SerializeField] private AudioSource _audioSource;

        private CanvasGroup _canvasGroup;
        private AudioClip _audioClipMoney;

        private Vector3 _flipVector = new Vector3(0, 180, 0);
        private bool _isFlipped = false;
        private bool _isPaired = false;
        private bool _isRotation; public bool IsRotation => _isRotation;

        private const float DURATION = 0.7f;
        private GameManager _gameManager;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Init(GameManager manager)
        {
            _gameManager = manager;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnCardClick();
        }

        public void SetCardSettings(MoneyType money)
        {
            _cardImage.sprite = money.Sprite;
            _audioClipMoney = money.Clip;
        }

        public void OnCardClick()
        {
            if (!_isFlipped && !_isPaired && !_isRotation)
            {
                if (_audioSource.isPlaying) _audioSource.Stop();

                _audioSource.PlayOneShot(_audioClipMoney);

                FlipCard(true);
            }
        }

        public void FlipCard(bool click = false)
        {
            if (!_isRotation)
            {
                _isRotation = true;
                transform.DOScale(0.7f, 0.4f).OnComplete(() => transform.DOScale(1f, 0.3f));

                transform.DOLocalRotate(!_isFlipped ? _flipVector : Vector3.zero, DURATION).OnComplete(() =>
                {
                    _isFlipped = !_isFlipped;
                    _isRotation = false;

                    if (click)
                        _gameManager.CheckPair(this);
                });
            }
        }

        public void PairCard()
        {
            _isPaired = true;
            _canvasGroup.DOFade(0, DURATION);
        }

        public bool CompareCard(CardController otherCard)
        {
            return _cardImage.sprite == otherCard._cardImage.sprite && this != otherCard && !_isPaired && !otherCard._isPaired;
        }

        public void ResetCard()
        {
            _isFlipped = false;
            _isPaired = false;
            _canvasGroup.alpha = 1;
            transform.rotation = Quaternion.identity;
        }
    }
}
