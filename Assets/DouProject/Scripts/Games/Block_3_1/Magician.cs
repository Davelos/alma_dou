using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DOU.PartThree.GreaterSmaller
{
    public class Magician : MonoBehaviour
    {
        [SerializeField] private Sprite[] _difficitySprites;
        [SerializeField] private Image _leftHat;
        [SerializeField] private Image _rightHat;

        public void SetImageDifficity(int index)
        {
            _leftHat.sprite = _difficitySprites[index];
            _rightHat.sprite = _difficitySprites[index];
        }
    }
}
