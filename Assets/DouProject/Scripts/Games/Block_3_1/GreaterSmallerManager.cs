using UnityEngine;
using System;

namespace DOU.PartThree.GreaterSmaller
{
    public enum TypeGame
    {
        Coins,
        Banknote,
        All
    }

    public enum TypeAnswer
    {
        Left,
        Right
    }

    public class GreaterSmallerManager : MonoBehaviour
    {
        [SerializeField] private int _countAsk;
        [SerializeField] private TopLebelGame _topLebelGame;
        [Space]
        [SerializeField] private MoneyValue[] _coins;
        [SerializeField] private MoneyValue[] _banknote;
        [Space]
        [SerializeField] private Magician _magician;
        [SerializeField] private Hat _leftHat;
        [SerializeField] private Hat _righHat;
        [Space]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _compliments;
        [SerializeField] private AudioClip[] _mistake;

        private MoneyValue[] _currentMoneyValue;
        private int _difflicity;
        private int _currentAsk;

        private bool _checkForGreater;
        private int _leftSum;
        private int _rightSum;

        public Action EndGame;
     
        private void Start()
        {
            _leftHat.Init(this);
            _righHat.Init(this);
        }

        public void InisilizationTypeGame(TypeGame typeGame, int difflicity)
        {
            _difflicity = difflicity;
            _magician.SetImageDifficity(difflicity);

            switch (typeGame)
            {
                case TypeGame.Coins:
                    _currentMoneyValue = _coins;
                    break;
                case TypeGame.Banknote:
                    _currentMoneyValue = _banknote;
                    break;
                case TypeGame.All:
                    _currentMoneyValue = new MoneyValue[_coins.Length + _banknote.Length];
                    _coins.CopyTo(_currentMoneyValue, 0);
                    _banknote.CopyTo(_currentMoneyValue, _coins.Length);
                    break;
            }
        }

        public void StartGame()
        {
            int random;
            _leftSum = 0;
            _rightSum = 0;

            ShuffleMoney(_currentMoneyValue);
            _checkForGreater = GenerateRandomBool();
            _topLebelGame.ShowLebel(_checkForGreater);

            for (int x = 0; x <= _difflicity; x++)
            {
                random = UnityEngine.Random.Range(x, _currentMoneyValue.Length);
                _leftHat.ActivateImage(x, _currentMoneyValue[random].MoneySprite);
                _leftSum += _currentMoneyValue[random].Value;

                random = UnityEngine.Random.Range(x, _currentMoneyValue.Length);

                if ((_rightSum + _currentMoneyValue[random].Value) != _leftSum)
                {
                    _righHat.ActivateImage(x, _currentMoneyValue[random].MoneySprite);
                    _rightSum += _currentMoneyValue[random].Value;
                }
                else
                {
                    _righHat.ActivateImage(x, _currentMoneyValue[random - 1].MoneySprite);
                    _rightSum += _currentMoneyValue[random - 1].Value;
                }
            }
        }

        public bool CheckAnswer(TypeAnswer typeAnswer)
        {
            bool isConditionMet = (_checkForGreater) ?
                (typeAnswer == TypeAnswer.Left ? _leftSum > _rightSum : _rightSum > _leftSum) :
                (typeAnswer == TypeAnswer.Left ? _leftSum < _rightSum : _rightSum < _leftSum);

            if (_audioSource.isPlaying) _audioSource.Stop();

            if (isConditionMet)
            {
                _currentAsk++;
                _audioSource.PlayOneShot(_compliments[UnityEngine.Random.Range(0, _compliments.Length)]);
                CheckLevel(_currentAsk);
            }
            else
            {
                _audioSource.PlayOneShot(_mistake[UnityEngine.Random.Range(0, _mistake.Length)]);
            }

            return isConditionMet;
        }

        public void ResetGame()
        {
            _leftHat.ClearImage();
            _righHat.ClearImage();
            _currentAsk = 0;
        }

        private void CheckLevel(int _currentLevel)
        {
            if (_currentLevel >= _countAsk)
            {
                ResetGame();
                CompletedGame();
            }
            else
            {
                StartGame();
            }
        }

        private void CompletedGame()
        {
            EndGame?.Invoke();
        }

        private void ShuffleMoney(MoneyValue[] moneys)
        {
            for (int i = 0; i < moneys.Length; i++)
            {
                int randomIndex = UnityEngine.Random.Range(i, moneys.Length);
                (moneys[i], moneys[randomIndex]) = (moneys[randomIndex], moneys[i]);
            }
        }

        private bool GenerateRandomBool()
        {
            int randomNum = UnityEngine.Random.Range(0, 2);
            return randomNum == 1;
        }
    }
}