using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Assets.SimpleLocalization.Scripts;


namespace DOU.PartThree.GreaterSmaller
{
    public class TopLebelGame : MonoBehaviour
    {
        [SerializeField] private Transform _lebelBox;
        [SerializeField] private CanvasGroup _canvasGroupText;
        [SerializeField] private TextMeshProUGUI _textTMP;
        [SerializeField] private string _textSmaller;
        [SerializeField] private string _textGreater;
        [SerializeField] private AudioClip _greaterClip;
        [SerializeField] private AudioClip _smallerClip;
        [SerializeField] private AudioSource _audioSource;

        private Vector3 _scaleDefault = new Vector3(0, 1, 1);

        private const float DURATION = 0.5f;

        private void OnEnable()
        {
            _canvasGroupText.alpha = 0f;
            _lebelBox.localScale = _scaleDefault;
        }

        public void ShowLebel(bool _checkForGreater)
        {
            if (_lebelBox.localScale == Vector3.one)
            {
                _canvasGroupText.DOFade(0f, DURATION).OnComplete(() =>
                {
                    _lebelBox.DOScale(_scaleDefault, DURATION).OnComplete(() => 
                    {
                        _textTMP.text = _checkForGreater ? LocalizationManager.Localize(_textGreater) : LocalizationManager.Localize(_textSmaller);
                        _audioSource.PlayOneShot(_checkForGreater ? _greaterClip : _smallerClip);

                        _lebelBox.DOScale(Vector3.one, DURATION).OnComplete(() =>
                        {
                            _canvasGroupText.DOFade(1f, DURATION);
                        });
                    });
                });
            }
            else
            {
                _textTMP.text = _checkForGreater ? LocalizationManager.Localize(_textGreater) : LocalizationManager.Localize(_textSmaller);

                _lebelBox.DOScale(Vector3.one, DURATION).OnComplete(() =>
                {
                    _canvasGroupText.DOFade(1f, DURATION);
                });

            }
        }
    }
}
