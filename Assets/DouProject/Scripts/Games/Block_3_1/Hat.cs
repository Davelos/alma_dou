using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

namespace DOU.PartThree.GreaterSmaller
{
    public class Hat : MonoBehaviour
    {
        [SerializeField] private TypeAnswer _typeAnswer;
        [SerializeField] private Image[] _hatImagesBox;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private Button _button;

        private GreaterSmallerManager _smallerManager;

        private void Start()
        {
            _button.onClick.AddListener(CLickHat);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(CLickHat);
        }

        public void Init(GreaterSmallerManager manager)
        {
            _smallerManager = manager;
        }

        private void CLickHat()
        {
            _canvasGroup.interactable = false;

            _button.transform.DOScale(0.7f, 0.2f).OnComplete(() =>
            {
                _button.transform.DOScale(1f, 0.2f).OnComplete(() =>
                {
                    if (_smallerManager.CheckAnswer(_typeAnswer))
                    {
                        _particleSystem.Stop();
                        _particleSystem.Play();
                        StartCoroutine(StopParticleAfterDelay(_particleSystem.main.duration));
                    }

                    _canvasGroup.interactable = true;
                });
            });
        }

        public void ActivateImage(int index, Sprite sprite)
        {
            _button.transform.localScale = Vector3.zero;
            _hatImagesBox[index].sprite = sprite;
            _hatImagesBox[index].gameObject.SetActive(true);

            _button.transform.DOScale(1f, 0.6f).OnComplete(() =>
            {
                _canvasGroup.interactable = true;
                _canvasGroup.blocksRaycasts = true;
            });
        }

        public void ClearImage()
        {
            foreach (var image in _hatImagesBox)
            {
                image.sprite = null;
                image.gameObject.SetActive(false);
            }

            _canvasGroup.interactable = false;
            _canvasGroup.blocksRaycasts = false;
        }

        IEnumerator StopParticleAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            _particleSystem.Stop();
        }
    }
}
