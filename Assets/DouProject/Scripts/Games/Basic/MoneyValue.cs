using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;



    [Serializable]
    public class MoneyValue
    {
        public Sprite MoneySprite;
        public int Value;
    }

