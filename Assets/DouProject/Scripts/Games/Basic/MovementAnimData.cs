using UnityEngine;
using System;

[Serializable]
public class MovementAnimData
{
    public Vector3 Velue;
    public float Duration;
    public float Pause;
}