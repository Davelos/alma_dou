using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoaderScene : MonoBehaviour
{
    private string loadedScene;



    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneAsync(sceneName));
    }

    public void UnloadScene()
    {
        if (!string.IsNullOrEmpty(loadedScene))
        {
            SceneManager.UnloadSceneAsync(loadedScene);
            loadedScene = null;
        }
    }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        if (!string.IsNullOrEmpty(loadedScene)) UnloadScene(); 
        
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        while (!asyncLoad.isDone)
            yield return null;

        loadedScene = sceneName;
    }
}
