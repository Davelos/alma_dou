using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;
using TMPro;
using Zenject;
using ALMA.VoiceoversService;
using Assets.SimpleLocalization.Scripts;

public enum AnimationType
{
    MovementAnimation,
    ScaleAnimation,
    None
}

public class MessageManager : MonoBehaviour
{
    [Header("Basic")]
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private bool _previewStart;
    [SerializeField] private LocalizeVoiceoversPaths _voiceoversPaths;
    [Space]
    [Header("PreviewBox")]
    [SerializeField] private AnimationType _animationTypePreview;
    [SerializeField] private RectTransform _previewBox;
    [SerializeField] private Vector3 _startPositionPreviewBox;
    [SerializeField] private CanvasGroup _fadeObjectPreview;
    [SerializeField] private AudioClip _clipPreview;
    [Space]
    [SerializeField] private MovementAnimData[] _showPreview;
    [SerializeField] private MovementAnimData[] _hidePreview;
    [Space]
    [Header("Completed Box")]
    [SerializeField] private AnimationType _animationTypeCompleted;
    [SerializeField] private RectTransform _completedBox;
    [SerializeField] private Vector3 _startPositionCompletedBox;
    [SerializeField] private CanvasGroup _fadeObjectCompleted;
    [SerializeField] private TextMeshProUGUI _textContentTMP;
    [SerializeField] private string _nameKeyContext;
    [SerializeField] private string _keyNameGame;
    [SerializeField] private AudioClip _clipWin;
    [SerializeField] private Button _replayGameButton;
    [SerializeField] private Button _exitGameButton;
    [Space]
    [SerializeField] private MovementAnimData[] _showCompleted;
    [SerializeField] private MovementAnimData[] _hideCompleted;

    public Action EndPreview;
    public Action ReplayGame;

    private Sequence _sequence;
    private VoiceoverPlayer _voiceoverPlayer;
    private ISceneLoader _sceneLoader;
    private const float _globalPaused = 0.5f;
    private bool _isClose = false;

    [Inject]
    private void Construct(ISceneLoader sceneLoader, VoiceoverPlayer voiceoverPlayer)
    {
        _sceneLoader = sceneLoader;
        //_voiceoverPlayer = voiceoverPlayer;
        //_voiceoverPlayer.LoadLessonVoiceovers(_voiceoversPaths);
    }

    private bool _isMove;

    private void OnEnable()
    {
        Initialization(true);

        if (_previewStart)
            StartShowBox(true);
    }

    private void OnDisable()
    {
        Initialization(false);
        //_voiceoverPlayer.UnloadLessonVoiceovers();
    }

    private void Initialization(bool initialization)
    {
        if (_animationTypeCompleted == AnimationType.None && _animationTypePreview == AnimationType.None) return;

        if (initialization)
        {
            _replayGameButton?.onClick.AddListener(() => Replay());
            _exitGameButton?.onClick.AddListener(() => _sceneLoader.UnloadSceneAndGoToMenu());

            if (_previewBox != null)
            {
                _previewBox.position = _startPositionPreviewBox;
                FadeObject(false, _fadeObjectPreview);
                _previewBox.gameObject.SetActive(false);
            }

            if (_completedBox != null)
            {
                _completedBox.anchoredPosition = _startPositionCompletedBox;
                FadeObject(false, _fadeObjectCompleted);
                _completedBox.gameObject.SetActive(false);
            }
        }
        else
        {
            _replayGameButton?.onClick.RemoveAllListeners();
            _exitGameButton?.onClick.RemoveAllListeners();
        }
    }

    public void StartShowBox(bool preview)
    {
        if (_animationTypeCompleted == AnimationType.None && _animationTypePreview == AnimationType.None)
        {
            if (_clipPreview != null && preview || _clipWin != null && !preview)
                _audioSource.PlayOneShot(preview ? _clipPreview : _clipWin);

            return;
        }

        if (preview)
            ShowMessageBox(_previewBox, _animationTypePreview, _clipPreview, preview);
        else
            ShowMessageBox(_completedBox, _animationTypeCompleted, _clipWin, preview);
    }

    public void EndShowBox(bool preview)
    {
        if (preview)
        {
            if (_fadeObjectPreview.alpha < 1f) return;

            HideMessageBox(_previewBox, _animationTypePreview, preview);
        }
        else
        {
            _isClose = true;
            HideMessageBox(_completedBox, _animationTypeCompleted, preview);
        }
    }

    private void Replay()
    {
        _replayGameButton.transform.DOScale(0.7f, 0.3f).SetAutoKill(false).OnComplete(() =>
        {
            HideMessageBox(_completedBox, _animationTypeCompleted, false);
        }).From();
    }

    private void FadeObject(bool fade, CanvasGroup canvasGroup)
    {
        float fadeValue = fade ? 1f : 0f;
        canvasGroup.DOFade(fadeValue, 0.8f);
    }

    private void ShowMessageBox(Transform messageBox, AnimationType animationType, AudioClip audioClip, bool preview)
    {
        if (messageBox == null) return;
        messageBox.gameObject.SetActive(true);

        _sequence = DOTween.Sequence();
        _sequence.AppendInterval(_globalPaused);

        MovementAnimData[] movementAnimDatas = preview ? _showPreview : _showCompleted;
        CanvasGroup currentFadeObject = preview ? _fadeObjectPreview : _fadeObjectCompleted;
        float audioDuration = audioClip != null ? audioClip.length : 0f;

        if (!preview)
        {
            string nameGame = LocalizationManager.Localize(_keyNameGame);
            _textContentTMP.text = LocalizationManager.Localize(_nameKeyContext, nameGame);
        }

        if (animationType == AnimationType.MovementAnimation)
        {
            if (movementAnimDatas.Length > 0)
            {
                foreach (var movementData in movementAnimDatas)
                {
                    if (movementData.Velue != null)
                    {
                        _sequence.Append(messageBox.DOMove(movementData.Velue, movementData.Duration).SetEase(Ease.Linear));
                    }
                }
            }
        }
        else if (animationType == AnimationType.ScaleAnimation)
        {
            messageBox.localScale = new Vector3(1, 0, 1);

            if (movementAnimDatas.Length > 0)
            {
                foreach (var movementData in movementAnimDatas)
                {
                    _sequence.Append(messageBox.DOScale(movementData.Velue, movementData.Duration).SetEase(Ease.OutCirc));
                }
            }
        }

        _sequence.AppendCallback(() =>
        {
            FadeObject(true, currentFadeObject);
            if (audioClip != null) _audioSource.PlayOneShot(preview ? _clipPreview : _clipWin);
        })
         .AppendInterval(audioDuration).OnComplete(() =>
         {
             if (preview && animationType != AnimationType.None && !_isClose)
                 EndShowBox(preview);
         });

    }

    private void HideMessageBox(Transform messageBox, AnimationType animationType, bool preview)
    {
        if (messageBox == null || _isMove) return;

        if (_audioSource.isPlaying) _audioSource.Stop();

        _isMove = true;
        _sequence.Kill();
        _sequence = DOTween.Sequence();
        _sequence.AppendInterval(_globalPaused);

        MovementAnimData[] movementAnimDatas = preview ? _hidePreview : _hideCompleted;
        CanvasGroup currentFadeObject = preview ? _fadeObjectPreview : _fadeObjectCompleted;

        if (animationType == AnimationType.MovementAnimation)
        {
            if (movementAnimDatas.Length > 0)
            {
                foreach (var movementData in movementAnimDatas)
                {
                    if (movementData.Velue != null)
                        _sequence.Append(messageBox.DOMove(movementData.Velue, movementData.Duration).SetEase(Ease.Linear));
                }
            }
        }
        else if (animationType == AnimationType.ScaleAnimation)
        {
            FadeObject(false, currentFadeObject);

            if (movementAnimDatas.Length > 0)
            {
                foreach (var movementData in movementAnimDatas)
                    _sequence.Append(messageBox.DOScale(movementData.Velue, movementData.Duration).SetEase(Ease.OutCirc));
            }
        }

        _sequence.AppendCallback(() =>
        {
            if (preview)
                EndPreview?.Invoke();
            else
                ReplayGame?.Invoke();

            messageBox.gameObject.SetActive(false);
            _isMove = false;
        });
    }
}