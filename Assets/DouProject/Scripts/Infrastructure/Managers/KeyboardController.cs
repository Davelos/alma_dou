using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : IDisposable
{
    private bool OskIsClosed => _oskProcess == null || _oskProcess.HasExited;

    private System.Diagnostics.Process _oskProcess;

    public void Dispose()
    {
        HideKeyboard();
    }

    public void ToggleKeyboard()
    {
        if (OskIsClosed) ShowKeyboard();
        else HideKeyboard();
    }

    public void ShowKeyboard() { if (OskIsClosed) _oskProcess = System.Diagnostics.Process.Start("osk.exe"); }

    public void HideKeyboard() { if (!OskIsClosed) _oskProcess.CloseMainWindow(); }

    public bool HasPhysicalKeyboard()
    {
        // ���������, �������� �� ���������� ���������� �����������
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (Input.touchSupported)
                return false; // ������������ ��������� ����, ������ �����, ��� ���������� ����������
            else
                return true; // �� ���������� ���������� � ��� ���������� �����, ����������������, ���� ���������� ����������
        }
        else
            return false; // �� ���������� ���������, ����������������, ��� ���������� ����������
    }
}
