using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using DG.Tweening;

public class UISceneInstaller : MonoInstaller, IInitializable
{
    [SerializeField] private Canvas _canvas;
    [SerializeField] private Transform _drawTextParent;
    [SerializeField] private Screenshoter _screenshoter;
    [SerializeField] private ScenesController _scenesController;
    [Space]
    [Header("Prefabs")]
    [SerializeField] private DrawTextController _drawTextPrefab;

    public override void InstallBindings()
    {
        InstallUIDependens();
        InstallFactories();
        InstallSingltons();

        InstallSceneInstaller();
    }

    public void Initialize()
    {
        DOTween.Init();
    }

    private void InstallUIDependens()
    {
        Container
            .Bind<Canvas>()
            .FromInstance(_canvas);
        Container
            .BindInterfacesAndSelfTo<KeyboardController>()
            .AsSingle();
        Container
            .BindInterfacesAndSelfTo<Screenshoter>()
            .FromInstance(_screenshoter);
    }

    private void InstallFactories()
    {
        Container
            .Bind<DrawTextFactory>()
            .AsSingle()
            .WithArguments(Container, _drawTextParent, _drawTextPrefab);
    }

    private void InstallSingltons()
    {
        Container
            .Bind<ScenesController>()
            .FromInstance(_scenesController);
    }

    private void InstallSceneInstaller()
    {
        Container
            .BindInterfacesAndSelfTo<UISceneInstaller>()
            .FromInstance(this)
            .AsSingle();
    }
}
