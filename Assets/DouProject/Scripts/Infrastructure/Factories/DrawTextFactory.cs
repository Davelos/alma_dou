﻿using UnityEngine;
using Zenject;

public class DrawTextFactory
{
    private readonly DrawTextController _prefab;
    private readonly Transform _parent;
    private readonly DiContainer _container;

    public DrawTextFactory(DiContainer container, DrawTextController prefab, Transform parent)
    {
        _prefab = prefab;
        _parent = parent;
        _container = container;
    }

    public DrawTextController Build()
    {
        return _container.InstantiatePrefabForComponent<DrawTextController>(_prefab, _parent);
    }
}