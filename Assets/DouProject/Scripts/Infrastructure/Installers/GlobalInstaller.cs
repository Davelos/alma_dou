using System;
using UnityEngine;
using Zenject;
using ALMA.VoiceoversService;
using ALMA.SoundService;

public class GlobalInstaller : MonoInstaller
{
    [SerializeField] private string _musicsConfigPath;
    [SerializeField] private string _commonVoiceoversConfigPath;

    public override void InstallBindings()
    {
        InstallMainCamera();
        InstallMusicPlayer();
        InstallVoiceoverPlayer();
        InstallLessonLoader();
    }

    private void InstallVoiceoverPlayer()
    {
        var localizeCommonVoiceoversPaths = Resources.Load<LocalizeCommonVoiceoversPaths>(_commonVoiceoversConfigPath);

        Container.BindInterfacesAndSelfTo<VoiceoverPlayer>()
            .AsSingle()
            .WithArguments(localizeCommonVoiceoversPaths)
            .NonLazy();
    }

    private void InstallLessonLoader()
    {
        Container
            .Bind<ISceneLoader>()
            .To<LessonsLoader>()
            .AsSingle();
    }

    private void InstallMusicPlayer()
    {
        var musicsConf = Resources.Load<GameMusics>(_musicsConfigPath);

        Container
            .BindInterfacesAndSelfTo<MusicPlayer>()
            .AsSingle()
            .WithArguments(musicsConf)
            .NonLazy();
    }

    private void InstallMainCamera()
    {
        Container
            .Bind<Camera>()
            .FromInstance(Camera.main)
            .AsSingle();
    }
}
