﻿namespace ALMA.SoundService
{
    public enum SoundGroups
    {
        Master,
        Voiceover,
        Video,
        Background,
        SoundFx,
        Music
    }
}
