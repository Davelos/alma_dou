﻿using UnityEngine;
using UnityEngine.Audio;

namespace ALMA.SoundService
{
    [CreateAssetMenu(fileName = "NewSoundSettings", menuName = "Sound/SettingsConfig")]
    public class SoundSettings : ScriptableObject
    {
        [field: SerializeField] public AudioMixer Mixer { get; private set; }
        [field: SerializeField] public SoundGroup[] SoundGroups { get; private set; }
    }
}