﻿using System;
using UnityEngine;
using Zenject;

namespace ALMA.SoundService
{
    public class SoundServiceInstaller : MonoInstaller, IInitializable
    {
        [Tooltip("Path from Resources directory")]
        [SerializeField] private string _soundSettingsConfigPath;

        public override void InstallBindings()
        {
            InstallSoundController();

            Container
                .BindInterfacesTo<SoundServiceInstaller>()
                .FromInstance(this);
        }

        public void Initialize()
        {
            var soundGroupsController = Container.Resolve<SoundGroupsController>();
            soundGroupsController.LoadPrefsSettings();
        }

        private void InstallSoundController()
        {
            var soundSettingsConf = Resources.Load<SoundSettings>(_soundSettingsConfigPath) 
                ?? throw new ArgumentException("SoundSettings file not found...");

            GameObject audioSourceContainer = new("GlobalAudioSources");
            Container
                .Bind<SoundGroupsController>()
                .AsSingle()
                .WithArguments(soundSettingsConf, audioSourceContainer)
                .NonLazy();
        }
    }
}