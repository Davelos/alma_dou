using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using Zenject;

namespace ALMA.SoundService
{
    public class SoundGroupsController
    {
        private readonly AudioMixer _mixer;
        private readonly GameObject _audioSourceContainer;
        private readonly SoundGroup[] _soundGroups;

        public SoundGroupsController(SoundSettings soundSettings, GameObject audioSourceContainer)
        {
            _audioSourceContainer = audioSourceContainer;
            _mixer = soundSettings.Mixer;

            _soundGroups = new SoundGroup[soundSettings.SoundGroups.Length];
            soundSettings.SoundGroups.CopyTo(_soundGroups, 0);

            foreach (SoundGroup soundGroup in _soundGroups)
            {
                soundGroup.Init(CreateAudioSource(soundGroup.GroupType), _mixer);
            }
        }

        public SoundGroup GetGroup(SoundGroups groupType) => _soundGroups.FirstOrDefault(groupe => groupe.GroupType == groupType);

        public void SavePrefsSettings()
        {
            foreach (var soundGroup in _soundGroups)
                soundGroup.SaveGroupInPrefs();
        }

        public void LoadPrefsSettings()
        {
            foreach (var soundGroup in _soundGroups)
                soundGroup.LoadGroupFromPrefs();
        }

        public AudioSource CreateAudioSource(SoundGroups group, GameObject sourceContainer = null)
        {
            if (sourceContainer == null)
                sourceContainer = _audioSourceContainer;

            AudioSource audioSource = sourceContainer.AddComponent<AudioSource>();
            audioSource.outputAudioMixerGroup = _mixer.FindMatchingGroups(group.ToString()).First();
            audioSource.playOnAwake = false;
            return audioSource;
        }
    }
}