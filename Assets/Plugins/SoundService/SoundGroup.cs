﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace ALMA.SoundService
{
    [Serializable]
    public class SoundGroup
    {
        public SoundGroups GroupType => _groupType;
        public AudioSource Source => _globalSource;

        public float Volume
        {
            get => DecToVal01();
            set => Val01ToDec(value);
        }

        public bool SoundOn => Volume > 0.00011f;

        [SerializeField] private SoundGroups _groupType;
        [SerializeField, Range(0f, 1f)] private float _defaultVolume;
        [SerializeField] private bool _isLoopingPlay;

        private AudioMixer _mixer;
        private AudioSource _globalSource;
        private string _groupVolumeName;
        private string _groupPrefsName;
        private float _lastVolume = 0f;
        private const float _decibelMultiply = 20f;

        public void Init(AudioSource globalSource, AudioMixer mixer)
        {
            _mixer = mixer;

            _groupVolumeName = $"{_groupType}Vol";
            _groupPrefsName = $"SoundSettings_{_groupType}";

            _globalSource = globalSource;
            _globalSource.loop = _isLoopingPlay;
        }

        public void Play(AudioClip clip, AudioSource targetSource = null)
        {
            if (clip == null)
                return;
            if (targetSource == null)
                targetSource = _globalSource;

            if (!_isLoopingPlay)
                targetSource.PlayOneShot(clip);
            else
            {
                targetSource.clip = clip;
                targetSource.Play();
            }
        }

        public void SaveGroupInPrefs() => PlayerPrefs.SetFloat(_groupPrefsName, Volume);

        public void LoadGroupFromPrefs() => Volume = PlayerPrefs.GetFloat(_groupPrefsName, _defaultVolume);

        public void TurnOn()
        {
            Volume = _lastVolume;
        }

        public void TurnOff()
        {
            _lastVolume = Volume;
            Volume = 0f;
        }

        private float DecToVal01()
        {
            _mixer.GetFloat(_groupVolumeName, out float mixerVol);
            return Mathf.Pow(10f, (mixerVol / _decibelMultiply));
        }

        private void Val01ToDec(float value)
        {
            float clampVal = Mathf.Clamp(value, 0.0001f, 1f);
            float mixerVol = _decibelMultiply * Mathf.Log10(clampVal);
            _mixer.SetFloat(_groupVolumeName, mixerVol);
        }
    }
}