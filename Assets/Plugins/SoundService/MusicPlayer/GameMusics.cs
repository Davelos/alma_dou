﻿using UnityEngine;

namespace ALMA.SoundService
{
    [CreateAssetMenu(fileName = "NewGameSound", menuName = "Settings/Game/GameMusics", order = 2)]
    public class GameMusics : ScriptableObject
    {
        [field: SerializeField] public bool PlayOnAwake { get; private set; }
        [field: SerializeField] public bool Shuffing { get; private set; }
        [field: SerializeField] public AudioClip[] MusicList { get; private set; }
    }
}