using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

using Random = UnityEngine.Random;

namespace ALMA.SoundService
{
    public class MusicPlayer : IInitializable, ITickable, IDisposable
    {
        public bool IsPlaying => _musicGroup.Source.isPlaying;
        public bool IsPausing => _isPausing;

        private SoundGroup _musicGroup;
        private AudioClip[] _musicList;
        private int _currentTrackIndex = -1;
        private bool _isActivated;
        private bool _isPausing;
        private bool _playOnAwake;
        private bool _useShuffing;

        public MusicPlayer(SoundGroupsController soundGroups, GameMusics gameMusics)
        {
            _musicGroup = soundGroups.GetGroup(SoundGroups.Music);
            if (gameMusics == null || gameMusics.MusicList.Length == 0)
            {
                Debug.LogWarning("GameMusics was not loaded");
                return;
            }

            _musicList = new AudioClip[gameMusics.MusicList.Length];
            gameMusics.MusicList.CopyTo(_musicList, 0);

            _playOnAwake = gameMusics.PlayOnAwake;
            _useShuffing = gameMusics.Shuffing;
        }

        public void Initialize()
        {
            if (_musicList == null || _musicList.Length == 0)
            {
                Debug.LogWarning("_musicList is not initialized...");
                return;
            }

            if (_useShuffing)
                ShuffleMusics();
            if (_playOnAwake)
                Play();
        }

        public void Tick()
        {
            if (_isPausing || !_isActivated)
                return;

            if (!_musicGroup.Source.isPlaying)
                PlayNextTrack();
        }

        public void Dispose()
        {
            if (_musicGroup.Source != null)
                _musicGroup.Source.Stop();
        }

        public void Pause()
        {
            if (!_musicGroup.Source.isPlaying)
                return;

            _musicGroup.Source.Pause();
            _isPausing = true;
        }

        public void UnPause()
        {
            if (_musicGroup.Source.isPlaying || _musicList.Length == 0)
                return;

            _musicGroup.Source.UnPause();
            _isPausing = false;
        }

        public void Play()
        {
            if (_musicGroup.Source.isPlaying || _musicList.Length == 0)
                return;

            if (_currentTrackIndex < 0)
                PlayNextTrack();
            else
            {
                if (IsPausing)
                    _musicGroup.Source.Stop();
                _musicGroup.Source.Play();
            }

            _isPausing = false;
            _isActivated = true;
        }

        public void Stop()
        {
            if (!_musicGroup.Source.isPlaying)
                return;

            _musicGroup.Source.Stop();
            _isActivated = false;
        }

        private void PlayNextTrack()
        {
            if (_musicList.Length == 0)
                return;

            _currentTrackIndex = (_currentTrackIndex + 1) % _musicList.Length;
            _musicGroup.Play(_musicList[_currentTrackIndex]);
        }

        private void ShuffleMusics()
        {
            for (int i = _musicList.Length - 1; i > 0; i--)
            {
                int j = Random.Range(0, i + 1);
                (_musicList[i], _musicList[j]) = (_musicList[j], _musicList[i]);
            }
        }
    }
}