﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ALMA.Language
{
    public class LanguageGroup : MonoBehaviour
    {
        public class LanguageChangeEvent : UnityEvent<MultiLanguage.Languages> { }
        public LanguageChangeEvent OnLanguageBtnChanged => _onLanguageChanged;

        [SerializeField] private List<LanguageToggle> _languageToggles;

        private LanguageChangeEvent _onLanguageChanged = new();
        private LanguageToggle _curTarget = null;

        private void Awake()
        {
            for (int i = 0; i < _languageToggles.Count; i++)
            {
                int id = i;
                LanguageToggle toggle = _languageToggles[id];
                toggle.onValueChanged.AddListener((isActive) => OnValueChanged(isActive, toggle));
            }
        }

        private void OnDestroy()
        {
            foreach (var toggle in _languageToggles)
                toggle.onValueChanged.RemoveAllListeners();
        }

        public void ActivateTgl(MultiLanguage.Languages language)
        {
            var newActiveTgl = _languageToggles.FirstOrDefault(tgl => tgl.Language == language);
            newActiveTgl.SetIsOnWithoutNotify(true);
        }

        private void OnValueChanged(bool isActive, LanguageToggle target)
        {
            if (isActive)
            {
                if (_curTarget == target)
                    return;

                _onLanguageChanged?.Invoke(target.Language);
                _curTarget = target;
            }
            target.SetSelectedSprite(isActive);
        }
    }
}