using Assets.SimpleLocalization.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace ALMA.Language
{
    public class MultiLanguage : MonoBehaviour
    {
        public enum Languages
        {
            Russian,
            Kazakh,
            English
        }

        public Languages CurrentLanguage => _currecntLanguage;

        [SerializeField] private Languages _defaultLanguage;
        [SerializeField] private LanguageGroup _languageGroup;

        private Languages _currecntLanguage;

        private void Awake()
        {
            LocalizationManager.Read();
            _languageGroup.OnLanguageBtnChanged.AddListener(SetLanguage);
            SetLanguage(_defaultLanguage);
        }

        private void Start()
        {
            _languageGroup.ActivateTgl(_defaultLanguage);
        }

        public void SetLanguage(Languages language)
        {
            LocalizationManager.Language = language.ToString();
            _currecntLanguage = language;
        }

    }
}
