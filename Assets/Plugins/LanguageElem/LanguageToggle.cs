using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ALMA.Language
{
    [RequireComponent(typeof(Toggle))]
    public class LanguageToggle : MonoBehaviour
    {
        [field: SerializeField] public MultiLanguage.Languages Language { get; private set; }
        public Toggle.ToggleEvent onValueChanged => _toggle.onValueChanged;
        public bool IsOn => _toggle.isOn;
        public Image Image => _toggle.image;

        [SerializeField] private Sprite _selectedSprite;
        [SerializeField] private Toggle _toggle;

        private void OnValidate()
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();
        }

        public void SetSelectedSprite(bool val)
        {
            Image.overrideSprite = val ? _selectedSprite : null;
        }

        internal void SetIsOnWithoutNotify(bool isOn)
        {
            _toggle.SetIsOnWithoutNotify(isOn);
            SetSelectedSprite(isOn);
        }
    }
}