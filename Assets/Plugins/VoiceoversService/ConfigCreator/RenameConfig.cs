﻿using System;
using UnityEngine;

namespace ALMA.VoiceoversService
{
    [CreateAssetMenu(fileName = "NewRenameConfig", menuName = "Settings/Voiceovers/RenameConfig")]
    public class RenameConfig : ScriptableObject
    {
        [Serializable]
        public class PreviosNameToNewName
        {
            public string PreviosName;
            public string NewName;
        }

        public PreviosNameToNewName[] PreviosNameToNewNames;
    }
}