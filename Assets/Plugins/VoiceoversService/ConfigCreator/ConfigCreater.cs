using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace ALMA.VoiceoversService
{
    public class ConfigCreater : MonoBehaviour
    {
        public Dictionary<string, string> RenameFileInfos => _renameFileInfos;
        public bool IsInitialized => _renameFileInfos != null && _renameFileInfos.Count > 0;

        [SerializeField] private Chapters _chapter;
        [SerializeField] private Lessons _lesson;
        [SerializeField] private string _renameConfigName;
        [SerializeField] private string _castomPath = string.Empty;

        [SerializeField, HideInInspector] private string _applicationResoucesPath;
        private readonly string[] _language = new[]
        {
            "English",
            "Kazakh",
            "Russian"
        };
        private readonly Dictionary<string, string> _renameFileInfos = new();

        private const string _voiceoverPath = "Voiceover";

        private string GetPathToRenameConfig() => $"{GetPathToVoiceovers(2)}/{_renameConfigName}";

        private string GetPathToVoiceovers(int languageId) => $"{_voiceoverPath}/{_language[languageId]}/{_chapter}/{_lesson}";

        private void OnValidate()
        {
            _applicationResoucesPath = $"{Application.dataPath}/Resources";
        }

        public void LoadRenameConfig()
        {
            ClearRenameConfig();
            var renameConfig = Resources.Load<RenameConfig>(GetPathToRenameConfig());
            if (renameConfig == null)
            {
                Debug.Log($"{GetPathToRenameConfig()} not found");
                return;
            }

            foreach (var namesInfo in renameConfig.PreviosNameToNewNames)
            {
                _renameFileInfos.Add(namesInfo.PreviosName, namesInfo.NewName);
            }
        }

        public void ClearRenameConfig()
        {
            _renameFileInfos.Clear();
        }

        public void RenameFiles()
        {
            for (int i = 0; i < _language.Length; i++)
            {
                string fileType = ".mp3";
                string applicationVoiceoversDirectoryPath = $"{_applicationResoucesPath}/{GetPathToVoiceovers(i)}";
                string[] voiceoversPaths = Directory.GetFiles(applicationVoiceoversDirectoryPath, $"*{fileType}");

                DeleteMetaFiles(applicationVoiceoversDirectoryPath);
                RenameVoiceovers(fileType, applicationVoiceoversDirectoryPath, voiceoversPaths);
            }
        }

#if UNITY_EDITOR
        public void CreateConfig()
        {
            for (int i = 0; i < _language.Length; i++)
            {
                string voiceoversPath = GetVoiceoversPath(i);

                var audioClips = Resources.LoadAll<AudioClip>(voiceoversPath);
                if (audioClips == null || audioClips.Length == 0)
                {
                    Debug.LogWarning($"audio for {_language[i]} not found...");
                    //continue;
                }

                List<VoiceoverData> voiceovers = CreateVoiceoversDatas(audioClips);

                string newConfigName = GetConfigName();
                DeleteIfExisting(voiceoversPath, newConfigName);

                VoiceoversConfig lessonVoiceoverConfig = BuildLessonVoiceoverConfig(voiceovers.ToArray(), newConfigName);
                AssetDatabase.CreateAsset(lessonVoiceoverConfig, $"Assets/Resources/{voiceoversPath}/{newConfigName}.asset");
            }
            AssetDatabase.SaveAssets();
        }

        private void DeleteIfExisting(string voiceoversPath, string newConfigName)
        {
            string fullConfigPath = $"{_applicationResoucesPath}/{voiceoversPath}/{newConfigName}.asset";
            if (File.Exists(fullConfigPath))
            {
                Debug.Log($"file {newConfigName} already exist. File was replaced");
                File.Delete(fullConfigPath);
                File.Delete($"{fullConfigPath}.meta");
            }
        }

        public void RefreshUnityAssets()
        {
            AssetDatabase.Refresh();
        }
#endif

        private string GetConfigName()
        {
            string newConfigName;
            if (_castomPath == string.Empty)
                newConfigName = $"{_chapter}{_lesson}VoiceoversConfig";
            else
                newConfigName = $"{_castomPath}VoiceoversConfig";
            return newConfigName;
        }

        private string GetVoiceoversPath(int i)
        {
            if (_castomPath != string.Empty)
                return $"{_voiceoverPath}/{_language[i]}/{_castomPath}";
            else
                return GetPathToVoiceovers(i);
        }

        private static List<VoiceoverData> CreateVoiceoversDatas(AudioClip[] audioClips)
        {
            List<VoiceoverData> voiceovers = new(audioClips.Length);
            foreach (var audioClip in audioClips)
                voiceovers.Add(new(audioClip.name, audioClip));
            return voiceovers;
        }

        private static VoiceoversConfig BuildLessonVoiceoverConfig(VoiceoverData[] voiceovers, string newConfigName)
        {
            var lessonVoiceoverConfig = ScriptableObject.CreateInstance<VoiceoversConfig>();
            lessonVoiceoverConfig.name = newConfigName;
            lessonVoiceoverConfig.SetVoiceovers(voiceovers);
            return lessonVoiceoverConfig;
        }

        private static void DeleteMetaFiles(string applicationVoiceoversDirectoryPath)
        {
            string[] metaFiles = Directory.GetFiles(applicationVoiceoversDirectoryPath, $"*.meta");
            foreach (var metaFile in metaFiles)
                File.Delete(metaFile);
        }

        private void RenameVoiceovers(string fileType, string applicationVoiceoversDirectoryPath, string[] voiceoversPaths)
        {
            foreach (var voiceoverPath in voiceoversPaths)
            {
                string fileName = Path.GetFileName(voiceoverPath);
                fileName = fileName.Remove(fileName.LastIndexOf('.'));

                try
                {
                    string newFileName = _renameFileInfos[fileName];
                    string newFilePath = $"{applicationVoiceoversDirectoryPath}/{newFileName}{fileType}";

                    File.Move(voiceoverPath, newFilePath);
                }
                catch (KeyNotFoundException)
                {
                    Debug.Log($"file {fileName} not fouded");
                    continue;
                }
            }
        }
    }
}