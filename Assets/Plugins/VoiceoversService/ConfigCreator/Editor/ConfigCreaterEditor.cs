﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ALMA.VoiceoversService
{
    [CustomEditor(typeof(ConfigCreater))]
    public class ConfigCreaterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            ConfigCreater configCreater = (ConfigCreater)target;

            EditorGUILayout.Space(20);
            if (GUILayout.Button("Load rename config"))
                configCreater.LoadRenameConfig();

            if (GUILayout.Button("Clear rename config"))
                configCreater.ClearRenameConfig();

            if (configCreater.IsInitialized)
            {
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(5));
                foreach (var renameFileInfo in configCreater.RenameFileInfos)
                    PrewAndNewNamesLabel(renameFileInfo);
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(5));

                EditorGUILayout.Space(10);
                if (GUILayout.Button("Rename files"))
                    configCreater.RenameFiles();
            }

            GUILayout.Space(10);
            if (GUILayout.Button("Create config"))
                configCreater.CreateConfig();

            if (GUILayout.Button("Refresh unity assets"))
                configCreater.RefreshUnityAssets();
        }

        private void PrewAndNewNamesLabel(KeyValuePair<string, string> pair)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField($"Previos name: {pair.Key}");
            EditorGUILayout.LabelField($"New name: {pair.Value}");
            EditorGUILayout.EndHorizontal();
        }
    }
}