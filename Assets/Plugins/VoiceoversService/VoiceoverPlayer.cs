﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.SimpleLocalization.Scripts;
using ALMA.SoundService;
using System.Linq;

namespace ALMA.VoiceoversService
{
    public class VoiceoverPlayer : IInitializable, IDisposable
    {
        private readonly Dictionary<string, string> _lessonVoiceoversPaths = new();
        private readonly Dictionary<string, string> _commonVoiceoversPaths = new();
        private readonly Dictionary<string, AudioClip> _voiceovers = new();
        private readonly SoundGroup _voiceoverGroup;

        private VoiceoverData[] _commonVoiceovers;
        private string _currentKeyPlaing;

        public VoiceoverPlayer(LocalizeCommonVoiceoversPaths commonVoiceoversPaths, SoundGroupsController soundGroups)
        {
            _voiceoverGroup = soundGroups.GetGroup(SoundGroups.Voiceover);
            Debug.Log("VoiceoverPlayer created");

            if (commonVoiceoversPaths == null)
            {
                Debug.Log("common Voiceovers was not loaded");
                return;
            }

            foreach (var data in commonVoiceoversPaths.Paths)
                _commonVoiceoversPaths.Add(data.Language.ToString(), data.PathToConfig);
        }

        public void Initialize()
        {
            Localize();
            LocalizationManager.OnLocalizationChanged += Localize;
        }

        public void Dispose()
        {
            LocalizationManager.OnLocalizationChanged -= Localize;
        }

        public void LoadLessonVoiceovers(LocalizeVoiceoversPaths voiceoversPaths)
        {
            foreach (var data in voiceoversPaths.Paths)
                _lessonVoiceoversPaths.Add(data.Language.ToString(), data.PathToConfig);
            LocalizeLessonVoiceovers();
        }

        public void UnloadLessonVoiceovers()
        {
            _lessonVoiceoversPaths.Clear();
            _voiceovers.Clear();
            Debug.Log($"lesson voiceovers has been cleared");
        }

        public float Play(string key)
        {
            if (key == null)
                return -1f;

            if (_voiceoverGroup.Source.isPlaying)
                Stop();

            try
            {
                _currentKeyPlaing = key;
                AudioClip clip = _voiceovers[key];
                _voiceoverGroup.Play(clip);

                return clip.length;
            }
            catch (KeyNotFoundException)
            {
                Debug.LogWarning($"Voiceover with key {key} not found!");
                return -1f;
            }
        }

        public void Stop()
        {
            if (_voiceoverGroup.Source == null || !_voiceoverGroup.Source.isPlaying)
                return;

            _voiceoverGroup.Source.Stop();
        }

        public AudioClip GetVoiceover(string key) => _voiceovers[key];

        public KeyValuePair<string, AudioClip>[] GetVoiceovers() => _voiceovers.ToArray();

        public AudioClip this[string key]
        {
            get => GetVoiceover(key);
        }

        private void Localize()
        {
            LocalizeCommonVoiceovers();
            LocalizeLessonVoiceovers();
        }

        private void LocalizeCommonVoiceovers()
        {
            if (_commonVoiceoversPaths == null || _commonVoiceoversPaths.Count == 0)
                return;

            string locViceoverPath = _commonVoiceoversPaths[LocalizationManager.Language];
            VoiceoversConfig commonVoiceoversConf = LoadVoiceoversConfig(locViceoverPath);
            Debug.Log($"common voiceovers was loaded: {locViceoverPath}");

            _commonVoiceovers = new VoiceoverData[commonVoiceoversConf.Voiceovers.Length];
            commonVoiceoversConf.Voiceovers.CopyTo(_commonVoiceovers, 0);

        }

        private void LocalizeLessonVoiceovers()
        {
            if (_lessonVoiceoversPaths.Count == 0)
                return;

            _voiceovers.Clear();

            string locViceoverPath = _lessonVoiceoversPaths[LocalizationManager.Language];
            VoiceoversConfig lessonVoiceovers = LoadVoiceoversConfig(locViceoverPath);
            Debug.Log($"lesson voiceovers was loaded: {locViceoverPath}");

            AddVoiceovers(lessonVoiceovers.Voiceovers);
            AddVoiceovers(_commonVoiceovers);

            if (_voiceoverGroup.Source.isPlaying)
                Play(_currentKeyPlaing);
        }

        private static VoiceoversConfig LoadVoiceoversConfig(string locViceoverPath)
        {
            return Resources.Load<VoiceoversConfig>(locViceoverPath) ?? throw new Exception($"VoiceoversConfig was not loaded! Path: {locViceoverPath}");
        }

        private void AddVoiceovers(VoiceoverData[] voiceovers)
        {
            if (voiceovers == null)
                return;

            foreach (VoiceoverData data in voiceovers)
                _voiceovers.Add(data.Key, data.Clip);
        }
    }
}