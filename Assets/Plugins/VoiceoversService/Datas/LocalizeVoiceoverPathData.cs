﻿using ALMA.Language;

namespace ALMA.VoiceoversService
{
    public class LocalizeVoiceoverPathData
    {
        public LocalizeVoiceoverPathData(MultiLanguage.Languages language, string pathToConfig)
        {
            Language = language;
            PathToConfig = pathToConfig;
        }

        public MultiLanguage.Languages Language { get; private set; }
        public string PathToConfig { get; private set; }
    }
}