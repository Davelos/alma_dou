﻿using UnityEngine;
using ALMA.Language;

namespace ALMA.VoiceoversService
{
    public abstract class VoiceoversPathsBase : ScriptableObject
    {
        public LocalizeVoiceoverPathData[] Paths
        {
            get
            {
                LocalizeVoiceoverPathData[] paths = new LocalizeVoiceoverPathData[_languages.Length];

                for (int i = 0; i < _languages.Length; i++)
                {
                    var curLanguage = _languages[i];
                    var curPath = GetPathToConfig(curLanguage);
                    paths[i] = new(curLanguage, curPath);
                }

                return paths;
            }
        }

        [SerializeField] private MultiLanguage.Languages[] _languages;

        protected abstract string GetPathToConfig(MultiLanguage.Languages language);
    }
}
