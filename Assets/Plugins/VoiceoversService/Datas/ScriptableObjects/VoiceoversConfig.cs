using UnityEngine;

namespace ALMA.VoiceoversService
{
    [CreateAssetMenu(fileName = "NewLsVoiceoverConfig", menuName = "Settings/Voiceovers/Lesson voiceover config")]
    public class VoiceoversConfig : ScriptableObject
    {
        [field: SerializeField] public VoiceoverData[] Voiceovers { get; private set; }

        public void SetVoiceovers(VoiceoverData[] voiceovers)
        {
            Voiceovers = voiceovers;
        }
    }
}
