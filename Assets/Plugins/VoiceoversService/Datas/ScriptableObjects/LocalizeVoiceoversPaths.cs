﻿using UnityEngine;
using ALMA.Language;

namespace ALMA.VoiceoversService
{
    [CreateAssetMenu(fileName = "NewLocalizeVoiceoversPaths", menuName = "Settings/Voiceovers/Localaze voiceovers paths")]
    public class LocalizeVoiceoversPaths : VoiceoversPathsBase
    {
        [SerializeField] private Chapters _chapter;
        [SerializeField] private Lessons _lesson;

        private const string _configPostfix = "VoiceoversConfig";
        private const string _voiceoverPathToDir = "Voiceover";

        protected override string GetPathToConfig(MultiLanguage.Languages language) => $"{_voiceoverPathToDir}\\{language}\\{_chapter}\\{_lesson}\\{_chapter}{_lesson}{_configPostfix}";
    }
}