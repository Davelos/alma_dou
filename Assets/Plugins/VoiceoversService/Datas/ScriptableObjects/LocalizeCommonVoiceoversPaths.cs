﻿using UnityEngine;
using ALMA.Language;

namespace ALMA.VoiceoversService
{
    [CreateAssetMenu(fileName = "NewLocalizeCommonVoiceoversPaths", menuName = "Settings/Voiceovers/Localaze common voiceovers paths")]
    public class LocalizeCommonVoiceoversPaths : VoiceoversPathsBase
    {
        [SerializeField] private string _commonDirectoryName = "Common";
        [SerializeField] private string _commonConfigName = "CommonVoiceoversConfig";

        private const string _voiceoverPathToDir = "Voiceover";

        protected override string GetPathToConfig(MultiLanguage.Languages language) => $"{_voiceoverPathToDir}\\{language}\\{_commonDirectoryName}\\{_commonConfigName}";
    }
}