﻿using System;
using UnityEngine;

namespace ALMA.VoiceoversService
{
    [Serializable]
    public class VoiceoverData
    {
        public VoiceoverData(string key, AudioClip clip)
        {
            Key = key;
            Clip = clip;
        }

        [field: SerializeField] public string Key { get; private set; }
        [field: SerializeField] public AudioClip Clip { get; private set; }
    }
}